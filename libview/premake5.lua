require( "premake-qt/qt" )

local qt = premake.extensions.qt

workspace "LibView"
	architecture "x64"

	configurations {
		"Debug",
		"Test",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
LibDir = {}

IncludeDir["Catch2"] = "LibView/vendor/"

LibDir["JG"] = "LibView/vendor/JG"


project "LibView"
	

	location "LibView"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"


	qt.enable()
	require("qtpath")
	qtmodules { "core", "widgets", "gui" }
	qtprefix "Qt5"
	configuration { "Debug" }
		qtsuffix "d"
	configuration {}

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	--pchheader "LibViewPCH.h"
	--pchsource "LibView/src/LibView/pch/LibViewPCH.cpp"

	files {
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
		"%{prj.name}/src/**.ui",
		"%{prj.name}/src/**.qrc"
	}

	includedirs	{
		-- "%{prj.name}/vendor/spdlog/include",
		-- "%{prj.name}/vendor/OpenAL/include",
		-- "%{IncludeDir.GLFW}",
		-- "%{IncludeDir.WavFileReader}",
		"%{prj.name}/src",
		"%{IncludeDir.Catch2}"
	}

	libdirs{

		"%{LibDir.JG}"
	}

	links{
		-- "GLFW",
		-- "opengl32.lib",
		-- "OpenAL32.lib"
		-- "JSON.lib"
	}

	filter "system:linux"

		staticruntime "on"
		systemversion "latest"

		buildoptions "-fPIC"

		defines{
			"LIBVIEW_PLATFORM_LINUX"
		}

	filter "system:windows"

		staticruntime "on"
		systemversion "latest"

		defines{
			"LIBVIEW_PLATFORM_WINDOWS"
		}

		postbuildcommands{
			-- ("{COPY} %{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/Sandbox")
		}

	filter {"system:windows", "configurations:Debug or Test"}

		buildoptions "/MDd"

		links {

			"JSON_mdd.lib"
		}
		
	filter {"system:windows", "configurations:Dist or Release"}

		buildoptions "/MD"
		
		links {

			"JSON.lib"
		}
	
	
	filter "configurations:Debug"
		defines "LIBVIEW_DEBUG"
		symbols "on"

		buildmessage 'Running python Widget compiler'

		prebuildcommands {
			'py ../qtwidbuild.py'
		 }

	filter "configurations:Release"
		defines "LIBVIEW_RELEASE"
		optimize "on"

		buildmessage 'Running python Widget compiler'

		prebuildcommands {
			'py ../qtwidbuild.py'
		 }

	filter "configurations:Dist"
		defines "LIBVIEW_DIST"
		optimize "on"

		buildmessage 'Running python Widget compiler'

		prebuildcommands {
			'py ../qtwidbuild.py'
		}

	filter "configurations:Test"
		defines "LIBVIEW_TEST"
		symbols "on"

