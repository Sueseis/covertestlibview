workspace "LibView"
	architecture "x64"

	configurations {
		"Test",
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Include Directories relative to root folder 

IncludeDir = {}
LibDir = {}

IncludeDir["Catch2"] = "LibView/vendor/"
-- IncludeDir["GLFWS"] = "Tesseract/vendor/GLFW/src"
-- IncludeDir["OpenAL"] = "Tesseract/vendor/OpenAL/include"
-- IncludeDir["WavFileReader"] = "Tesseract/vendor/KojiroSakadoWavFileReader/"

-- LibDir["OpenAL"] = "Tesseract/vendor/OpenAL/libs/Win64/"

-- include "Tesseract/vendor/GLFW"

-- group "Dependencies"
-- include "Tesseract/vendor/GLFW"
-- include "Tesseract/vendor/OpenAL"
-- include "Tesseract/vendor/Glad"
-- include "Tesseract/vendor/imgui"
-- group ""


project "LibView"
	

	location "LibView"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"


	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	--pchheader "LibViewPCH.h"
	--pchsource "LibView/src/LibView/pch/LibViewPCH.cpp"

	files {
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs	{
		-- "%{prj.name}/vendor/spdlog/include",
		-- "%{prj.name}/vendor/OpenAL/include",
		-- "%{IncludeDir.GLFW}",
		-- "%{IncludeDir.WavFileReader}",
		"%{prj.name}/src",
		"%{IncludeDir.Catch2}"
	}

	libdirs{

		-- "%{LibDir.OpenAL}"
	}

	links{
		-- "GLFW",
		-- "opengl32.lib",
		-- "OpenAL32.lib"
	}

	filter "system:linux"

		staticruntime "on"
		systemversion "latest"

		--buildoptions "-fPIC"

		defines{
			"LIBVIEW_PLATFORM_LINUX"
		}

	filter "system:windows"

		staticruntime "on"
		systemversion "latest"

		defines{
			"LIBVIEW_PLATFORM_WINDOWS"
		}

		postbuildcommands{
			-- ("{COPY} %{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/Sandbox")
		}

	filter {"system:windows", "configurations:Debug or Test"}

		buildoptions "/MDd"
		
	filter {"system:windows", "configurations:Dist or Release"}

		buildoptions "/MD"
	
	
	filter "configurations:Debug"
		defines "LIBVIEW_DEBUG"
		symbols "on"

	filter "configurations:Release"
		defines "LIBVIEW_RELEASE"
		optimize "on"

	filter "configurations:Dist"
		defines "LIBVIEW_DIST"
		optimize "on"

	filter "configurations:Test"
		defines "LIBVIEW_TEST"
		symbols "on"



--[[


	
	project "Sandbox"
	location "Sandbox"
	kind "ConsoleApp"
	
	language "C++"
	cppdialect "C++17"
	
	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")
	
	files {
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}
	
	includedirs {
		-- "LibView/vendor/spdlog/include",
		-- "Tesseract/src"
	}
	
	links {
		"LibView"
	}
	
	filter "system:windows"
	staticruntime "on"
	systemversion "latest"
	
	defines{
		"LIBVIEW_PLATFORM_WINDOWS"
	}
	
	filter "configurations:Debug"
	defines "LIBVIEW_DEBUG"
	buildoptions "/MDd"
	symbols "on"
	
	filter "configurations:Release"
	defines "LIBVIEW_RELEASE"
	buildoptions "/MD"
	optimize "on"
	
	filter "configurations:Dist"
	defines "LIBVIEW_DIST"
	buildoptions "/MD"
	optimize "on"
	
	
	
]]--