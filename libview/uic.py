import os

allUicFiles = []


def iterateFilesIn(fPath):

    global allUicFiles

    l = os.listdir(path=fPath)
    print("All Files in", fPath, ":",l)

    for p in l:

        print(p, "is a: ", end="")

        p = fPath + "/" + p

        if os.path.isdir(p):

            print("directory\n\n=>")
            iterateFilesIn(p)
        
        elif os.path.splitext(p)[1] == ".ui":

            print("file with extention .ui")
            allUicFiles.append(p)
        
        else:

            print("other file")

iterateFilesIn('.')

print(allUicFiles)

for uicf in allUicFiles:

    basename  = os.path.basename(uicf)
    basename = os.path.splitext(basename)[0]
    folder = os.path.split(uicf)[0]
    uiFileName = "ui_" + basename
    #command = "uic " + uicf + " -o " + folder + "/" + uiFileName + ".h"
    #print("running: ", command)
    
    os.system(command)

    with open(folder + "/" + basename + ".h", "w") as f:

        f.write(
"#pragma once\n\n"
+"#include <QWidget>\n" 
+"#include \"" + uiFileName + ".h\"\n\n" 
+ "class " + basename + " : public QWidget \n"
+"{\n"
+"	Q_OBJECT\n\n"
+"public:\n"
+"  " + basename + "(QWidget *parent = Q_NULLPTR);\n"
+"  ~" + basename + "(); \n\n"
+"private:\n"
+"  Ui::" + basename + " ui;\n"
+"};\n"
        )

    with open(folder + "/" + os.path.splitext(basename)[0] + ".cpp", "w") as f:

        f.write(
"#include \""+ os.path.splitext(basename)[0] + ".h\"\n\n"
+ basename + "::" + basename + "(QWidget *parent) : QWidget(parent) \n"
+"{\n"
+"	ui.setupUi(this);\n\n"
+"}\n"
+ basename + "::~" + basename + "() {}\n"
        )

    
    mocFileName = "moc_" + basename 
    command = "moc " + folder + "/" + basename + ".h " + " -o " + folder + "/" + mocFileName + ".cpp"
    #print("running: ", command)
    
    #os.system(command)



