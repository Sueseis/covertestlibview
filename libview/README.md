# LibView

## Project setup

To setup the LibView project for visual studio you need to follow the following steps.

### Qt installation

The LibView Project requires a valid [Qt](https://qt.io) installation in order to function using premake or outside of the Test configuration.
Make shure to set the Qt dll folder in your environment PATH variable. To set this under Windows you should find it under Start > Search for "Edit system environment variables" > Environment Variables > Under "System Variables" > Click "Path" > click "Edit" > click "New" > click "Browse" > Navigate to the Qt dll folder (Usually under "C:\\Qt\\\[version\]\\msvc2019_64\\bin) > click "OK"

### Setting up Qt with premake

navigate to \\vendor\\bin\\premake and create a new file named ```qtpath.lua```.
Open the file and write ```qtpath "<location of qt library>"```, where ```<location of qt library>``` might be ```F:/Qt/5.14.2/msvc2017_64``` as described above.

```qtpath.lua``` might look like this:

```qtpath "F:/Qt/5.14.2/msvc2017_64"```

### Creating visual studio 2019 Solution files

In order to create the Solution files you need to run the ```_GenerateProjects.bat``` file in the Project root. It should create Solution and Project files for the project. By clicking the Solution file you should be able to open the solution.

### Configurations

The Project contains 4 configurations.
Currently there are the following major differences between those

Test : disables all optimizations; runs only Test code
Debug : disables all optimizations; runs only the main function
Release : allows optimisation; runs only the main function; removes trace level debug output
Dist : allows optimisation; runs only the main function; removes all debug output
