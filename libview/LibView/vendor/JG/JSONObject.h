#pragma once
#include "JSON_decl.h"
#include <string>
#include <map>


namespace JG
{
    namespace json
    {
        class JSONObject
        {
        private:

            std::map<std::string, JSON*> m_Map;

        public:

            JSONObject();
            JSONObject(const JSONObject& object);
            JSONObject(const std::map<std::string, JSON*>& object);
            ~JSONObject();

            JSON& operator[] (const std::string& index);

            std::string toString();

        private:

            void clearData();
        };

    }
}