#include "AttributeMap.h"
#include "LibView/core/Macros.h"

#include "LibView/exception/model/AttributePathAlreadyFoundException.h"
#include "LibView/exception/model/AttributePathNotFoundException.h"
#include "LibView/exception/model/AttributeIndexAlreadyFoundException.h"
#include "LibView/exception/model/AttributeIndexNotFoundException.h"




namespace LibView
{
    namespace model
    {
        

        void AttributeMap::set(std::string path, const index_t& index, int64_t multiplicity)
        {
            throwOnMissingPath(path);
            throwOnMissingIndex(index);

            // set the map value at path key with a pair of the given values
            m_AttributeMap[path].setFirst(index);
            m_AttributeMap[path].setSecond(multiplicity);

            m_ReverseMap[index] = path;
        }

        void AttributeMap::add(std::string path, const index_t& index, int64_t multiplicity)
        {
            throwOnFoundPath(path);
            throwOnFoundIndex(index);

            // initialise the map value at path key with a pair of the given values
            m_AttributeMap[path] = LibViewPair<index_t, int64_t>(index, multiplicity);

            // create an entry in the reverse map
            m_ReverseMap[index] = path;
        }

        void AttributeMap::remove(std::string path)
        {
            throwOnMissingPath(path);

            // Remove entries in maps

            index_t index = m_AttributeMap[path].getFirst();
            m_AttributeMap.erase(path);
            m_ReverseMap.erase(index);
        }

        void AttributeMap::remove(const index_t& index)
        {
            throwOnMissingIndex(index);

            // Remove entries in maps

            std::string path = m_ReverseMap[index];
            m_AttributeMap.erase(path);
            m_ReverseMap.erase(index);
        }

        AttributeMap::AttributeMap() : 
            m_AttributeMap(LIBVIEW_ATTRIBMAP_MAP()),
            m_ReverseMap(LIBVIEW_ATTRIBMAP_REV_MAP())
        {}

        LibViewPair<index_t, int64_t> AttributeMap::operator[](const std::string& path) const
        {
            throwOnMissingPath(path);
            return m_AttributeMap.at(path);
        }

        index_t AttributeMap::getIndex(const std::string& path) const
        {
            throwOnMissingPath(path);

            //return index_t::requestNull();

            // TODO REPLACE
            return m_AttributeMap.at(path).getFirst();
        }

        int64_t AttributeMap::getMultiplicity(const std::string& path) const
        {
            throwOnMissingPath(path);


            return m_AttributeMap.at(path).getSecond();
        }

        std::string AttributeMap::getPath(const index_t& index) const
        {
            throwOnMissingIndex(index);

            std::string result = m_ReverseMap.at(index);
            return result;
        }

        std::string AttributeMap::operator[](const index_t& index) const
        {
            return getPath(index);
        }

        bool AttributeMap::has(const index_t& index) const
        {
            throwOnMissingIndex(index);

            return m_ReverseMap.find(index) != m_ReverseMap.end();
        }

        bool AttributeMap::has(const std::string& path) const
        {
            //throwOnMissingPath(path);

            return m_AttributeMap.find(path) != m_AttributeMap.end();
        }

        std::vector<LibViewPair<index_t, int64_t>> AttributeMap::getPairList() const
        {
            std::vector<LibViewPair<index_t, int64_t>> pairVector;
            for(auto const& element : m_AttributeMap)
            {
                pairVector.push_back(element.second);
            }
            return pairVector;
        }

        std::vector<index_t> AttributeMap::getIndexList() const
        {
            std::vector<index_t> indexVector;

            for(const std::pair<std::string, LibViewPair<index_t, int64_t>>& element : m_AttributeMap)
            {
                indexVector.push_back(element.second.getFirst());
            }

            return indexVector;
        }

        
        void AttributeMap::setMultiplicity(const std::string& path, int64_t multiplicity)
        {
            throwOnMissingPath(path);

            // set the map value at path key with a pair of the given values
            m_AttributeMap[path].setSecond(multiplicity);
        }



        void AttributeMap::throwOnFoundIndex(const index_t& index) const
        {
            if(m_ReverseMap.find(index) != m_ReverseMap.end())
            {
                // Throw exception if index is already present
                throw exception::AttributeIndexAlreadyFoundException(LIBVIEW_LOCATION);
            }
        }



        void AttributeMap::throwOnMissingIndex(const index_t& index) const
        {
            if(m_ReverseMap.find(index) == m_ReverseMap.end())
            {
                // Throw exception if index is not found
                throw exception::AttributeIndexNotFoundException(LIBVIEW_LOCATION);
            }
        }


        void AttributeMap::throwOnFoundPath(const std::string& path) const
        {
            if(m_AttributeMap.find(path) != m_AttributeMap.end())
            {
                // Throw exception if key is already present
                throw exception::AttributePathAlreadyFoundException(LIBVIEW_LOCATION);
            }
        }


        void AttributeMap::throwOnMissingPath(const std::string& path) const
        {
            if(m_AttributeMap.find(path) == m_AttributeMap.end())
            {
                // Throw exception if key is not found
                throw exception::AttributePathNotFoundException(LIBVIEW_LOCATION);
            }
        }
    }
}