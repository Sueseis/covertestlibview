#pragma once

/** @file
 *
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <string>
#include <stdint.h>
#include <vector>

#include "LibViewPair.h"


#include"LibView/core/LibViewTesting.h"

#include "LibView/core/config.h"

#if LIBVIEW_ATTRIBMAP_USE_UNORDERED_MAP


#include <unordered_map>
#define LIBVIEW_ATTRIBMAP_REV_MAP std::unordered_map<index_t, std::string, index_t_hash>
#define LIBVIEW_ATTRIBMAP_MAP std::unordered_map<std::string, LibViewPair<index_t, int64_t>>


#else

#include <map>
#define LIBVIEW_ATTRIBMAP_MAP std::map <std::string, LibViewPair<index_t, int64_t>>
#define LIBVIEW_ATTRIBMAP_REV_MAP std::map<index_t, std::string>

#endif


namespace LibView
{
    namespace model
    {

        class AttributeMap
        {
        protected:

            friend class AttributeHolder;

            /**
             * Sets the index of an attribute and its multiplicity for a given path.
             * Overwrites existing entries. Be mindful to check for unwanted overwrites.
             *
             * @param path (const std::string&) : The path of the attribute in a liberty file
             * @param index (int64_t) : The index of the attribute
             * @param multiplicity (int64_t) : The multiplicity of the attribute
             *
             * @throws exception::AttributePathNotFoundException when the Attribute path has not yet been set
             * @throws exception::AttributeIndexNotFoundException when the Attribute index has not yet been set
             */
            void set(std::string path, const index_t& index, int64_t multiplicity);

            /**
             * Adds the index of an attribute and its multiplicity for a given path in the map.
             *
             * @param path (const std::string&) : The path of the attribute in a liberty file
             * @param index (int64_t) : The index of the attribute
             * @param multiplicity (int64_t) : The multiplicity of the attribute
             *
             * @throws exception::AttributePathAlreadyFoundException when the Attribute path has already been been set
             * @throws exception::AttributeIndexAlreadyFoundException when the Attribute path has already been been set
             */
            void add(std::string path, const index_t& index, int64_t multiplicity);

            /**
             * Delete all entries for the attribute at the given Path from the data maps.
             *
             * @param path (const std::string&) : The path of the attribute in a liberty file
             *
             * @throws exception::AttributePathNotFoundException when the Attribute path has not yet been set
             */
            void remove(std::string path);


            /**
             * Delete all entries for the attribute at the given Path from the data maps.
             *
             * @param index (int64_t) : The index of the attribute in a liberty file
             *
             * @throws exception::AttributeIndexNotFoundException when the Attribute index has not yet been set
             */
            void remove(const index_t& index);


        public:


            /**
             * Default constructor for AttributeMap
             */
            AttributeMap();

            /**
             * Returns the pair of (index, multiplicity) of the attribute at the given path
             *
             * @param path (const std::string&) : The path of the attribute in a liberty file
             *
             * @return LibViewPair<int64_t,int64_t> : The index of the Attribute
             *
             * @throws exception::AttributePathNotFoundException when the Attribute path has not yet been set
             */
            LibViewPair<index_t, int64_t> operator[](const std::string& path) const;

            /**
             * Returns the index of the attribute at the given path
             *
             * @param path (const std::string&) : The path of the attribute in a liberty file
             *
             * @return int64_t : The index of the Attribute
             *
             * @throws exception::AttributePathNotFoundException when the Attribute path has not yet been set
             */
            index_t getIndex(const std::string& path) const;

            /**
             * Returns the multiplicity of the attribute at the given path
             *
             * @param path (const std::string&) : The path of the attribute in a liberty file
             *
             * @return int64_t : The multiplicity of the Attribute
             *
             * @throws exception::AttributePathNotFoundException when the Attribute path has not yet been set
             */
            int64_t getMultiplicity(const std::string& path) const;

            /**
             * Returns the path of the attribute at the given index
             *
             * @param index (int64_t) : The index of the attribute in a liberty file
             *
             * @return std::string : The path of the Attribute
             *
             * @throws exception::AttributeIndexNotFoundException when the Attribute index has not yet been set
             */
            std::string getPath(const index_t& index) const;

            /**
            * Returns the path of the attribute at the given index
            *
            * @param index (int64_t) : The index of the attribute in a liberty file
            *
            * @return std::string : The path of the Attribute
            *
            * @throws exception::AttributeIndexNotFoundException when the Attribute index has not yet been set
            */
            std::string operator[](const index_t& index) const;

            /**
             * Returns whether the index is present
             *
             * @param index (int64_t) : The index of the attribute in a liberty file
             *
             * @return bool : Whether the index exists
             *
             * @throws exception::AttributeIndexNotFoundException when the Attribute index has not yet been set
             */
            bool has(const index_t& index) const;

            /**
             * Returns whether the path is present
             *
             * @param path (const std::string&) : The path of the attribute in a liberty file
             *
             * @return bool : Whether the index exists
             *
             * @throws exception::AttributeIndexNotFoundException when the Attribute index has not yet been set
             */
            bool has(const std::string& path) const;

            /**
             * Returns list of all pairs (values of map)
             *
             * @return std::vector<LibViewPair<index_t, int64_t>> : List of all pairs in the map
             *
             */
            std::vector<LibViewPair<index_t, int64_t>> getPairList() const;


            /**
             * Returns list of all paths (keys of map)
             *
             * @return std::vector<std::string> : List of all keys in the map
             *
             */
            std::vector<index_t> getIndexList() const;


        private:

            /**
             * Maps the path of an attribute to a pair of its index and multiplicity.
             */
            LIBVIEW_ATTRIBMAP_MAP m_AttributeMap;


            /**
             * Maps the index of an attribute to a its path.
             */
            LIBVIEW_ATTRIBMAP_REV_MAP m_ReverseMap;

            /**
             * Set the multiplicity for a given path.
             *
             * @param path (const std::string&) : The path of the attribute in a liberty file
             * @param newMultiplicity (int64_t) : The multiplicity to be associated
             */
            void setMultiplicity(const std::string& path, int64_t multiplicity);

            /**
             * Throw Exception if index is found.
             *
             * @throws exception::AttributeIndexAlreadyFoundException when the Attribute path has already been been set
             */
            void throwOnFoundIndex(const index_t& index) const;


            /**
             * Throw Exception if index is not found.
             *
             * @throws exception::AttributeIndexNotFoundException when the Attribute index has not yet been set
             */
            void throwOnMissingIndex(const index_t& index) const;


            /**
             * Throw Exception if path is found.
             *
             * @throws exception::AttributePathAlreadyFoundException when the Attribute path has already been been set
             */
            void throwOnFoundPath(const std::string& path) const;


            /**
             * Throw Exception if path is not found.
             *
             * @throws exception::AttributePathNotFoundException when the Attribute path has not yet been set
             */
            void throwOnMissingPath(const std::string& path) const;
        };


    }
}
#include "LibView/core/LibViewEndTesting.h"