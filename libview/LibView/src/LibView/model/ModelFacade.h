#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */




#include "LibView/presenter/PresenterFacade_decl.h"
#include "LibView/presenter/IMergeTemplate.h"
#include "LibView/model/data_structure/LibertyFileData.h"
#include "LibView/model/data_structure/LibertyFileGroup.h"
#include "PresetHolder.h"

#include "ModelState.h"

#include <vector>
#include <string>


namespace LibView
{
    namespace model
    {
        class ModelFacade
        {


        public:

            /**
             * Constructor for the Facade
             */
            ModelFacade();
            ~ModelFacade();

            /**
             * Loads a set of LibFiles.
             * Receives a list of paths of the LibFiles that should be loaded into the programm.
             *
             * @param paths (std::vector<std::string>) : List of paths to the LibFiles to load
             */
            void loadLibertyFiles(const std::vector<std::string>& paths);

            /**
             * Removes a LibFile given its index.
             *
             * @param index (index_t) : Index of LibFile to be removed
             *
             * @throws LibFileIndexNotFoundException if index not found
             */
            void removeLibertyFile(const index_t& index);

            /**
              * Sorts the loaded LibFiles according to the value in a group, in ascending or descending order.
             *
             * @param groupToSortBy (index_t) : Sorting parameter is group index (therefore value stored inside)
             * @param ascending (bool) : True for ascending order, False for descending order
             *
             * @return std::vector<index_t> : Sorted list with indicies of the LibFiles
             */
            std::vector<index_t> sortBy(const index_t& groupToSortBy, bool ascending);

            /**
             * Gets the attributes matching to a given search term.
             *
             * @param searchTerm (std::string) : Search term to match to
             */
            std::vector<index_t> getMatchingAttributes(std::string searchTerm);

            /**
             * Gets all children given an index of the parent Group.
             *
             * @param index (index_t)
             *
             * @return std::vector<index_t> : List of all children Group indicies
             *
             * @throws AttributeIndexNotFoundException if index not found
             *
             */
            std::vector<index_t> getChildren(const index_t& index);

            /**
             * Forwards the nofication that there has been an update in the ModelState to the presenter.
             */
            void pushModelStateToPresenter();

            /**
             * Initialises the classes in the model package (HolderObserver),
             * stores reference to PresenterFacade.
             *
             * @param presenterFacade (presenter::PresenterFacade*) : Reference of PresenterFacade to store
             */
            void init(presenter::PresenterFacade* presenterFacade);

            /**
             * Returns the current ModelState.
             *
             * @return ModelState : current ModelState
             *
             * @throws //TODO define: if no ModelState initialised
             */
            ModelState& getModelState() const;

            /**
             * Merges two LibFiles on a merge template.
             *
             * @param template (IMergeTemplate*) : Template on which to merge on
             * @param libFileOne (index_t) : Index of first LibFile
             * @param libFileTwo (index_t) : Index of second LibFile
             */
            void mergeLibertyFiles(presenter::IMergeTemplate* tpl, const index_t& libFileOne, const index_t& libFileTwo);

            /**
             * //TODO define delta generation
             * @param libFileIndex
             * @param attributeIndex
             */
            double getDelta(const index_t& libFileIndex, const index_t& attributeIndex);

            /**
             * Getter for the LibertyFileGroup
             * @param groupIndex (index_t) : Index of the group
             *
             * @return data_structure::LibertyFileGroup : Underlying LibertyFileGroup
             *
             * @throws AttributeIndexNotFoundException if index not found
             */
            model::data_structure::LibertyFileGroup getGroupByIndex(const index_t& groupIndex) const;

            /**
             * Getter for the LibertyFileData
             *
             * @param libFileIndex (index_t) : Index of the LibFile
             *
             * @return data_structure::LibertyFileData : Underlying LibertyFileData
             *
             * @throws LibFileIndexNotFoundException if index not found
             */
            model::data_structure::LibertyFileData* getLibFileByIndex(const index_t& libFileIndex) const;


            /**
             * Add libFile to list of last added
             */
            void addLastAddedLibFiles(const index_t& newFiles);
            
            /**
             * sets the last removed lib file
             */
            void setLastRemovedLibbFile(const index_t& newFiles);
            
            /**
             * clear list of last added files
             */
            void resetLastAddedLibFiles();
            
            /**
             * clear list of last removed libfiles
             */
            void resetLastRemovedLibFiles();
            
            /**
             * clear list of last added attributes
             */
            void resetLastAddedAttributes();
            
            /**
             * clear list of last removed attributes
             */
            void resetLastRemovedAttributes();
            
            /**
             * add attribute to attribute list
             */
            void addAttribute(const std::string& attributeHierarchy);
            
            
            /**
             * remove attribute from attribute list
             */
            void removeAttribute(const std::string& attributeHierarchy);

            
            /**
             * returns the name of the attribute with a given index
             */
            std::string getAttributeName(const index_t& index);
            
            
            /**
             * returns the index of an attribute with given name
             */
            index_t getAttributeIndex(const std::string& index);

            /**
             * returns whether attribute is part of last removed attributes
             */
            bool isLastRemovedAttribute(const index_t& index);

            /**
             * returns the preset with a given index
             */
            const APreset* getPreset(const index_t& index) const;

            /**
             * returns a vector of all active presets
             */
            const std::vector<APreset*>& getPresetList() const;

        private:

            presenter::PresenterFacade* m_pPresenter;
            model::ModelState* m_ModelState; // TODO : Change to pointer
            //model::AttributeHolder* m_AttributeHolder;  // TODO : Change to pointer
            model::LibFileHolder* m_LibFileHolder; // TODO : Change to pointer
            PresetHolder* m_PresetHolder;
        };

    }
}