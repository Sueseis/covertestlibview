/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * GroupBuilder implementation
  */

#include "GroupBuilder.h"

#include "LibView/model/ModelFacade.h"
#include "LibView/core/DebugLog.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            //GroupBuilder::GroupBuilder() : builtGroup(nullptr), attrHolder(nullptr)
            //{}

            GroupBuilder::GroupBuilder(ModelFacade* pFacade)
                :
                builtGroup(nullptr),
                ModelFacadeKnower(pFacade)
            {}

            /**
             * The build operation for creating a group in the data structur after parsing a file.
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* GroupBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                std::string name;
                std::vector<translator::token::Token> tokens = group->getName();
                std::vector<translator::token::Token>::iterator iterator = tokens.begin();

                for(; iterator != tokens.end(); ++iterator)
                {
                    name.append(iterator->getValue());
                }
                builtGroup = new LibertyFileGroup(getModel());
                builtGroup->setParent(parent);
                std::string path = constructGroupPath(group, parent);
                builtGroup->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                getModel()->addAttribute(path);

                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                {
                    parent->setChild(builtGroup);
                }

                return builtGroup; // built
            }

            void GroupBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                builtGroup->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void GroupBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                builtGroup->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            std::string GroupBuilder::undifinedSimpleAttributeValueString(std::vector<translator::token::Token> value)
            {
                std::string result = " : ";

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    result.append(iterator->getValue());
                }
                result.append(" ;"); //closing simple attribute
                
                return result;
            }



            std::string GroupBuilder::undifinedComplexAttributeValueString(std::vector<translator::token::Token> value)
            {
                std::string result = "("; //opening complex attribute

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    if(iterator->getKey() == translator::token::TokenKeys::ParameterSeperator)
                        result.append(", "); //VECTOR NOT SAFE
                    else
                        result.append(iterator->getValue());
                }
                result.append(") ;"); //close complex attribute
                return result;
            }



            DynamicDimensionArray* GroupBuilder::to1DArray(std::vector<translator::token::Token> value)
            {
                DynamicDimensionArray* result = new DynamicDimensionArray(1);

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    if(iterator->getKey() == translator::token::TokenKeys::Float || iterator->getKey() == translator::token::TokenKeys::FloatS || iterator->getKey() == translator::token::TokenKeys::Integer)
                        *result << atof(iterator->getValue().c_str());
                }
                return result;
            }


            DynamicDimensionArray* GroupBuilder::to2DArray(size_t columns, size_t rows, std::vector<translator::token::Token> value) //TODO fix for scientific Float
            {
                size_t count = 0;
                DynamicDimensionArray result = DynamicDimensionArray(2);
                result.reserve(columns);
               
                std::vector<translator::token::Token>::iterator iterator = value.begin();
               
                for(; iterator != value.end(); ++iterator)
                {
                    if(iterator->getKey() == translator::token::TokenKeys::Float || iterator->getKey() ==translator::token::TokenKeys::FloatS || iterator->getKey() == translator::token::TokenKeys::Integer)
                    {
                        std::string temp = iterator->getValue();
                        double doubleValue = atof(temp.c_str());
                        size_t position = floor(count / rows);
                        result[position] << doubleValue;
                        count++;
                        //LIBVIEW_LOG_BEGIN
                        //LIBVIEW_LOG_ELEMENT(result.toString())
                        //LIBVIEW_LOG_END
                    }
                }                
                
                    return new DynamicDimensionArray(result);
            }


            DynamicDimensionArray* GroupBuilder::to3DArray(size_t columns, size_t rows, size_t height, std::vector<translator::token::Token> value) //TODO FIX
            {
                size_t count = 1;
                DynamicDimensionArray result = DynamicDimensionArray(3);
                result.reserve(columns);
                for(size_t i = 0; i < columns; i++)
                    result[i].reserve(rows);

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                size_t column;
                size_t row;

                for(; iterator != value.end(); ++iterator)
                {
                    if(iterator->getKey() == translator::token::TokenKeys::Float || iterator->getKey() == translator::token::TokenKeys::FloatS || iterator->getKey() == translator::token::TokenKeys::Integer)
                    {
                        size_t column = floor(count / rows);
                        size_t row = count % height;
                        result[column][row] << atof(iterator->getValue().c_str());
                        count++;
                    }
                }
                return new DynamicDimensionArray(result);

            }


            std::string GroupBuilder::expressionToString(std::vector<translator::token::Token> value)
            {
                std::string result = "";

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    result.append(iterator->getValue());
                }
                return result;
            }


            double GroupBuilder::toDouble(translator::token::Token value)
            {
                return atof(value.getValue().c_str());
            }




            std::vector<std::string> GroupBuilder::toVectorOfString(std::vector<translator::token::Token> value)
            {
                std::vector<std::string> vectorOfString;

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    if(iterator->getKey() != translator::token::TokenKeys::ParameterSeperator)
                        vectorOfString.push_back(iterator->getValue());
                }
                return vectorOfString;
            }


            std::string GroupBuilder::stringOfFirstToken(std::vector<translator::token::Token> value) //TODO rename
            {
                std::string result = "";
                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    result.append(iterator->getValue());
                }
                return result;
            }


            std::string GroupBuilder::constructGroupPath(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                std::string name;
                std::string path;



                std::vector<translator::token::Token> tokens = group->getName();
                std::vector<translator::token::Token>::iterator iterator = tokens.begin();


                //for(std::vector<translator::token::Token>::iterator iterator = group->getName().begin(); iterator != group->getName().end(); ++iterator)
                //                                                                      ---------                             ---------
                // Returnen neue kopien; iteratoren können also nie gleich sein

                for(; iterator != tokens.end(); ++iterator)
                {
                    name.append(iterator->getValue());
                }


                if(parent) //TODO FIX name
                    path = parent->getPath() + "/" + group->getType().getValue() + "(" + name + ")";
                else
                    path = group->getType().getValue() + "(" + name + ")";

                return path;
            }


            std::vector<std::string> GroupBuilder::iterateOverAttributes(translator::ast::Group* group)
            {
                std::string groupType = group->getType().getValue();
                std::vector<std::string> addedAttributes;
                addedAttributes.clear();
                std::vector<translator::ast::Attribute*> simpleAttributes = group->getSimpleAttributes();
                bool isScalar = false;

                for(translator::ast::Attribute* attribute : simpleAttributes)
                {
                    translator::token::Token nameToken = attribute->getM_Name();
                    std::string name = nameToken.getValue();
                    setSimpleAttribute(name, attribute->getM_Value());
                    addedAttributes.push_back(name);
                }
                std::vector<translator::ast::Attribute*> complexAttributes = group->getComplexAttributes();

                std::string groupName = stringOfFirstToken(group->getName());
                if(groupName == "scalar")
                    isScalar = true;

                for(translator::ast::Attribute* attribute : complexAttributes)
                {

                    translator::token::Token nameToken = attribute->getM_Name();
                    std::string name = nameToken.getValue();
                    if((name == "values" || name == "intermediate_values") && isScalar)
                        name += "scalar";
                    setComplexAttribute(name, attribute->getM_Value());
                    addedAttributes.push_back(name);
                }

                return addedAttributes;
            }

            void GroupBuilder::updateAttributeHolder(std::string path, std::vector<std::string> addedAttributes)
            {
                getModel()->addAttribute(path); //ping AttributeHolder with group path

                for(std::string attributes : addedAttributes)
                {
                    getModel()->addAttribute(path + "/" + attributes); //ping AttributeHolder with attribute paths
                }
            }

            LibertyFileGroup* GroupBuilder::getLibrary(LibertyFileGroup* child)
            {
                if(child->getParent() != nullptr)
                    return getLibrary(child->getParent());
                else
                    return child;
            }

            IndexTemplate* GroupBuilder::getTemplate(LibertyFileGroup* child)
            {
                std::string name = child->getName();
                LibertyFileGroup* library = getLibrary(child);
                IndexTemplate* indexTemplate = (IndexTemplate*) library->getChild(name);
                if(indexTemplate != nullptr)
                    return indexTemplate;
                else
                    throw - 1; //TODO replace                
            }
        }
    }
}