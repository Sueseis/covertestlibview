/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * LibraryBuilder implementation
  *
  * Builder class for the Library class in the data structure.
  */

#include "LibraryBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            LibraryBuilder::LibraryBuilder(ModelFacade* pFacade) :
                GroupBuilder(pFacade),
                library(new Library(pFacade))
            {
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* LibraryBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                std::string path = constructGroupPath(group, parent);
                library->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = library->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(library);

               


                return library;
            }



            void LibraryBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "delay_model")
                {
                    library->setDelay_module(stringOfFirstToken(value));
                    return;
                }
                else if(name == "time_unit")
                {
                    library->setTime_unit(stringOfFirstToken(value));
                    return;
                }
                else if(name == "voltage_unit")
                {
                    library->setVoltage_unit(stringOfFirstToken(value));
                    return;
                }
                else if(name == "current_unit")
                {
                    library->setCurrent_unit(stringOfFirstToken(value));
                    return;
                }
                else if(name == "pulling_resistance_unit")
                {
                    library->setPulling_resistance_unit(stringOfFirstToken(value));
                    return;
                }
                else if(name == "leakage_power_unit")
                {
                    library->setLeakage_power_unit(stringOfFirstToken(value));
                    return;
                }
                else if(name == "input_threshold_pct_fall")
                {
                    library->setInput_threshold_pct_fall(toDouble(value.front()));
                    return;
                }
                else if(name == "input_threshold_pct_rise")
                {
                    library->setInput_threshold_pct_rise(toDouble(value.front()));
                    return;
                }
                else if(name == "output_threshold_pct_fall")
                {
                    library->setOutput_threshold_pct_fall(toDouble(value.front()));
                    return;
                }
                else if(name == "output_threshold_pct_rise")
                {
                    library->setOutput_threshold_pct_rise(toDouble(value.front()));
                    return;
                }
                else if(name == "slew_lower_threshold_pct_fall")
                {
                    library->setSlew_lower_threshold_pct_fall(toDouble(value.front()));
                    return;
                }
                else if(name == "slew_lower_threshold_pct_rise")
                {
                    library->setSlew_lower_threshold_pct_rise(toDouble(value.front()));
                    return;
                }
                else if(name == "slew_upper_threshold_pct_fall")
                {
                    library->setSlew_upper_threshold_pct_fall(toDouble(value.front()));
                    return;
                }
                else if(name == "slew_upper_threshold_pct_rise")
                {
                    library->setSlew_upper_threshold_pct_rise(toDouble(value.front()));
                    return;
                }
                else if(name == "slew_derate_from_library")
                {
                    library->setSlew_derate_from_library(toDouble(value.front()));
                    return;
                }
                else if(name == "nom_process")
                {
                    library->setNom_process(toDouble(value.front()));
                    return;
                }
                else if(name == "nom_temperature")
                {
                    library->setNom_temperature(toDouble(value.front()));
                    return;
                }
                else if(name == "nom_voltage")
                {
                    library->setNom_voltage(toDouble(value.front()));
                    return;
                }
                else if(name == "default_cell_leakage_power")
                {
                    library->setDefault_cell_leakage_power(toDouble(value.front()));
                    return;
                }
                else if(name == "default_fanout_load")
                {
                    library->setDefault_fanout_load(toDouble(value.front()));
                    return;
                }
                else if(name == "default_inout_pin_cap")
                {
                    library->setDefault_inout_pin_cap(toDouble(value.front()));
                    return;
                }
                else if(name == "default_input_pin_cap")
                {
                    library->setDefault_input_pin_cap(toDouble(value.front()));
                    return;
                }
                else if(name == "default_output_pin_cap")
                {
                    library->setDefault_output_pin_cap(toDouble(value.front()));
                    return;
                }
                else if(name == "default_leakage_power_density")
                {
                    library->setDefault_leakage_power_density(toDouble(value.front()));
                    return;
                }


                library->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }


            void LibraryBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "capacitive_load_unit")
                {
                    double doublePart = 0;
                    std::string stringPart = "";
                    for(std::vector<translator::token::Token>::iterator iterator = value.begin(); iterator != value.end(); ++iterator)
                    {
                        if(iterator->getKey() == translator::token::TokenKeys::Float || iterator->getKey() == translator::token::TokenKeys::FloatS || iterator->getKey() == translator::token::TokenKeys::Integer)
                            doublePart = atof(iterator->getValue().c_str());
                        else if(iterator->getKey() == translator::token::TokenKeys::Name)
                            stringPart = iterator->getValue();
                    }
                    library->setCapacitive_load_unit(doublePart, stringPart);
                    return;
                }
                else if(name == "library_features")
                {
                    library->setLibrary_features(toVectorOfString(value));
                    return;
                }

                library->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }
        }
    }
}