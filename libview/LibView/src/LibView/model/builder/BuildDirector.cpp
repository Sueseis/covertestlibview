/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph B�hrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * BuildDirector implementation
  *
  * Director class  for the builder pattern, contains director operations for parsing and merging
  */

#include "BuildDirector.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
           // BuildDirector::BuildDirector() : libFileData(nullptr),
           //     depth(0),
           //     depthMinusOne(0),
           //     currentBuilder(nullptr),
           //     getModel()(nullptr)
           // {
           //     hierarchy.fill(nullptr);      
           // }

            /**
            * constructor for BuildDirector
            * @param ModelFacade* reference to ModelFacade
            */
            BuildDirector::BuildDirector(ModelFacade* facade ) : 
                ModelFacadeKnower(facade),
                libFileData(nullptr),
                depth(0),
                depthMinusOne(0),
                currentBuilder(nullptr)
            {
                hierarchy.fill(nullptr);
            }

            /**
            * destructor of BuildDirector freeing the currentBuilder
            */
            BuildDirector::~BuildDirector()
            {
                delete currentBuilder;
            }

            /**
             * Director operation for parsing. The Operation decides on what builder is needed to build the specific group of the data structure . Then it calls the construct operation to get the specific builder and uses it.
             * @param treeRoot
             * @return LibertyFileFata*
             */
            LibertyFileData* BuildDirector::loadLibertyFile(const std::string& filePath)
            {     
                libFileData = new LibertyFileData(filePath);
                translator::ast::Group* treeRoot = translator::Translator::translate(filePath);
                currentBuilder = construct(treeRoot->getType().getValue());
                hierarchy[depth] = currentBuilder->buildGroup(treeRoot, nullptr);
                libFileData->setLibraryGroup(hierarchy[depth]);
                depth++;
                depthFirstSearch(treeRoot->getChildren());
                depth = 0;
                depthMinusOne = 0;
                currentBuilder = nullptr;               
                hierarchy.fill(nullptr);

                delete treeRoot;
                return libFileData;
            }


            /**
            * DepthFirstSearch to iterate over all groups and translate them into the data_structure
            * @param std::vector<translator::ast::Group*> vector of child groups
            */
            void BuildDirector::depthFirstSearch(std::vector<translator::ast::Group*> children)
            {
                for(translator::ast::Group* child : children)
                {
                    //std::string test = hierarchy[0]->getDisplayName();
                    delete currentBuilder;
                    currentBuilder = construct(child->getType().getValue());
                    hierarchy[depth] = currentBuilder->buildGroup(child, hierarchy[depthMinusOne]);
                    depth++;
                    depthMinusOne++;
                    depthFirstSearch(child->getChildren());
                    //hierarchy[depth] = nullptr;
                    depth--;
                    depthMinusOne--;
                }
            }

            /**
             * operation to construct a precise builder
             * @param std::string name of builder wanted
             * @return GroupBuilder* the wanted builder
             */
            GroupBuilder* BuildDirector::construct(std::string builder)
            {
                if(builder == "library")
                    return new LibraryBuilder(getModel());
                else if(builder == "output_voltage")
                    return new OutputVoltageBuilder(getModel());
                else if(builder == "input_voltage")
                    return new InputVoltageBuilder(getModel());
                else if(builder == "operating_conditions")
                    return new OperatingConditionsBuilder(getModel());
                else if(builder == "lu_table_template")
                    return new LuTableTemplateBuilder(getModel());
                else if(builder == "power_lut_template")
                    return new PowerLutTemplateBuilder(getModel());
                else if(builder == "normalized_driver_waveform")
                    return new NormalizedDriverWaveformBuilder(getModel());
                else if(builder == "cell")
                    return new CellBuilder(getModel());
                else if(builder == "latch")
                    return new LatchBuilder(getModel());
                else if(builder == "leakage_power")
                    return new LeakagePowerBuilder(getModel());
                else if(builder == "pin")
                    return new PinBuilder(getModel());
                else if(builder == "internal_power")
                    return new InternalPowerBuilder(getModel());
                else if(builder == "ff")
                    return new FlipFlopBuilder(getModel());
                else if(builder == "timing")
                    return new TimingBuilder(getModel());
                else if(builder == "fall_transition" || builder == "rise_transition")
                    return new TransitionTableBuilder(getModel());
                else if(builder == "ocv_sigma_cell_fall" || builder == "ocv_sigma_cell_rise" || builder == "ocv_sigma_fall_constraint" || builder == "ocv_sigma_rise_constraint")
                    return new OcvSigmaCellBuilder(getModel());
                else if(builder == "ocv_sigma_fall_transition" || builder == "ocv_sigma_rise_transition")
                    return new OcvSigmaTransitionBuilder(getModel());
                else if(builder == "rise_power" || builder == "fall_power" || builder == "cell_fall" || builder == "cell_rise" || builder == "rise_constraint" || builder == "fall_constraint")
                    return new BasicTableBuilder(getModel());

                return new GroupBuilder(getModel());
            }
        }
    }
}
