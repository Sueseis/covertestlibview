/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * BasicTableBuilder implementation
  *
  * Builder class for the BasicTable class in the data structure.
  */

#include "BasicTableBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            //BasicTableBuilder::BasicTableBuilder() : basicTable(new BasicTable)
            //{}

            BasicTableBuilder::BasicTableBuilder(ModelFacade* pFacade) : 
                GroupBuilder(pFacade),
                basicTable(new BasicTable(pFacade))
            {}

            void BasicTableBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                basicTable->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void BasicTableBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "index_1")
                {                  
                    basicTable->setIndex_1(to1DArray(value));
                    return;
                }
                else if(name == "index_2")
                {
                    basicTable->setIndex_2(to1DArray(value));
                    return;
                }
                else if(name == "index_3")
                {
                    basicTable->setIndex_3(to1DArray(value));
                    return;
                }
                
                else if(name == "values")
                {
                    IndexTemplate* indexTemplate = getTemplate(basicTable);
                    if(indexTemplate->getIndex_3() != nullptr)
                    {
                        size_t columns = indexTemplate->getDimensionOfIndex_1();
                        size_t rows = indexTemplate->getDimensionOfIndex_2();
                        size_t  height = indexTemplate->getDimensionOfIndex_3();
                        basicTable->setValues(to3DArray(columns, rows, height, value));
                    }
                    else if(indexTemplate->getIndex_2() != nullptr)
                    {
                        size_t columns = indexTemplate->getDimensionOfIndex_1();
                        size_t rows = indexTemplate->getDimensionOfIndex_2();
                        basicTable->setValues(to2DArray(columns, rows, value));
                    }
                    else if(indexTemplate->getIndex_1() != nullptr)
                    {
                        basicTable->setValues(to1DArray(value));
                    }
                    return;
                }
                else if(name == "valuesscalar")
                {
                    basicTable->setValues(to1DArray(value));
                    return;
                }
                
                basicTable->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
              LibertyFileGroup* BasicTableBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
              {
                  basicTable->setParent(parent);
                  std::string path = constructGroupPath(group, parent);
                  basicTable->setPath(path);

                  std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                  path = basicTable->getPath();

                  //updateAttributeHolder(path, addedAttributes);
                  getModel()->addAttribute(path);
                  // Temporary patch ( insert children into parent group)

                  if(parent != nullptr)
                      parent->setChild(basicTable);


                  return basicTable;
              }


        }
    }
}