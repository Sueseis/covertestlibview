/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * LeakagePowerBuilder implementation
  *
  * Builder class for the LeakagePower class in the data structure.
  */

#include "LeakagePowerBuilder.h"
#include "LibView/model/ModelFacade.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            LeakagePowerBuilder::LeakagePowerBuilder(ModelFacade* pFacade) :
                GroupBuilder(pFacade),
                leakagePower(new LeakagePower(pFacade))
            {
            }

            void LeakagePowerBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "when")
                {
                    leakagePower->setWhen(expressionToString(value));
                    return;
                }
                else if(name == "value")
                {
                    leakagePower->setValue(toDouble(value.at(1)));
                    return;
                }

                leakagePower->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void LeakagePowerBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                leakagePower->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* LeakagePowerBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                leakagePower->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                leakagePower->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = leakagePower->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)
                if(parent != nullptr)
                    parent->setChild(leakagePower);


                return leakagePower;
            }
        }
    }
}