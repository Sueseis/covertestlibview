/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * LuTableTemplateBuilder implementation
  *
  * Builder class for the LuTableTemplate class in the data structure.
  */

#include "LuTableTemplateBuilder.h"
#include "LibView/model/ModelFacade.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            LuTableTemplateBuilder::LuTableTemplateBuilder(ModelFacade* pFacade) :
                GroupBuilder(pFacade),
                lutTemplate(new LuTableTemplate(pFacade))
            {
            }

            void LuTableTemplateBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "variable_1")
                {
                    lutTemplate->setVariable_1(stringOfFirstToken(value));
                    return;
                }
                else if(name == "variable_2")
                {
                    lutTemplate->setVariable_2(stringOfFirstToken(value));
                    return;
                }
                else if(name == "variable_3")
                {
                    lutTemplate->setVariable_3(stringOfFirstToken(value));
                    return;
                }

                lutTemplate->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            void LuTableTemplateBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "index_1")
                {
                    lutTemplate->setIndex_1(to1DArray(value));
                    return;
                }
                else if(name == "index_2")
                {
                    lutTemplate->setIndex_2(to1DArray(value));
                    return;
                }
                else if(name == "index_3")
                {
                    lutTemplate->setIndex_3(to1DArray(value));
                    return;
                }

                lutTemplate->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* LuTableTemplateBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                lutTemplate->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                lutTemplate->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = lutTemplate->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(lutTemplate);


                return lutTemplate;
            }
        }
    }
}