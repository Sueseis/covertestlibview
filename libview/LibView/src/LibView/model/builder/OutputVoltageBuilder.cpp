/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * OutputVoltageBuilder implementation
  *
  * Builder class for the OutputVoltage class in the data structure.
  */

#include "OutputVoltageBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            OutputVoltageBuilder::OutputVoltageBuilder(ModelFacade* pFacade) :
                GroupBuilder(pFacade),
                outputVoltage(new OutputVoltage(pFacade))
            {}

            void OutputVoltageBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "vol")
                {
                    outputVoltage->setVol(expressionToString(value));
                    return;
                }
                else if(name == "voh")
                {
                    outputVoltage->setVoh(expressionToString(value));
                    return;
                }
                else if(name == "vomin")
                {
                    outputVoltage->setVomin(expressionToString(value));
                    return;
                }
                else if(name == "vomax")
                {
                    outputVoltage->setVomax(expressionToString(value));
                    return;
                }

                outputVoltage->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void OutputVoltageBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {

                outputVoltage->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }
            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* OutputVoltageBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                outputVoltage->setParent(parent);

                outputVoltage->setParent(parent);
                std::string path = constructGroupPath(group, parent);
                outputVoltage->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = outputVoltage->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(outputVoltage);


                return outputVoltage;
            }
        }
    }
}