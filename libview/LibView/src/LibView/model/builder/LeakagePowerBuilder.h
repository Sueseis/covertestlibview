#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "GroupBuilder.h"
#include "LibView/model/data_structure/LeakagePower.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class LeakagePowerBuilder : public GroupBuilder
            {
            public:
                //LeakagePowerBuilder();
                LeakagePowerBuilder(ModelFacade* pFacade);

                /**
                 * @param group
                 * @param parent
                 */
                LibertyFileGroup* buildGroup(translator::ast::Group* group, LibertyFileGroup* parent) override;
            private:
                LeakagePower* leakagePower;

                void setSimpleAttribute(std::string name, std::vector<translator::token::Token> value) override;

                void setComplexAttribute(std::string name, std::vector<translator::token::Token> value) override;
            };
        }
    }
}