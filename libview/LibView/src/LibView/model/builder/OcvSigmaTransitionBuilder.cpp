/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * OcvSigmaTransitionBuilder implementation
  *
  * Builder class for the OcvSigmaTransition class in the data structure.
  */

#include "OcvSigmaTransitionBuilder.h"
#include "LibView/model/ModelFacade.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            OcvSigmaTransitionBuilder::OcvSigmaTransitionBuilder(ModelFacade* pFacade) :
                GroupBuilder(pFacade),
                ocvSigmaTransition(new OcvSigmaTransition(pFacade))
            {
            }

            void OcvSigmaTransitionBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "sigma_type")
                {
                    ocvSigmaTransition->setSigma_type(stringOfFirstToken(value));
                    return;
                }

                ocvSigmaTransition->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void OcvSigmaTransitionBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "index_1")
                {
                    ocvSigmaTransition->setIndex_1(to1DArray(value));
                    return;
                }
                else if(name == "index_2")
                {
                    ocvSigmaTransition->setIndex_2(to1DArray(value));
                    return;
                }
                else if(name == "index_3")
                {
                    ocvSigmaTransition->setIndex_3(to1DArray(value));
                    return;
                }
                
                else if(name == "values")
                {
                    IndexTemplate* indexTemplate = getTemplate(ocvSigmaTransition);
                    if(indexTemplate->getIndex_3() != nullptr)
                    {
                        size_t columns = indexTemplate->getDimensionOfIndex_1();
                        size_t rows = indexTemplate->getDimensionOfIndex_2();
                        size_t  height = indexTemplate->getDimensionOfIndex_3();
                        ocvSigmaTransition->setValues(to3DArray(columns, rows, height, value));
                    }
                    else if(indexTemplate->getIndex_2() != nullptr)
                    {
                        size_t columns = indexTemplate->getDimensionOfIndex_1();
                        size_t rows = indexTemplate->getDimensionOfIndex_2();
                        ocvSigmaTransition->setValues(to2DArray(columns, rows, value));
                    }
                    else if(indexTemplate->getIndex_1() != nullptr)
                    {
                        ocvSigmaTransition->setValues(to1DArray(value));
                    }
                    return;
                }
                else if(name == "intermediate_values")
                {
                    IndexTemplate* indexTemplate = getTemplate(ocvSigmaTransition);
                    if(indexTemplate->getIndex_3() != nullptr)
                    {
                        size_t columns = indexTemplate->getDimensionOfIndex_1();
                        size_t rows = indexTemplate->getDimensionOfIndex_2();
                        size_t  height = indexTemplate->getDimensionOfIndex_3();
                        ocvSigmaTransition->setIntermediate_values(to3DArray(columns, rows, height, value));
                    }
                    else if(indexTemplate->getIndex_2() != nullptr)
                    {
                        size_t columns = indexTemplate->getDimensionOfIndex_1();
                        size_t rows = indexTemplate->getDimensionOfIndex_2();
                        ocvSigmaTransition->setValues(to2DArray(columns, rows, value));
                    }
                    else if(indexTemplate->getIndex_1() != nullptr)
                    {
                        ocvSigmaTransition->setIntermediate_values(to1DArray(value));
                    }
                    return;
                }
                else if(name == "valuesscalar")
                {
                    ocvSigmaTransition->setValues(to1DArray(value));
                    return;
                }
                else if(name == "intermediate_valuesvaluesscalar")
                {
                    ocvSigmaTransition->setValues(to1DArray(value));
                    return;
                }
                ocvSigmaTransition->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* OcvSigmaTransitionBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                ocvSigmaTransition->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                ocvSigmaTransition->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = ocvSigmaTransition->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(ocvSigmaTransition);


                return ocvSigmaTransition;
            }
        }
    }
}