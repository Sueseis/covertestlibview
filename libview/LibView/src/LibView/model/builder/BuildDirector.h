﻿#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */
#include <iostream>
#include <array>

#include "LibView/model/translator/ast/Group.h"
#include "LibView/model/data_structure/LibertyFileData.h"
#include "LibView/model/translator/Translator.h"
#include "LibView/model/AttributeHolder.h"


#include "LibraryBuilder.h"
#include "OutputVoltageBuilder.h"
#include "InputVoltageBuilder.h"
#include "PinBuilder.h"
#include "LuTableTemplateBuilder.h"
#include "PowerLutTemplateBuilder.h"
#include "OcvSigmaCellBuilder.h"
#include "BasicTableBuilder.h"
#include "TransitionTableBuilder.h"
#include "OcvSigmaTransitionBuilder.h"
#include "OperatingConditionsBuilder.h"
#include "NormalizedDriverWaveformBuilder.h"
#include "CellBuilder.h"
#include "LeakagePowerBuilder.h"
#include "FlipFlopBuilder.h"
#include "TimingBuilder.h"
#include "InternalPowerBuilder.h"
#include "LatchBuilder.h"

//#include "LibView/model/ModelFacade.h"


#include "LibView/model/ModelFacadeKnower.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class BuildDirector : public ModelFacadeKnower
            {
            public:
                
                BuildDirector(ModelFacade* facade = nullptr);
                ~BuildDirector();

                /**
                 * Director operation for parsing. The Operation decides on what builder is needed to build the specific group of the data structure . Then it calls the construct operation to get the specific builder and uses it.
                 * @param treeRoot
                 */
                LibertyFileData* loadLibertyFile(const std::string& path);

            private:
                /**
                 * operation to construct a precise builder
                 * @param builder
                 */
                GroupBuilder* construct(std::string builder);

                GroupBuilder* currentBuilder;

                // Allocate 64 hierarchy levels to account for custom arbitrary libFile content standards
                //                                                                         .... and our gross incompetence
                std::array<LibertyFileGroup*, 64> hierarchy;

                size_t depth;

                size_t depthMinusOne;

                LibertyFileData* libFileData;

                void depthFirstSearch(std::vector<translator::ast::Group*> children);

                //AttributeHolder* attributeHolder;

                // ModelFacade* m_Model;
            };
        }
    }
}