/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * CellBuilder implementation
  *
  * Builder class for the Cell class in the data structure.
  */

#include "CellBuilder.h"
#include "LibView/model/ModelFacade.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            CellBuilder::CellBuilder(ModelFacade* pFacade) : 
                GroupBuilder(pFacade),
                cell(new Cell(pFacade))
            {
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* CellBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                cell->setParent(parent);
                std::string path = constructGroupPath(group, parent);
                cell->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = cell->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);

                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(cell);


                return cell;
            }

            void CellBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "cell_leakage_power")
                {
                    cell->setCell_leakage_power(toDouble(value.front()));
                    return;
                }

                cell->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }
            void CellBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                cell->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }
        }
    }
}