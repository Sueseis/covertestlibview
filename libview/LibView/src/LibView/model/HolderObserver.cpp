/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "HolderObserver.h"

 /**
  * HolderObserver implementation
  *
  * The concrete Observer of the Observer pattern.
  * Observes the AttributeHolder and the LibFileHolder and creates a new model state  if something changes.
  */


namespace LibView
{
    namespace model
    {

        HolderObserver::HolderObserver(
            ModelFacade* modelFacade,
            ModelState* modelState,
            AttributeHolder* attributeHolder,
            LibFileHolder* libFileHolder)
        {
            m_pModelFacade = modelFacade;
            m_pModelState = modelState;
            m_pAttributeHolder = attributeHolder;
            m_pLibFileHolder = libFileHolder;

            m_pAttributeHolder->attach(this);
            m_pLibFileHolder->attach(this);
        }

        void HolderObserver::update()
        {
            //m_pModelState->updateLastChangedAttributes(getIndiciesFromPairs(m_pAttributeHolder->getLastAdded()), getIndiciesFromPairs(m_pAttributeHolder->getLastRemoved()));

            //m_pModelState->updateLastChangedLibFiles(getIndiciesFromPairs(m_pLibFileHolder->getLastAdded()), getIndiciesFromPairs(m_pLibFileHolder->getLastRemoved()));
        }
        std::vector<index_t> HolderObserver::getIndiciesFromPairs(std::vector<path_index_pair> pairList)
        {
            std::vector<index_t> indicies;

            for(path_index_pair pair : pairList)
            {
                indicies.push_back(pair.getSecond());
            }
            return indicies;
        }
    }
}