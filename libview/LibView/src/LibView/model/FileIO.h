#pragma once

#include <string>

namespace LibView
{
    namespace model
    {

        /**
         * Basic File I/O interaction class.
         */
        class FileIO
        {
        public:

            /**
             * @param[in] path (std::string) : The path to a File on disk.
             *
             * @return std::string : A String of the files contents.
             * 
             * @throws exception::FileOpenException on file open error.
             * @throws exception::FileCloseException on file close error.
             */
            static std::string readFile(std::string path);


            /**
             * @param[in] path (const char*) : The path to a File on disk.
             *
             * @return std::string : A String of the files contents.
             *
             * @throws exception::FileOpenException on file open error.
             * @throws exception::FileCloseException on file close error.
             */
            static std::string readFile(const char* path);

        };
    }
}