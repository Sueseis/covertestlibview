#pragma once

#include "AHolder.h"
#include "data_structure/LibertyFileData.h"
#include "AttributeHolder.h"
#include "LibView/presenter/IMergeTemplate.h"
#include "data_structure/LibertyFileData.h"
#include "LibFileLoader.h"
#include <vector>
#include "ModelFacade_decl.h"


namespace LibView
{
    namespace model
    {
        /**
         * With AHolder<index_t (index of LibFile), std::string (name of LibFile)>
         *
         * Is subject of the Observer Pattern.
         * Holds all currently loaded LibFileObjects.
         * Manipulates the AttributeHolder.
         * Can sort a set of LibFileObjects.
         */
        class LibFileHolder : public AHolder
        {
            friend class ModelFacade;

        public:

            ~LibFileHolder();

            LibFileHolder(ModelFacade* facade);

            /**
             * Used when loadLibertyFiles is done.
             * Motifies the observer.
             */
            void notify() override;

            /**
             * Returns a list of all LibFiles currently loaded 
             * @return std::vector<Lib>
             */
            std::vector<data_structure::LibertyFileData*> getLibFileList();


            /**
             * returns a pointer to a lib file with given index
             */
            data_structure::LibertyFileData* getLibFileByIndex(const index_t& index) const;



            /**
             * Gets all children indicies for a given group index
             *
             * @param index (index_t) : Index of parent group
             *
             * @return std::vector<index:index_t> : Return list of all indicies of the children
             */
            std::vector<index_t> getChildren(const index_t& index);

            /** 
             * Calculates all delta values for a given LibFile
             * 
             * @param libFile (data_structure::LibertyFileData*) : LibertyFileData to calculate deltas on
             */
            void calculateAllDelta(data_structure::LibertyFileData* libFile);

            /**
             * Merges two LibFiles using the Merge class on a given template
             *
             * @param template (presenter::IMergeTemplate*) : Merge template to be applied
             * @param libFileOne (index_t) : Index of first LibFile
             * @param libFileTwo (index_t) : Index of second LibFile
             */
            void mergeLibertyFiles(presenter::IMergeTemplate* tpl, const index_t& libFileOne, const index_t& libFileTwo);

            /**
             * Gets the difference between 

             * @param libFileIndex
             * @param attributeIndex
             *
             * @return double
             */
            double getDelta(const index_t& libFileIndex, const index_t& attributeIndex);

            /**
             * return a pointer to the attributeholder
             */
            inline AttributeHolder* getAttributeHolder() { return m_AttributeHolder; }

        protected:

            /**
             * Loops load libertyfile for each path given.
             * Calls notify when finished.
             *
             * @param filePathList (std::vector<std::string>) : List of file paths
             */
            void loadLibertyFiles(const std::vector<std::string>& filePathList);

            /**
             * Removes a given liberty file from the program.
             * Add liberty file to last removed.
             * Calls regenerateAttributes.
             *
             * @param index
             */
            void removeLibertyFile(const index_t& index);
            
        private:

            /**
             * decreases the multiplicity of each attribute occuring in the file.
             */
            void regenerateAttributes();

            /**
             * Loads a LibFile from a given path. 
             * Add file to last added.
             *
             * @param pathList
             */
            void loadLibertyFile(const std::string& path);

        private: 

            AttributeHolder* m_AttributeHolder;
            std::vector<data_structure::LibertyFileData*> m_LibFiles;
            LibFileLoader m_LibFileLoader;
            ModelFacade* m_ModelFacade = nullptr;
        };
    }
}