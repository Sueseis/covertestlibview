/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "LibFileUtils.h"

/**
 * LibFileUtils implementation
 * 
 * Allows Operations on the loaded LibFiles
 */

namespace LibView
{
    namespace model
    {
        /**
         * sorts all LibFiles according to a sorting parameter.
         * returns a sorted list of the libFiles.
         * @param sortingParameter
         * @param allLibFiles
         * @param ascending
         * @return std::vector<index: int64_t>
         */
        std::vector<int64_t> LibFileUtils::sortLibFileListAccordingTo(SortingParameter param, std::vector<int64_t> allLibFiles, bool ascending)
        {
            return std::vector<int64_t>(); // TODO FIX
        }

        /**
         * returns a list of all attributes that match a given search term.
         * iterates all attributes and if it matches put the attribute in the list.
         * @param searchTerm
         * @return std::vector<index: int64_t>
         */
        std::vector<int64_t> LibFileUtils::getMatchingAttributesList(std::string searchTerm)
        {
            return std::vector<int64_t>(); // TODO FIX
        }

        /**
         * methode to calculate the aberrations beetween two parameters
         * @param param1
         * @param param2
         * @return double
         */
        double LibFileUtils::calculateDleta(double param1, double param2)
        {
            return 0.0;
        }
    }
}