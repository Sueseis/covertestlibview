#pragma once
#include <map>


namespace LibView
{
    namespace model
    {
        /**
         * Templated std::map wrapper for the LibView Project. This class simplifies map interaction by limiting functionality to basic interactions.
         *
         * @tparam typename T_TypeKey : The type of the key indices
         * @tparam typename T_TypeValue : The type of the key indexed values
         */
        template<typename T_TypeKey, typename T_TypeValue>
        class LibViewMap
        {
        private:

            /**
             * The adapted instance of std::map
             */
            std::map<T_TypeKey, T_TypeValue> m_Map;


        public:

            /**
             * The default constructor. Initializes the adapted map.
             */
            LibViewMap() : m_Map(std::map<T_TypeKey, T_TypeValue>()){};

            /**
             * The element access operator. Maps to std::map::operator[]
             *
             * @param const T_TypeKey& key : The key to a value
             * @return T_TypeValue& : The value at a given key
             */
            T_TypeValue& operator[](const T_TypeKey& key)
            {
                return m_Map[key];
            }


            /**
             * The element access operator. Maps to std::map::operator[]
             *
             * @param const T_TypeKey& key : The key to a value
             * @return T_TypeValue& : The value at a given key
             */
            T_TypeValue& operator[](T_TypeKey&& key)
            {
                return m_Map[key];
            }

            /**
             * The element removal operator. Maps to std::map::erase
             *
             * @param const T_TypeKey& key : The key to a value
             */
            void remove(T_TypeKey& key)
            {
                m_Map.erase(key);
            }



        };

    }
}