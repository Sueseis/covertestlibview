#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "AObserver.h"
#include "HeldPair.h"
#include "LibViewPair.h"
#include <vector>
#include <set>
#include "LibView/core/LibFileIndex.h"

 /**
  * AHolder implementation
  *
  * The Abstract Subject for the ObserverPattern
  */



namespace LibView
{
    namespace model
    {
        class AHolder
        {
        public:

            /**
             * Attaches an observer to the class
             *
             * @param observer (AObserver*) : Observer to attach
             */
            void attach(AObserver* observer);

            /**
             * Detaches a observer from the class.
             *
             * @param observer (AObserver*) : Observer to detach
             */
            void detach(AObserver* observer);

            /**
             * Notifies all observers of an update
             *
             * For all o in m_Observer:
             * o->update()
             */
            virtual void notify() = 0;

            /**
             * Empties the last added/removed list after the model state is updated.
             */
            void clearLastStoredItems();

            /**
             * Adds a LibViewPair to the last added list
             *
             * @param pair (path_index_pair) : Pair of path and index
             */
            void addToLastAdded(path_index_pair pair);

            /**
             * clear list of last added elements
             */
            void resetLastAdded();
            
            /**
             * clear list of last removed elements
             */
            void resetLastRemoved();

            /**
             * Adds a LibViewPair to the last removed list
             *
             * @param pair (path_index_pair) : Pair of path and index
             */
            void addToLastRemoved(path_index_pair pair);

            /**
             * Returns all last added LibViewPair s
             *
             * @return std::vector<path_index_pair> : List of LibViewPair, pair of path and index
             */
            std::vector<path_index_pair> getLastAdded() const;

            /**
             * Returns all last removed LibViewPair s
             *
             * @return std::vector<LibViewPair<K, J>> : List of LibViewPair, pair of path and index
             */
            std::vector<path_index_pair> getLastRemoved() const;

        protected:

            std::set<AObserver*> m_Observers;

        private:
            std::vector<path_index_pair> m_LastRemoved;
            std::vector<path_index_pair> m_LastAdded;
        };
    }

}