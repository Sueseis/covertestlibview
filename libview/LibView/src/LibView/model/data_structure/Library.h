#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"
#include "CapacitiveLoadUnitAttribute.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {

            class Library : public LibertyFileGroup
            {
            public:

                Library(ModelFacade* pModel);
                ~Library();

                std::string getDelay_module();

                /**
                 * @param value
                 */
                void setDelay_module(std::string value);

                std::vector<std::string> getLibrary_features();

                /**
                 * @param value
                 */
                void setLibrary_features(std::vector<std::string> value);

                std::string getTime_unit();

                /**
                 * @param value
                 */
                void setTime_unit(std::string value);

                std::string getVoltage_unit();

                /**
                 * @param value
                 */
                void setVoltage_unit(std::string value);

                std::string getCurrent_unit();

                /**
                 * @param value
                 */
                void setCurrent_unit(std::string value);

                std::string getPulling_resistance_unit();

                /**
                 * @param value
                 */
                void setPulling_resistance_unit(std::string value);

                CapacitiveLoadUnitAttribute* getCapacitive_load_unit();

                /**
                 * @param doublePart
                 * @param enumPart
                 */
                void setCapacitive_load_unit(double doublePart, std::string enumPart);

                std::string getLeakage_power_unit();

                /**
                 * @param value
                 */
                void setLeakage_power_unit(std::string value);

                double getInput_threshold_pct_fall();

                /**
                 * @param value
                 */
                void setInput_threshold_pct_fall(double value);

                double getInput_threshold_pct_rise();

                /**
                 * @param value
                 */
                void setInput_threshold_pct_rise(double value);

                double getOutput_threshold_pct_fall();

                /**
                 * @param value
                 */
                void setOutput_threshold_pct_fall(double value);

                double getOutput_threshold_pct_rise();

                /**
                 * @param value
                 */
                void setOutput_threshold_pct_rise(double value);

                double getSlew_lower_threshold_pct_fall();

                /**
                 * @param value
                 */
                void setSlew_lower_threshold_pct_fall(double value);

                double getSlew_lower_threshold_pct_rise();

                /**
                 * @param value
                 */
                void setSlew_lower_threshold_pct_rise(double value);

                double getSlew_upper_threshold_pct_fall();

                /**
                 * @param value
                 */
                void setSlew_upper_threshold_pct_fall(double value);

                double getSlew_upper_threshold_pct_rise();

                /**
                 * @param value
                 */
                void setSlew_upper_threshold_pct_rise(double value);

                double getSlew_derate_from_library();

                /**
                 * @param value
                 */
                void setSlew_derate_from_library(double value);

                double getNom_process();

                /**
                 * @param value
                 */
                void setNom_process(double value);

                double getNom_temperature();

                /**
                 * @param value
                 */
                void setNom_temperature(double value);

                double getNom_voltage();

                /**
                 * @param value
                 */
                void setNom_voltage(double value);

                double getDefault_cell_leakage_power();

                /**
                 * @param value
                 */
                void setDefault_cell_leakage_power(double value);

                double getDefault_fanout_load();

                /**
                 * @param value
                 */
                void setDefault_fanout_load(double value);

                double getDefault_inout_pin_cap();

                /**
                 * @param value
                 */
                void setDefault_inout_pin_cap(double value);

                double getDefault_input_pin_cap();

                /**
                 * @param value
                 */
                void setDefault_input_pin_cap(double value);

                double getDefault_leakage_power_density();

                double getDefault_output_pin_cap();

                /**
                 * @param value
                 */
                void setDefault_output_pin_cap(double value);

                /**
                 * @param value
                 */
                void setDefault_leakage_power_density(double value);

            private:
                std::string delay_module;

                bool isSetLibrary_features;

                std::vector<std::string> library_features;

                std::string time_unit;

                std::string voltage_unit;

                std::string current_unit;

                std::string pulling_resistance_unit;

                bool isSetCapacitive_load_unit;

                CapacitiveLoadUnitAttribute capacitive_load_unit;

                std::string leakage_power_unit;

                bool isSetInput_threshold_pct_fall;

                double input_threshold_pct_fall;

                bool isSetInput_threshold_pct_rise;

                double input_threshold_pct_rise;

                bool isSetOutput_threshold_pct_fall;

                double output_threshold_pct_fall;

                bool isSetOutput_threshold_pct_rise;

                double output_threshold_pct_rise;

                bool isSetSlew_lower_threshold_pct_fall;

                double slew_lower_threshold_pct_fall;

                bool isSetSlew_lower_threshold_pct_rise;

                double slew_lower_threshold_pct_rise;

                bool isSetSlew_upper_threshold_pct_fall;

                double slew_upper_threshold_pct_fall;

                bool isSetSlew_upper_threshold_pct_rise;

                double slew_upper_threshold_pct_rise;

                bool isSetSlew_derate_from_library;

                double slew_derate_from_library;

                bool isSetNom_process;

                double nom_process;

                bool isSetNom_temperature;

                double nom_temperature;

                bool isSetNom_voltage;

                double nom_voltage;

                bool isSetDefault_cell_leakage_power;

                double default_cell_leakage_power;

                bool isSetDefault_fanout_load;

                double default_fanout_load;

                bool isSetDefault_inout_pin_cap;

                double default_inout_pin_cap;

                bool isSetDefault_input_pin_cap;

                double default_input_pin_cap;

                bool isSetDefault_output_pin_cap;

                double default_output_pin_cap;

                bool isSetDefault_leakage_power_density;

                double default_leakage_power_density;
            };
        }
    }
}