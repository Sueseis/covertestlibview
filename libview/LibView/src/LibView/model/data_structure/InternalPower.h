#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class InternalPower : public LibertyFileGroup {
            public:
                InternalPower(ModelFacade* pModel);
                ~InternalPower();

                std::string getRelated_pin();

                void setRelated_pin(std::string);

            private:

                std::string related_pin;
            };
        }
    }
}