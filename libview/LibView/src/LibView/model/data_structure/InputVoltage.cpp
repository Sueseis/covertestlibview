/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * InputVoltage implementation
  *
  * class that contains attributes and the according getter and setter for the input_voltage group
  */

#include "InputVoltage.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            InputVoltage::InputVoltage(ModelFacade* pModel)
                : 
                LibertyFileGroup(pModel),
                isSetVil(false),
                isSetVih(false),
                isSetVimin(false),
                isSetVimax(false)
            {
                library_parent = (Library*) m_parent;
            }

            
            InputVoltage::~InputVoltage()
            {}



            /**
             * @return double
             */
            VoltageExpression* InputVoltage::getVil()
            {
                return &vil;
            }

            /**
             * @param value
             */
            void InputVoltage::setVil(std::string value)
            {
                isSetVil = true;
                orderedAttributes.push_back("vil");
                orderedAttributeValues.push_back(value);
                vil.setVoltageExpression(value, getScale(library_parent->getVoltage_unit()));
            }

            void InputVoltage::setParent(LibertyFileGroup* parent)
            {
                library_parent = (Library*) parent;
                m_parent = parent;
            }

            /**
             * @return double
             */
            VoltageExpression* InputVoltage::getVih()
            {
                return &vih;
            }

            /**
             * @param value
             */
            void InputVoltage::setVih(std::string value)
            {
                orderedAttributes.push_back("vih");
                orderedAttributeValues.push_back(value);
                isSetVih = true;
                vih.setVoltageExpression(value, getScale(library_parent->getVoltage_unit()));
            }

            /**
             * @return double
             */
            VoltageExpression* InputVoltage::getVimin()
            {
                return &vimin;
            }

            /**
             * @param value
             */
            void InputVoltage::setVimin(std::string value)
            {
                orderedAttributes.push_back("vimin");
                orderedAttributeValues.push_back(value);
                isSetVimin = true;
                vimin.setVoltageExpression(value, getScale(library_parent->getVoltage_unit()));
            }

            /**
             * @return double
             */
            VoltageExpression* InputVoltage::getVimax()
            {
                return &vimax;
            }

            /**
             * @param value
             */
            void InputVoltage::setVimax(std::string value)
            {
                orderedAttributes.push_back("vimax");
                orderedAttributeValues.push_back(value);
                isSetVimax = true;
                vimax.setVoltageExpression(value, getScale(library_parent->getVoltage_unit()));
            }

            double InputVoltage::getScale(std::string voltage_unit)
            {
                if(voltage_unit == "100mV")
                    return 0.1;
                else if(voltage_unit == "10mV")
                    return 0.01;
                else if(voltage_unit == "1mV")
                    return 0.001;
                else
                    return 1;
            }
        }
    }
}