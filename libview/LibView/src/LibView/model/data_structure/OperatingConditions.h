#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class OperatingConditions : public LibertyFileGroup {
            public:
                OperatingConditions(ModelFacade* pModel);
                ~OperatingConditions();

                double getProcess();

                /**
                 * @param value
                 */
                void setProcess(double value);

                double getTemperature();

                /**
                 * @param value
                 */
                void setTemperature(double value);

                double getVoltage();

                /**
                 * @param value
                 */
                void setVoltage(double value);

            private:
                bool isSetProcess;

                double process;

                bool isSetTemperature;

                double temperature;

                bool isSetVoltage;

                double voltage;
            };
        }
    }
}