#include "DynamicDimensionArray.h"

#include <memory>

namespace LibView
{
    namespace model
    {
        DynamicDimensionArray::DynamicDimensionArray()
            : m_Dimension(0),
            m_Size(0)
        {
            m_Type = Type::Scalar;
            m_Double = (double*) std::malloc(sizeof(double));
            *m_Double = 0.0;
        }


        DynamicDimensionArray::DynamicDimensionArray(size_t dimension)
            :
            m_Dimension(dimension),
            m_Size(0)
        {
            if(dimension == 0)
            {
                m_Type = Type::Scalar;
                m_Double = (double*) std::malloc(sizeof(double));
                *m_Double = 0.0;
            }
            else
            {
                m_Type = Type::Array;
                m_Array = new std::vector<DynamicDimensionArray*>();
            }
        }

        DynamicDimensionArray::DynamicDimensionArray(const DynamicDimensionArray& other)
            : m_Dimension(other.m_Dimension),
            m_Type(other.m_Type),
            m_Size(0)
        {
            if(m_Type == Type::Scalar)
            {
                m_Double = (double*) std::malloc(sizeof(double));
                *m_Double = other.get();
            }
            else
            {
                m_Array = new std::vector<DynamicDimensionArray*>();

                reserve(other.m_Size);

                for(size_t i = 0; i < m_Array->size(); i++)
                {
                    *(m_Array->at(i)) = other.getPosition(i);
                }
            }
        }



        DynamicDimensionArray::~DynamicDimensionArray()
        {
            if(m_Type == Type::Scalar)
            {
                delete m_Double;
            }
            else if(m_Type == Type::Array)
            {
                for(size_t i = 0; i < m_Array->size(); i++)
                {
                    delete m_Array->at(i);

                }
                delete m_Array;
            }
            else
            {
                throw - 1;
            }
        }



        DynamicDimensionArray& DynamicDimensionArray::reserve(size_t size)
        {
            if(m_Dimension == 0)
            {
                // TODO : Throw exception
                return *this;
            }

            m_Size += size;

            for(size_t i = 0; i < size; i++)
            {
                m_Array->push_back(new DynamicDimensionArray(m_Dimension - 1));
            }

            return *this;
        }



        DynamicDimensionArray& DynamicDimensionArray::operator<<(double value)
        {
            if(m_Dimension > 1 && m_Dimension == 0)
            {
                return *this;
            }

            DynamicDimensionArray* newElement = new DynamicDimensionArray(m_Dimension - 1);
            newElement->setScalar(value);
            m_Array->push_back(newElement);
            m_Size++;



            return *this;
        }



        void DynamicDimensionArray::setScalar(double value)
        {
            if(m_Type == Type::Scalar)
            {
                (*m_Double) = value;
            }

            else
            {

                throw - 1;
            }
        }



        double DynamicDimensionArray::get() const
        {
            if(m_Type == Type::Scalar)
            {
                return *m_Double;
            }


            throw - 1;
            // TODO Throw exception
        }



        DynamicDimensionArray& DynamicDimensionArray::operator[](size_t index)
        {
            if(m_Type == Type::Array)
            {
                // TODO check bounds
                return *(m_Array->at(index));
            }


            throw - 1;
            // TODO Throw exception
        }

        DynamicDimensionArray& DynamicDimensionArray::operator=(const DynamicDimensionArray& other)
        {
            if(m_Type == Type::Scalar)
            {
                std::free(m_Double);
            }
            else
            {
                for(size_t i = 0; i < m_Array->size(); i++)
                {
                    delete m_Array->at(i);

                }
                delete m_Array;
            }

            m_Dimension = other.m_Dimension;
            m_Type = other.m_Type;

            if(m_Type == Type::Scalar)
            {
                m_Double = (double*) std::malloc(sizeof(double));
                *m_Double = other.get();
            }
            else
            {
                m_Array = new std::vector<DynamicDimensionArray*>();

                reserve(other.m_Size);

                for(size_t i = 0; i < m_Array->size(); i++)
                {
                    *(m_Array->at(i)) = other.getPosition(i);
                }
            }

            return *this;
        }

        DynamicDimensionArray& DynamicDimensionArray::getPosition(size_t index) const
        {
            if(m_Type == Type::Array)
            {
                // TODO check bounds
                return *(m_Array->at(index));
            }


            throw - 1;
            // TODO Throw exception
        }

        std::string DynamicDimensionArray::toString() const
        {
            std::string result = "";
            result = toStringHelper();
            if(m_Dimension == 1)
            {
                result = result.substr(0, result.size() - 1);
            }
            else
            {
                result = result.substr(2, result.size() - 5);
            }
            return result;
        }

        std::string DynamicDimensionArray::toStringHelper() const
        {

            if(m_Type == Type::Scalar)
            {
                return std::to_string(*m_Double);
            }

            std::string result = "[ ";

            size_t length = m_Array->size();

            for(size_t i = 0; i < length; i++)
            {
                result += m_Array->at(i)->toStringHelper();
                if(m_Dimension < 2)
                    result += i == length - 1 ? "" : ", ";
            }

            result += " ]\n";

            return result;
        }



        size_t DynamicDimensionArray::getSize()
        {
            return m_Size;
        }
    }
}