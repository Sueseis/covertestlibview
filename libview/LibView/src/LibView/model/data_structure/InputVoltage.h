#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"
#include "VoltageExpression.h"
#include "LibView/model/data_structure/Library.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {

            class InputVoltage : public LibertyFileGroup {
            public:

                InputVoltage(ModelFacade* pModel);
                ~InputVoltage();

                VoltageExpression* getVil();

                /**
                 * @param value
                 */
                void setVil(std::string value);

                VoltageExpression* getVih();

                /**
                 * @param value
                 */
                void setVih(std::string value);

                VoltageExpression* getVimin();

                /**
                 * @param value
                 */
                void setVimin(std::string value);

                VoltageExpression* getVimax();

                /**
                 * @param value
                 */
                void setVimax(std::string value);

                void setParent(LibertyFileGroup*);

            private:

                double getScale(std::string);

                bool isSetVil;

                VoltageExpression vil;

                bool isSetVih;

                VoltageExpression vih;

                bool isSetVimin;

                VoltageExpression vimin;

                bool isSetVimax;

                VoltageExpression vimax;

                Library* library_parent;
            };
        }
    }
}