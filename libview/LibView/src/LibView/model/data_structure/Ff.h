#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class Ff : public LibertyFileGroup {
            public:
                Ff(ModelFacade* pModel);

                ~Ff();

                std::string getClocked_on();

                /**
                 * @param value
                 */
                void setClocked_on(std::string value);

                std::string getNext_state();

                /**
                 * @param value
                 */
                void setNext_state(std::string value);

                std::string getClear();

                /**
                 * @param value
                 */
                void setClear(std::string value);
                
                std::string getPreset();

                /**
                 * @param value
                 */
                void setPreset(std::string value);



            private:
                std::string clocked_on;

                std::string next_state;

                std::string clear;

                std::string preset;
            };
        }
    }
}