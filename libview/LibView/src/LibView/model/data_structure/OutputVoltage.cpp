/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * OutputVoltage implementation
  *
  * class that contains attributes and the according getter and setter for the output_voltage group
  */

#include "OutputVoltage.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            OutputVoltage::OutputVoltage(ModelFacade* pModel)
                : LibertyFileGroup(pModel),
                isSetVol(false),
                isSetVoh(false),
                isSetVomin(false),
                isSetVomax(false)
            {
                library_parent = (Library*) m_parent;
            }

            OutputVoltage::~OutputVoltage()
            {}

            /**
             * @return double
             */
            VoltageExpression* OutputVoltage::getVolDouble()
            {
                return &vol;
            }

            /**
             * @param value
             */
            void OutputVoltage::setVol(std::string value)
            {
                orderedAttributes.push_back("vol");
                orderedAttributeValues.push_back(value);
                isSetVol = true;
                vol.setVoltageExpression(value, getScale(library_parent->getVoltage_unit()));
            }

            /**
             * @return double
             */
            VoltageExpression* OutputVoltage::getVohDouble()
            {
                return &voh;
            }

            void OutputVoltage::setParent(LibertyFileGroup* parent)
            {
                library_parent = (Library*) parent;
                m_parent = parent;
            }

            /**
             * @param value
             */
            void OutputVoltage::setVoh(std::string value)
            {
                orderedAttributes.push_back("voh");
                orderedAttributeValues.push_back(value);
                isSetVoh = true;
                voh.setVoltageExpression(value, getScale(library_parent->getVoltage_unit()));
            }

            /**
             * @return double
             */
            VoltageExpression* OutputVoltage::getVominDouble()
            {
                return &vomin;
            }

            /**
             * @param value
             */
            void OutputVoltage::setVomin(std::string value)
            {
                orderedAttributes.push_back("vomin");
                orderedAttributeValues.push_back(value);
                isSetVomin = true;
                vomin.setVoltageExpression(value, getScale(library_parent->getVoltage_unit()));
            }

            /**
             * @return double
             */
            VoltageExpression* OutputVoltage::getVomaxDouble()
            {
                return &vomax;
            }

            /**
             * @param value
             */
            void OutputVoltage::setVomax(std::string value)
            {
                orderedAttributes.push_back("vomax");
                orderedAttributeValues.push_back(value);
                isSetVomax = true;
                vomax.setVoltageExpression(value, getScale(library_parent->getVoltage_unit()));
            }

            double OutputVoltage::getScale(std::string voltage_unit)
            {

                if(voltage_unit == "100mV")
                    return 0.1;
                else if(voltage_unit == "10mV")
                    return 0.01;
                else if(voltage_unit == "1mV")
                    return 0.001;
                else
                    return 1;
            }
        }
    }
}