#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include<string>
#include"LibertyFileGroup.h"

#include "LibView/core/LibFileIndex.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {

            class LibertyFileData
            {
            public:
                LibertyFileData(const std::string& filePath);
                LibertyFileData();
                ~LibertyFileData();

                std::string getDisplayName() const;

                LibertyFileGroup* getLibraryGroup() const;

                void setLibraryGroup(LibertyFileGroup*);

                index_t getLibFileIndex() const;

                std::string getFilePath();

                //void setFilePath(std::string);

                inline std::string getFileContents()
                {
                    return m_LibFileContents;
                }
                inline void setFileContents(const std::string& content)
                {
                    m_LibFileContents = content;
                };

            private:

                LibertyFileGroup* m_libraryGroup;

                std::string displayName;

                index_t libFileIndex;

                const std::string filePath;

                std::string assigneDisplayName();

                char forwardSlash = '/';

                char dot = '.';

                std::string empty = "";

                std::string m_LibFileContents;
            };
        }
    }
}