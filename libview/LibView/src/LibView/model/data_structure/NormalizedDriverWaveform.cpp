/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * NormalizedDriverWaveform implementation
  *
  * class that contains attributes and the according getter and setter for the normalized_driver_waveform group
  */

#include "NormalizedDriverWaveform.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            NormalizedDriverWaveform::NormalizedDriverWaveform(ModelFacade* pModel)
                : LibertyFileGroup(pModel),
                // Never use variables as initialiser values
                driver_waveform_name("")
            {}

            NormalizedDriverWaveform::~NormalizedDriverWaveform()
            {
                delete index_1;
                delete index_2;
                delete values;
            }

            /**
             * @return std::string
             */
            std::string NormalizedDriverWaveform::getDriver_waveform_name()
            {
                
                return driver_waveform_name;
            }

            /**
             * @param value
             */
            void NormalizedDriverWaveform::setDriver_waveform_name(std::string value)
            {
                orderedAttributes.push_back("driver_waveform_name");
                orderedAttributeValues.push_back(value);
                path.insert(path.find_last_of("(") + 1, "|" + value + "|");
                driver_waveform_name = value;
            }

            /**
             * @return DynamicDimensionArray
             */
            DynamicDimensionArray* NormalizedDriverWaveform::getIndex_1()
            {
                return index_1;
            }

            /**
             * @param value
             */
            void NormalizedDriverWaveform::setIndex_1(DynamicDimensionArray* value)
            {
                orderedAttributes.push_back("index_1");
                orderedAttributeValues.push_back(value->toString());
                index_1 = value;
            }

            /**
             * @return DynamicDimensionArray
             */
            DynamicDimensionArray* NormalizedDriverWaveform::getIndex_2()
            {
                return index_2;
            }

            /**
             * @param value
             */
            void NormalizedDriverWaveform::setIndex_2(DynamicDimensionArray* value)
            {
                orderedAttributes.push_back("index_2");
                orderedAttributeValues.push_back(value->toString());
                index_2 = value;
            }

            /**
             * @return DynamicDimensionArray
             */
            DynamicDimensionArray* NormalizedDriverWaveform::getValues()
            {
                return values;
            }

            /**
             * @param value
             */
            void NormalizedDriverWaveform::setValues(DynamicDimensionArray* value)
            {
                orderedAttributes.push_back("values");
                orderedAttributeValues.push_back(value->toString());
                values = value;
            }
        }
    }
}