/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

/**
 * GroupIterator implementation
 * 
 * Iterrator to iterate through the tree structure of the data
 */

#include "GroupIterator.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            GroupIterator::GroupIterator()
            {
            }

            void GroupIterator::next() 
            {

            }

            void GroupIterator::hasNext() {

            }
        }
    }
}