#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class GroupIterator {
            public:
                GroupIterator();

                void next();

                void hasNext();
            };
        }
    }
}