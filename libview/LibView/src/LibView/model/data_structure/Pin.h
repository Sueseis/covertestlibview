#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {

            class Pin : public LibertyFileGroup {
            public:
                Pin(ModelFacade* pModel);
                ~Pin();

                double getCapacitance();

                /**
                 * @param value
                 */
                void setCapacitance(double value);

                std::string getDirection();

                /**
                 * @param value
                 */
                void setDirection(std::string value);

                std::string getDriver_waveform_rise();

                /**
                 * @param value
                 */
                void setDriver_waveform_rise(std::string value);

                std::string getDriver_waveform_fall();

                /**
                 * @param value
                 */
                void setDriver_waveform_fall(std::string value);

                std::string getInput_voltage();

                /**
                 * @param value
                 */
                void setInput_voltage(std::string value);

                double getMax_capacitance();

                /**
                 * @param value
                 */
                void setMax_capacitance(double value);

                double getMin_capacitance();

                /**
                 * @param value
                 */
                void setMin_capacitance(double value);

                std::string getOutput_voltage();

                /**
                 * @param value
                 */
                void setOutput_voltage(std::string value);

                std::string getFunction();

                /**
                 * @param value
                 */
                void setFunction(std::string value);

                std::string getClock();

                void setClock(std::string value);
                
                std::string getThree_state();

                void setThree_state(std::string value);
                
                std::string getNextstate_type();

                void setNextstate_type(std::string value);

                double getMin_pulse_width_low();

                void setMin_pulse_width_low(double value);
                
                double getMin_pulse_width_high();

                void setMin_pulse_width_high(double value);

            private:
                bool isSetCapacitance;

                double capacitance;

                std::string direction;

                std::string driver_waveform_rise;

                std::string driver_waveform_fall;

                std::string input_voltage;

                std::string function;

                bool isSetMax_capacitance;

                double max_capacitance;

                bool isSetMin_capacitance;

                double min_capacitance;

                std::string output_voltage;

                std::string clock;

                double min_pulse_width_low;
                
                double min_pulse_width_high;

                std::string nextstate_type;
                
                std::string three_state;
            };
        }
    }
}