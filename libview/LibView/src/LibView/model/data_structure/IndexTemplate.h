#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"
#include "LibView/model/data_structure/DynamicDimensionArray.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class IndexTemplate : public LibertyFileGroup
            {
            public:
                IndexTemplate(ModelFacade* pModel);

                ~IndexTemplate();

                DynamicDimensionArray* getIndex_1();

                /**
                 * @param value
                 */
                void setIndex_1(DynamicDimensionArray* value);

                DynamicDimensionArray* getIndex_2();

                /**
                 * @param value
                 */
                void setIndex_2(DynamicDimensionArray* value);

                DynamicDimensionArray* getIndex_3();

                /**
                 * @param value
                 */
                void setIndex_3(DynamicDimensionArray* value);

                size_t getDimensionOfIndex_1();

                size_t getDimensionOfIndex_2();

                size_t getDimensionOfIndex_3();

            private:
                DynamicDimensionArray* index_1;

                DynamicDimensionArray* index_2;

                DynamicDimensionArray* index_3;
            };
        }
    }
}