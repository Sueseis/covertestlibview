/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * Ff implementation
  *
  * class that contains attributes and the according getter and setter for the Ff group
  */

#include "Ff.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            Ff::Ff(ModelFacade* pModel)
                : 
                LibertyFileGroup(pModel),
                // Never use variables as initialiser values
                clocked_on(""),
                next_state(""),
                clear("")
            {}

            Ff::~Ff()
            {}

            /**
             * @return std::string
             */
            std::string Ff::getClocked_on()
            {
                return clocked_on;
            }

            /**
             * @param value
             */
            void Ff::setClocked_on(std::string value)
            {
                orderedAttributes.push_back("clocked_on");
                orderedAttributeValues.push_back(value);
                clocked_on = value;
            }

            /**
             * @return std::string
             */
            std::string Ff::getNext_state()
            {
                return next_state;
            }

            /**
             * @param value
             */
            void Ff::setNext_state(std::string value)
            {
                orderedAttributes.push_back("next_state");
                orderedAttributeValues.push_back(value);
                next_state = value;
            }

            /**
             * @return std::string
             */
            std::string Ff::getClear()
            {
                return clear;
            }

            /**
             * @param value
             */
            void Ff::setClear(std::string value)
            {
                orderedAttributes.push_back("clear");
                orderedAttributeValues.push_back(value);
                clear = value;
            }
            std::string Ff::getPreset()
            {
                return preset;
            }
            void Ff::setPreset(std::string value)
            {
                orderedAttributes.push_back("preset");
                orderedAttributeValues.push_back(value);
                preset = value;
            }
        }
    }
}