#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "BasicTable.h"
#include "LibView/model/data_structure/DynamicDimensionArray.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class TransitionTable : public BasicTable {
            public:
                TransitionTable(ModelFacade* pModel);
                ~TransitionTable();

                DynamicDimensionArray* getIntermediate_values();

                /**
                 * @param value
                 */
                void setIntermediate_values(DynamicDimensionArray* value);

            private:

                DynamicDimensionArray* intermediate_values;
            };
        }
    }
}