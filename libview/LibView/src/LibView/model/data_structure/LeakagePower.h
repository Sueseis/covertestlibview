#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class LeakagePower : public LibertyFileGroup {
            public:
                LeakagePower(ModelFacade* pModel);
                ~LeakagePower();

                double getValue();

                /**
                 * @param value
                 */
                void setValue(double value);

                std::string getWhen();

                /**
                 * @param value
                 */
                void setWhen(std::string value);

            private:
                std::string when;

                bool isSetValue;

                double value;
            };
        }
    }
}