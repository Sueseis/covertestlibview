#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <string>

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {

            class VoltageExpression
            {
            public:

                VoltageExpression();
                ~VoltageExpression();

                VoltageExpression(std::string value, double);

                std::string getAsString();

                /**
                 * @param value
                 */
                void setVoltageExpression(std::string value, double);

                double getAsDouble();

            private:
                void calculateDoubleValue(double);

                std::string string;

                double doubleValue;

                std::string empty = "";
            };
        }
    }
}