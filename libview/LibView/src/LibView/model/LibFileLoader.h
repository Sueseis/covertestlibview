#pragma once


/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "data_structure/LibertyFileData.h"
#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacadeKnower.h"

namespace LibView
{
    namespace model
    {

        class LibFileLoader
        {

        private:

            data_structure::BuildDirector m_BuildDirector;

            //ModelFacade* m_modelFacade;

        public:

            LibFileLoader(ModelFacade* pFacade);

            /**
             * @param path
             *
             * Load lib file from a given path
             */
            data_structure::LibertyFileData* loadFromFile(const std::string& path);
        };

    }
}