#pragma once

#include <string>
#include <stdint.h>

namespace LibView
{
    namespace model
    {
        /**
         * (deprecated)
         */
        struct HeldPair
        {
        public:

            std::string path;
            int64_t index;

            HeldPair(std::string path, int64_t index);
        };
    }
}