#include "ModelFacadeKnower.h"

#include "ModelFacade.h"

namespace LibView
{
    namespace model
    {
        ModelFacadeKnower::ModelFacadeKnower(ModelFacade* pModel)
            :
            m_Model(pModel)
        
        {
        
        }
    }
}
