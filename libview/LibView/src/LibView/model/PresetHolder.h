#pragma once


#include <vector>
#include "APreset.h"


namespace LibView
{
    namespace model
    {
        class PresetHolder
        {
        private:

            std::vector<APreset*> m_Presets;

        public:


            PresetHolder();
            ~PresetHolder();

            /**
             * return list of loaded presets
             */
            inline const std::vector<APreset*>& getPresets() const { return m_Presets; }
        };
    }
}