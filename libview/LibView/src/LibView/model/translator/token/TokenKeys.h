#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <string>

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace token
			{
				/**
				* Enum of the allowed token types, the token "keys"
				*/
				enum class TokenKeys {
					Float = 0,
					FloatS,
					Integer,
					SpecialCharacter,
					Comment,
					Name,
					StringEnclosure, //"
					LeftParameterEnclosure, // (
					RightParameterEnclosure, // )
					LeftStatementEnclosure, //{
					RightStatementEnclosure, //}
					SeperatorAttributeValue, //' : '
					LeftFieldEnclosure, // [
					RightFieldEnlosure,// ]
					SeperatorFieldRange, //:
					AttributeCloser, //;
					NewLine, // \n
					Spacing, // ' '
					RuleEscape,// '\'
					ParameterSeperator, //,
					Tab,
					Operator //+, -, *, /
				};

				static const char* KeyName[] = {
					"Float",
					"Scientific Float",
					"Integer",
					"Comment",
					"Name",
					"StringEnclosure",
					"LeftParameterEnclosure",
					"RightParameterEnclosure",
					"LeftStatementEnclosure",
					"RightStatementEnclosure",
					"SeperatorAttributeValue",
					"LeftFieldEnclosure",
					"RightFieldEnlosure",
					"SeperatorFieldRange",
					"AttributeCloser",
					"NewLine",
					"Spacing",
					"RuleEscape",
					"ParameterSeperator"
				};

				inline std::string toString(TokenKeys key) { return KeyName[(int)key]; };
			}
		}
	}
}