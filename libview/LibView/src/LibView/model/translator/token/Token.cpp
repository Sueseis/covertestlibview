/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "Token.h"
namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace token
            {

                Token::Token(TokenKeys key, uint64_t row, uint64_t column, std::string word)
                    : m_Key(key), 
                    m_Row(row), 
                    m_Column(column), 
                    m_Value(word)
                {}


                Token::Token(const Token& other) 
                    :
                    m_Value(other.m_Value),
                    m_Column(other.m_Column),
                    m_Row(other.m_Row),
                    m_Key(other.m_Key)
                {}

                
                //not to be used
                Token::Token()
                    : m_Key(TokenKeys::Comment), m_Row(0), m_Column(0), m_Value("")
                {}
                
                TokenKeys Token::getKey() const
                {
                    return m_Key;
                }

                /**
                 * @return std::string
                 */
                std::string Token::getValue() const
                {
                    return m_Value;
                }

                /**
                 * @return int
                 */
                const uint64_t Token::getColumn() const
                {
                    return m_Column;
                }

                /**
                 * @return int
                 */
                const uint64_t Token::getRow() const
                {
                    return m_Row;
                }
                std::string Token::toString() const
                {
                    std::string description = "[Word]: " + m_Value + "    " + "[Type]: " + std::to_string((int) m_Key)
                    	+ "    " + "[Position: Row, Column]: " + std::to_string(m_Row) + ";" + std::to_string(m_Column);
                    return description;
                }
                bool Token::isEqual(Token other)
                {
                    return (other.getKey() == this->getKey() &&
                        other.getRow() == this->getRow() &&
                        other.getColumn() == this->getColumn() &&
                        other.getValue() == this->getValue());
                }
                Token& Token::operator=(const Token& other)
                {
                    m_Value = other.m_Value;
                    m_Column = other.m_Column;
                    m_Row = other.m_Row;
                    m_Key = other.m_Key;

                    return *this;
                }
            }
        }
    }
}