#pragma once

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace tokenizer
			{
				/**
				Structure defining all allowed types of transitions in the tokenisers state mashine.
				Abstracted transitions as integers.
				*/
				struct Transitions {

					//Not supported controle characters
					static const int NoSupportCC = 0;

					static const int Tab = 1;

					static const int LineFeed = 2;

					//VerticalTab

					//FormFeed,

					static const int CarriageReturn = 3;

					// NoSupportCC2,

					static const int Space = 4;

					static const int SpecialCharacter = 5; //ExclamationMark,

					static const int Quotes = 6;

					//SpecialCharacter1,

					static const int LeftBracket = 7;

					static const int RightBracket = 8;

					static const int Asterisk = 9;

					static const int Plus = 10;

					static const int Comma = 11;

					static const int Dash = 12;

					static const int FullStop = 13;

					static const int ForwardSlash = 14;

					static const int Numbers = 15;

					static const int Colon = 16;

					static const int Semicolon = 17;

					//SpecialCharacters2,

					static const int Letters = 18; //Captial

					static const int LeftSquareBracket = 19;

					static const int BackwardSlash = 20;

					static const int RightSquareBracket = 21;

					//SpecialCharacter3,

					//lower Letter

					static const int Letter_e = 22;

					static const int LeftCurlyBracket = 23;

					// Special CHaractzer4,

					static const int RightCurlyBracket = 24;

					static const int Underscore = 25;

					static const int SIZE = 26;
					//special Char

					//nosupportcc
				};
			}
		}
	}
}