#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */




#include "tokenize.h"
#include "Tokenizer.h"
#include "LibView/model/translator/token/Token.h"
#include "LibView/model/translator/token/TokenKeys.h"
#include "LibView/model/translator/reader/readFile.h"
#include "States.h"
#include "Transition.h"

 //exceptions
#include "LibView/exception/model/tokenizer/NoNextTokenAvailableException.h"
#include "LibView/exception/model/tokenizer/WordNotAllowedException.h"
#include "LibView/exception/model/tokenizer/TriedRetrievingEmptyVariableException.h"
#include "LibView/core/Macros.h"

#include <map>
#include "LibView/model/translator/reader/PreCalculatingCharacterReader.h"

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace tokenizer
			{

				/**
				* Tokenizer that only calculates the next token on being asked for it (on demand).
				*/
				class OnDemandTokenizer : public Tokenizer {

				private:
					//member

					/**
					* Maps token state to token key.
					*/
					std::map<States, token::TokenKeys> m_KeyMap;

					/*
					* Accesses the characters of a given text;
					*/
					translator::reader::PreCalculatingCharacterReader m_Reader;

					/*
					* Contains the transition table for this task.
					*/
					Transition m_Transtion;

					char m_CharacterHolder;
					bool m_CharacterHolderEmpty;

					bool m_HasNextToken;

					uint64_t m_CurrentRow;
					uint64_t m_CurrentColumn;


				public:

					/*
					* Constructor for a tokenizer that shall accesses a text by directory path.
					* @param text [std::string*]: Pointer to the string to be tokenized.
					*/
					OnDemandTokenizer(std::string path);

					/*
					* Constructor for a tokenizer that directly gets a text.
					* @param text [std::string*]: Pointer to the string to be tokenized.
					*/
					OnDemandTokenizer(std::string* text);

					token::Token getNextToken() override;
					inline bool hasNextToken() override { return (hasNextCharacter() || (!characterHolderIsEmpty())); }

				private:

					inline States getNextState(States state, char transition) { return m_Transtion.getNextState(state, transition); };
					/**
					 * Partitions the states in sattes where the read word matches a valid token, and those who dont.
					 * @param state
					 */
					bool isTokenState(States state);

					/**
					 * Looks if a token represents a new line in the file.
					 * @param token [const token::Token&]
					 * @return [bool]
					 */
					bool isNewLine(const token::Token& token) const;

					/**
					*	Looks if a state is an error state.
					 * @param state [States]
					 * @return [bool]
					 */
					bool isErrorState(States state);

					/**
					 * @param state
					 */
					token::TokenKeys getKey(States state);

					void incrementRow();

					void incrementColumn();

					void setUpKeyMap();

					/*
					* Retrievs next character from reader and increments column.
					*/
					char getNextCharacter();

					/*
					* Resets column and incremnets row
					*/
					void setPositionNewLine();

					/*
					* Sets the character holder to a char and sets flag to be set
					*/
					void setCharacterHolder(char toHold);

					char getHoldCharacter();

					inline bool characterHolderIsEmpty() { return m_CharacterHolderEmpty; };

					inline uint64_t getCurrentRow() { return m_CurrentRow; };
					inline uint64_t getCurrentColumn() { return m_CurrentColumn; };

					inline void clearCharacterHolder() { m_CharacterHolderEmpty = true; };

					inline bool hasNextCharacter() { return m_Reader.hasNextCharacter(); };

				};
			}
		}
	}
}