#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



#include "tokenize.h"
#include <string>
#include "LibView/model/translator/reader/readFile.h"
#include "LibView/model/translator/reader/PreCalculatingCharacterReader.h"

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace tokenizer
			{
				class Tokenizer : public tokenize {
				
				private:
					LibView::model::translator::reader::readFile* m_Reader;

				public:

					//Tokenizer();

					/**
					 * @param reader
					 */
					void setReader(reader::readFile* reader);

					/**
					 * @param filePath
					 */
					void startTokenize(std::string filePath);

					//token::Token getNextToken() override; // TODO not shure if correct

				
				};
			}
		}
	}
}
