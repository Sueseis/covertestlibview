/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "LibView/exception/model/reader/NoNextCharacterException.h"
#include "LibView/exception/model/reader/NotASCIICharacterException.h"
#include "LibView/core/Macros.h"
#include "PreCalculatingCharacterReader.h"

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace reader
			{

				void PreCalculatingCharacterReader::setIterators() {
					m_Current = m_Text.begin();
					m_End = m_Text.end();
				}

				PreCalculatingCharacterReader::PreCalculatingCharacterReader(std::string* text)
				{
					m_Text = *text;
					setIterators();
				}

				PreCalculatingCharacterReader::PreCalculatingCharacterReader(std::string path)
				{
					m_Text = FileIO::readFile(path);
					setIterators();
				}

				char PreCalculatingCharacterReader::getNextCharacter()
				{
					if (!hasNextCharacter()) {
						throw exception::NoNextCharacterException(LIBVIEW_LOCATION);
					}
					char c = *m_Current;
					if (!isASCII(c)) {
						throw exception::NOTASCIICharacterException(LIBVIEW_LOCATION);
					}
					m_Current++;
					return c;
				}

			}
		}
	}
}