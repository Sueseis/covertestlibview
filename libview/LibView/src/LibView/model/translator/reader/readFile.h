#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <string>
namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace reader
            {
                /**
                * Text character iterator interface 
                */
                class readFile {
                public:
                    
                    /**
                     * Sets up the access to a file on disc.
                     * Throws exception on faulty file access.
                     * @param filePath
                     */
                    //virtual void readFilePath(std::string filePath) = 0;
                   

                    /**
                     * Provides the functionality to read a file character by character.
                     * @return char : The next character in the given file.
                     */
                    virtual char getNextCharacter() = 0;

                    /**
                    * Checks if the end of a file got reached.
                    * @return bool : A boolean state indicator. True if there are more characters that can be read. False otherwise.
                    */
                    virtual inline bool hasNextCharacter() = 0;
                };

            }
        }
    }
}