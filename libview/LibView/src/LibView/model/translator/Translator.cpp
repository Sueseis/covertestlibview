/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "Translator.h"

/**
 * Translator implementation
 * 
 * Fassade for the Translation package.
 */
namespace LibView
{
    namespace model
    {
        namespace translator
        {
            ast::Group* Translator::translate(std::string path)
            {
                return parser::StackParser().calculateAST(path);
            }
        }
    }
}