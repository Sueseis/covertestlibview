#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



//#include "LibView/model/builder/"

#include "LibView/model/translator/parser/StackParser.h"

#include "LibView/model/translator/ast/Group.h"

namespace LibView
{
    namespace model
    {
        namespace translator
        {
            /**
            * Interface for a function translating a file to an a.s.t.
            */
            class Translator { //: public translate {
            private:
               // parser::Parser* m_parser; --> Design change

            public:
                /**
                * @param path [std::string] : The sytem path to the file.
                * @return root group [Group*] : The root element of the abstract syntax tree created out of the text of the file. 
                */
                static ast::Group* translate(std::string path);
            };

        }
    }
}