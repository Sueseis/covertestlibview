/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "StackParserState.h"

#include "StackParser.h"
//#include "LibView/model/translator/parser/StackParser.h"
namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser {
				/**
				 * StackParserState implementation
				 *
				 * Abstract parsing state according to the statemashine. Handles Tokens for the parser.
				 */


				StackParserState::~StackParserState()
				{
				}

				/**
				  * Handles the current token of the parser to the handling private method according to the key.
				  */
				 void StackParserState::handleToken() {
					
					
					switch (m_Parser.getCurrentToken().getKey()) {


					case token::TokenKeys::AttributeCloser:			handleAttributeCloser();	break;
					case token::TokenKeys::Comment:					handleComment();			break;
					case token::TokenKeys::Float:					handleFloat();				break;
					case token::TokenKeys::FloatS:					handleFloatS();				break;
					case token::TokenKeys::Integer:					handleInteger();			break;
					case token::TokenKeys::LeftFieldEnclosure:		handleLFE();				break;
					case token::TokenKeys::LeftParameterEnclosure:	handleLPE();				break;
					case token::TokenKeys::LeftStatementEnclosure:	handleLSE();				break;
					case token::TokenKeys::Name:					handleName();				break;
					case token::TokenKeys::NewLine:					handleNewLine();			break;
					case token::TokenKeys::ParameterSeperator:		handleParameterSeperator();	break;
					case token::TokenKeys::RightFieldEnlosure:		handleRFE();				break;
					case token::TokenKeys::RightParameterEnclosure:	handleRPE();				break;
					case token::TokenKeys::RightStatementEnclosure:	handleRSE();				break;
					case token::TokenKeys::RuleEscape:				handleRuleEscape();			break;
					case token::TokenKeys::SeperatorAttributeValue:	handleSAV();				break;
					case token::TokenKeys::SeperatorFieldRange:		handleSFR();				break;
					case token::TokenKeys::Spacing:					handleSpacing();			break;
					case token::TokenKeys::SpecialCharacter:		handleSpecialCharacter();	break;
					case token::TokenKeys::StringEnclosure:			handleStringEnclosure();	break;
					case token::TokenKeys::Tab:						handleTab();				break;
					case token::TokenKeys::Operator:				handleOperator();			break;
					
					default:										throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
					}

				}

				 StackParserState& StackParserState::operator=(const StackParserState& other)
				 {
					 //TODO parser assignment
					 return *this;
				 }

				 StackParserState::StackParserState(StackParser& parser) :
					m_Parser(parser)
				{
				}

				 void StackParserState::setState(StackParserState* state)
				 {
					 m_Parser.setState(state);
				 }

				 void StackParserState::handleFloat()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleFloatS()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleInteger()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleComment()
				 {
				 }

				 void StackParserState::handleOperator()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleName()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleStringEnclosure()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleLPE()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleRPE()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleLSE()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleRSE()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleSAV()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleLFE()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleRFE()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleSFR()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleAttributeCloser()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleNewLine()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleSpacing()
				 {
				 }

				 void StackParserState::handleRuleEscape()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleParameterSeperator()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleSpecialCharacter()
				 {
					 throw exception::WordNotAllowedAtThisPositionException(LIBVIEW_LOCATION);
				 }

				 void StackParserState::handleTab()
				 {
				 }

			}
		}
	}
}