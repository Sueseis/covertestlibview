#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



#include "LibView/model/translator/parser/StackParserState.h"
namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser
			{
				namespace concreteState {

					/**
					* Collects all tokens of the name of a group or the values for a complex attribute.
					* Entered when reading "(", then looping.
					*/
					class ParameterReading : public StackParserState {

					public:

						/**
						* Constructor
						* @param parser [StackParser&] : The parser the state is part of.
						*/
						ParameterReading(StackParser& parser);

					protected:
						//Collects the following tokens
						void handleLFE() override;
						void handleSFR() override;
						void handleRFE() override;
						void handleFloat() override;
						void handleFloatS() override;
						void handleInteger() override;
						void handleName() override;
						void handleParameterSeperator() override;
						void handleStringEnclosure() override;

						void handleRuleEscape() override; //allows multilining by entering that state

						void handleRPE() override; //recognition of paramter ending

					private:
						/**
						* Adds parameter and sets state to read.
						*/
						void addParameter();
					};
				}
			}
		}
	}
}
