#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibView/model/translator/parser/StackParserState.h"

namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace parser
            {
                namespace concreteState {

                    class TypeRead : public StackParserState {
                    public:

                        /**
                        * Constructor
                        * @param parser [StackParser&] : The parser the state is part of.
                        */
                        TypeRead(StackParser& parser);

                    protected:
                         void handleLPE() override; //Entering group or compelx attribute reading
                         void handleSAV() override; //Entering simple attribute reading
                    };
                }
            }
        }
    }
}