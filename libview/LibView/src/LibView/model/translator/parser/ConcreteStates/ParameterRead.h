#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



#include "LibView/model/translator/parser/StackParserState.h"
namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace parser
            {
                namespace concreteState {

                    /**
                    * State for when the name of a group or the vlaues of a complex attribute were read. 
                    * Entered by ")" reading.
                    */
                    class ParameterRead : public StackParserState {
                    public:

                        /**
                        * Constructor
                        * @param parser [StackParser&] : The parser the state is part of.
                        */
                        ParameterRead(StackParser& parser);
                    
                    protected:
                        void handleLSE() override;
                        void handleAttributeCloser() override;
                    };

                }
            }
        }
    }
}