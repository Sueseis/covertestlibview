/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "ParameterReading.h"
#include "LibView/model/translator/parser/StackParser.h"
namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace parser
            {
                namespace concreteState {
                    ParameterReading::ParameterReading(StackParser& parser) :
                        StackParserState(parser)
                    {}
                    void ParameterReading::handleLFE()
                    {
                        addParameter();
                    }
                    void ParameterReading::handleSFR()
                    {
                        addParameter();
                    }
                    void ParameterReading::handleRFE()
                    {
                        addParameter();
                    }
                    void ParameterReading::handleFloat()
                    {
                        addParameter();
                    }
                    void ParameterReading::handleFloatS()
                    {
                        addParameter();
                    }
                    void ParameterReading::handleInteger()
                    {
                        addParameter();
                    }
                    void ParameterReading::handleName()
                    {
                        addParameter();
                    }
                    void ParameterReading::handleParameterSeperator()
                    {
                        addParameter();
                    }
                    void ParameterReading::handleStringEnclosure()
                    {
                        m_Parser.addParameter();
                    }
                    void ParameterReading::handleRuleEscape()
                    {
                        setState(new concreteState::Multiline(m_Parser));
                    }
                    void ParameterReading::handleRPE()
                    {
                        setState(new concreteState::ParameterRead(m_Parser));
                    }
                    void ParameterReading::addParameter()
                    {
                        m_Parser.addParameter();
                    }
                }
            }
        }
    }
}