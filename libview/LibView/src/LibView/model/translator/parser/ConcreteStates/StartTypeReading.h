#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



#include "LibView/model/translator/parser/StackParserState.h"
namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser
			{
				namespace concreteState
				{
					/**
					* Start of the state mashine. Expects the type of a group or an attribute.
					*/
					class StartTypeReading : public StackParserState
					{
					public:

						/**
						* Constructor
						* @param parser [StackParser&] : The parser the state is part of.
						*/
						StartTypeReading(StackParser& parser);

					protected:

						inline void handleNewLine() override {}; //It's an earthquake under the ocean. We handle it by not handling it.	Simplification of the parser.				
						void handleName() override;
						void handleRSE() override;

						inline bool isFinal() override { return true; };
					};


				}
			}
		}
	}
}