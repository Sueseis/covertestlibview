#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



#include "LibView/model/translator/parser/StackParserState.h"

namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace parser
            {
                namespace concreteState {

                    /**
                    * Entered when at least one token was read as a value for a simple attribute. 
                    * Collects further tokens as values for that attributes.
                    */
                    class SARead : public StackParserState {
                    public:

                        /**
                        * Constructor
                        * @param parser [StackParser&] : The parser the state is part of.
                        */
                        SARead(StackParser& parser);

                    protected:

                        //Collects following tokens.
                        inline void handleFloat() override { addParameter(); };
                        inline void handleFloatS() override { addParameter(); };
                        inline void handleInteger() override { addParameter(); };
                        inline void handleOperator() override { addParameter(); };
                        inline void handleName() override { addParameter(); };

                        void handleStringEnclosure() override; //Enters string reading.

                        void handleAttributeCloser() override; //Escapes the loop collecting, adds build attribute. Goes back to start.

                    private:
                        /**
                        * Adds parameter and sets state to read.
                        */
                        void addParameter();
                    };

                }
            }
        }
    }
}