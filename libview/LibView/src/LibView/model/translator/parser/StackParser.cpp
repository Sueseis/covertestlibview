
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "StackParser.h"
#include "LibView/model/translator/token/TokenKeys.h"
namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser {
				/**
				 * StackParser implementation
				 *
				 * Parser working with a stack to handle downward recursion. Stores data of statements in attributes, while parsing.
				 */


				translator::ast::Group* StackParser::calculateAST(const std::string& path)
				{
					tokenizer::OnDemandTokenizer tokenizer = tokenizer::OnDemandTokenizer(path);

					setState(new concreteState::StartTypeReading(*this));

					//empty text
					if (!tokenizer.hasNextToken()) {
						//throw error
					}

					while (tokenizer.hasNextToken()) {
						m_CurrentToken = tokenizer.getNextToken();
						m_State->handleToken();
					}
						return m_Upper;
				}


				StackParser::StackParser() :
					m_State(new StackParserState(*this)),
					m_GroupStack(),
					m_UpperGroupSet(false)
				{
				}

				/**
				  * Adds a simple attribute using the type and parameters members to the top group in the group stack.
				  */
				void StackParser::addSimpleAttribute() {
					m_GroupStack.top()->addSimpleAttribute(m_Type, m_Parameters);
					clearParamters();
				}

				/**
				 * Adds a complex attribute using the type and parameters members to the top group in the group stack.
				 */
				void StackParser::addComplexAttribute() {
					m_GroupStack.top()->addComplexAttribute(m_Type, m_Parameters);
					clearParamters();
				}


				/**
				 * @param type
				 */
				void StackParser::setType() {
					m_Type = m_CurrentToken;
				}

				StackParser::~StackParser()
				{
					delete m_State;
				}


				void StackParser::clearParamters() {
					m_Parameters.clear();
				}

				/**
				 * @param Parameter1
				 */
				void StackParser::pushGroup() {
					if (m_UpperGroupSet && m_GroupStack.empty()) {
						throw exception::MoreThanOneUppestGroupException(LIBVIEW_LOCATION);
					}

					ast::Group* group = new ast::Group(m_Type, m_Parameters); /// May only be deleted by data structure class
					clearParamters();

					if (!m_UpperGroupSet) {
						m_Upper = group;
						m_UpperGroupSet = true;
					}
					else {
						group->setParent(m_GroupStack.top());
						peakGroup()->addChild(group);
					}
					m_GroupStack.push(group);
				}

				/**
				 * @return Group
				 */
				ast::Group* StackParser::peakGroup() const {

					if (!m_GroupStack.empty()) {

						return m_GroupStack.top();
					}
					throw exception::OutsideOfGroupDeclarationException(LIBVIEW_LOCATION);
				}

				void StackParser::popGroup() {
					if (!m_GroupStack.empty()) {
						m_GroupStack.pop();
					}
					else {
						throw exception::MoreGroupsClosedThanStartedException(LIBVIEW_LOCATION);
					}
				}

			}
		}
	}
}