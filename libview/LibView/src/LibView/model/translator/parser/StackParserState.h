#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "StackParser_decl.h"
#include "LibView/model/translator/token/Token.h"
#include "LibView/model/translator/token/TokenKeys.h"

#include "LibView/exception/model/parser/WordNotAllowedAtThisPositionException.h"

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser
			{

				class StackParserState {

				protected:

					StackParser& m_Parser;


				public:

					~StackParserState();

					/**
					* State works with parsers funtions to handle the curretnly looked at token.
					* @throws [WordNotAllowedAtThisPositionException] : If a token is not allowed at a certain positiion syntacticly.
					*/
					virtual void handleToken();

					/**
					* States are files by default
					*/
					inline virtual bool isFinal() { return false; };

					/**
					* Overrides the = operator for states
					*/
					StackParserState& operator=(const StackParserState& other);

					/**
					* Constructor for the stack parser state.
					* @param parser [StackParser&] : the related parser
					*/
					StackParserState(StackParser& parser);

				protected:

					void setState(StackParserState* state);


					//setup all transitions with errors, the classes that inherit will override the allowed transitions

								//handle[TOKENKEY_NAME]

					virtual void handleFloat();

					virtual void handleFloatS();

					virtual void handleInteger();

					virtual void handleComment();//comments are not handled by this application

					virtual void handleOperator();

					virtual void handleName();

					virtual void handleStringEnclosure();

					virtual void handleLPE();

					virtual void handleRPE();

					virtual void handleLSE();

					virtual void handleRSE();

					virtual void handleSAV();

					virtual void handleLFE();

					virtual void handleRFE();

					virtual void handleSFR();

					virtual void handleAttributeCloser();

					virtual void handleNewLine();

					virtual void handleSpacing(); //spacing ignored by deafult

					virtual void handleRuleEscape();

					virtual void handleParameterSeperator();

					virtual void handleSpecialCharacter();

					virtual void handleTab(); //tabs ignored by default 

				};

			}
		}
	}
}