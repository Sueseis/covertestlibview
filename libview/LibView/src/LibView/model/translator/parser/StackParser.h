#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



#include "Parser.h"
#include "StackParserState_decl.h"
#include "LibView/model/translator/ast/Group.h"
#include "LibView/model/translator/tokenizer/OnDemandTokenizer.h"

 //states
#include "LibView/model/translator/parser/ConcreteStates/EscapedExit.h"
#include "LibView/model/translator/parser/ConcreteStates/Multiline.h"
#include "LibView/model/translator/parser/ConcreteStates/ParameterRead.h"
#include "LibView/model/translator/parser/ConcreteStates/ParameterReading.h"
#include "LibView/model/translator/parser/ConcreteStates/SARead.h"
#include "LibView/model/translator/parser/ConcreteStates/SAReadingValue.h"
#include "LibView/model/translator/parser/ConcreteStates/StartTypeReading.h"
#include "LibView/model/translator/parser/ConcreteStates/StringValue.h"
#include "LibView/model/translator/parser/ConcreteStates/TypeRead.h"

#include <vector>
#include <stack>

#include "LibView/exception/model/parser/MoreGroupsClosedThanStartedException.h"
#include "LibView/exception/model/parser/OutsideOfGroupDeclarationException.h"
#include "LibView/exception/model/parser/MoreThanOneUppestGroupException.h"

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser
			{
				/*
				 A concrete parser.
				 Name derives from working group by group and pushing them on a stack as they are stacked in the file.
				 Fully sequentiell.
				*/
				class StackParser : public Parser
				{

				private:

					//member

					StackParserState* m_State;
					std::stack<ast::Group*> m_GroupStack;
					token::Token m_Type;
					std::vector<token::Token> m_Parameters;
					token::Token m_CurrentToken;
					ast::Group* m_Upper;

					bool m_UpperGroupSet;


				public:
					
					/**
					* Constructor
					*/
					StackParser();

					/*
					Destructor for the StackParser class.
					*/
					~StackParser();

					translator::ast::Group* calculateAST(const std::string& path) override;

					/**
					* Sets type to current token.
					*/
					void setType();

					
					/**
					* Sets the state of the parser.
					* @param state [StackParserState*]
					*/
					inline void setState(StackParserState* state) {
						delete m_State;
						m_State = state;
					};

					/**
					 * Adds a simple attribute using the type and parameters members to the top group in the group stack.
					 */
					void addSimpleAttribute();

					/**
					 * Adds a complex attribute using the type and parameters members to the top group in the group stack.
					 */
					void addComplexAttribute();

					/**
					* Getter for the strored type token.
					* @return type [token::Token]
					*/
					inline token::Token getType() { return m_Type; };


					/**
					* Getter for stored paramters.
					* @return paramter list [std::vector<token::Token>]
					*/
					inline std::vector<token::Token> getParameters() { return m_Parameters; };

					/**
					 * Adds the current token to the parameter list.
					 */
					inline void addParameter() { m_Parameters.push_back(m_CurrentToken); };

					/**
					* Flushes the stored paramter list.
					*/
					void clearParamters();

					/**
					* Pushes a new group onto the stack taking in the type member and the paramter list for the name.
					*/
					void pushGroup();

					/**
					* @return top stack element [ast::Group*] : Poitner to the top stack group without removing it.
					*/
					ast::Group* peakGroup() const;

					/**
					 Removes the top stack group.
					*/
					void popGroup();

					/**
					* Returns the curretnly looked at token which is stored.
					*/
					inline token::Token getCurrentToken() const { return m_CurrentToken; };

				};

			}
		}
	}
}