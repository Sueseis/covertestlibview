/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "LibView/model/translator/token/TokenKeys.h"
#include "LibView/model/translator/token/Token.h"
#include "Group.h"

#include <vector>

 /**
  * Group implementation
  *
  * Knot of a token tree, representing a typed (and named) collection of statements.
  * LibFile:
  * [type] ([name]?) {
  * ...
  * }
  */


namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace ast
            {
                /**
                * Default Contructor
                */
                Group::Group(token::Token type, std::vector<token::Token> name)
                    :
                    m_Type(type),
                    m_Name(name),
                    m_Children(std::vector<Group*>()),
                    m_SimpleAttributeList(std::vector<Attribute*>()),
                    m_ComplexAttributeList(std::vector<Attribute*>()),
                    m_Parent(nullptr)
                {}

                Group::~Group()
                {
                    for(Group* child : m_Children)
                        delete child;
                    for(Attribute* attribute : m_SimpleAttributeList)
                        delete attribute;
                    for(Attribute* attribute : m_ComplexAttributeList)
                        delete attribute;
                }

                /**
                 * @param type
                 */
                void Group::setType(token::Token type)
                {
                    m_Type = type;
                }

                /**
                 * @return Token
                 */
                token::Token Group::getType()
                {
                    return m_Type;
                }

                /**
                 * @param name
                 */
                void Group::setName(std::vector<token::Token> name)
                {
                    m_Name = name;
                }

                /**
                 * @return vector<Token>
                 */
                std::vector<token::Token> Group::getName()
                {
                    return m_Name;
                }

                /**
                 * @param child
                 */
                void Group::addChild(Group* child)
                {

                    m_Children.push_back(child);
                }

                /**
                 * @return Group
                 */
                std::vector<Group*> Group::getChildren()
                {
                    return m_Children;
                }

                /**
                 * @param attributeName
                 * @param value
                 */
                void Group::addSimpleAttribute(token::Token attributeName, std::vector < token::Token > value)
                {
                    m_SimpleAttributeList.push_back(new Attribute(attributeName, value));
                }

                /**
                 * @return vector<Attribute>
                 */
                std::vector<Attribute*> Group::getSimpleAttributes()
                {
                    return m_SimpleAttributeList;
                }

                /**
                 * @param attributeName
                 * @param value
                 */
                void Group::addComplexAttribute(token::Token attributeName, std::vector<token::Token> value)
                {
                    m_ComplexAttributeList.push_back(new Attribute(attributeName, value));
                }

                /**
                 * @return vector<Attribute>
                 */
                std::vector<Attribute*> Group::getComplexAttributes()
                {
                    return m_ComplexAttributeList;
                }

                /**
                 * @param parent
                 */
                void Group::setParent(Group* parent)
                {
                    this->m_Parent = parent;
                }

                /**
                 * @return Group
                 */
                Group Group::getParent()
                {
                    return *m_Parent;
                }
            }
        }
    }
}