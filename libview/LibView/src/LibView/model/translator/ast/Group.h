#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "Attribute.h"
#include "LibView/model/translator/token/Token.h"
#include <vector>

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace ast
			{
				/**
				* Encapsulates the sytax of a liberty formatted file.
				*/
				class Group {

				private:

					//members


					/*
					parentGroup(nameParent) {
						...
						simpleAttributeName : value;
						...
						complexAtrtibuteName(values);
						...
						child1Group(nameChild1) { ... }
						child2Group(nameChild2) { ... ]
						...
					}
					*/

					token::Token m_Type;
					std::vector<token::Token> m_Name;
					std::vector<Group*> m_Children;
					std::vector<Attribute*> m_SimpleAttributeList;
					std::vector<Attribute*> m_ComplexAttributeList;
					Group* m_Parent;


				public:

					//functions

					/**
					 Constructor setting type and name and leaving other members open.
					 Leaves attributes, parents and children empty.
					*/
					Group(token::Token type, std::vector<token::Token> name);
					~Group();

					/**
					 * (syntax: group_type(group_name){...})
					 * (e.g.: cell, pin, ...)
					 * @param token::Token : The token setting the type of the group.
					 */
					void setType(token::Token type);

					/**
					* Getter for the groups type.
					* @return [token::Token] : Groups type
					*/
					token::Token getType();

					/**
					 * (syntax: group_type(group_name){...})
					 * @param name std::vector<token::Token> : Tokens making up the name for the group.
					 */
					void setName(std::vector<token::Token> name);

					/**
					* Getter for the groups name.
					* @return std::vector<token::Token>
					*/
					std::vector<token::Token> getName();

					/**
					 * Use this function to add a group that is inside another to the upper one.
					 * (parent_group(){
					 *		child_group(){}
					 *  })
					 * @param child [Group*] : Adds a child to this group.
					 */
					void addChild(Group* child);


					/**
					* Getter for the groups children.
					* @return children [std::vector<Group*>]
					*/
					std::vector<Group*> getChildren();

					/**
					 * ( attribute_name : attribute_value; )
					 * @param attributeName [(token::Token] : Token of the attributes name.
					 * @param value [std::vector<token::Token>] : Tokens making up the attributes values.
					 */
					void addSimpleAttribute(token::Token attributeName, std::vector<token::Token> value);

					/**
					* Getter for the simple attributes.
					* @return std::vector<Attribute*>
					*/
					std::vector<Attribute*> getSimpleAttributes();

					/**
					 * file format:  attribute_name(attribute_values);
					 * @param attributeName [(token::Token] : Token of the attributes name.
					 * @param value [std::vector<token::Token>] : Tokens making up the attributes values.
					 */
					void addComplexAttribute(token::Token attributeName, std::vector<token::Token> value);

					/**
					* Getter for the complex attributes.
					* @return complex attributes [std::vector<Attribute*>]
					*/
					std::vector<Attribute*> getComplexAttributes();

					/**
					* Setter for the parent group.
					 * @param parent [Group*]
					 */
					void setParent(Group* parent);

					/**
					* Getter for the parent group.
					* @return parent [Group]
					*/
					Group getParent();


				};
			}
		}
	}
}