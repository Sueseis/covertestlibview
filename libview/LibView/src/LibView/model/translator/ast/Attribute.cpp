/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "Attribute.h"

namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace ast
            {
                Attribute::Attribute(token::Token name, std::vector<token::Token> value)
                    : m_Name(name),
                    m_Value(value)
                {}

                token::Token Attribute::getM_Name()
                {
                    return m_Name;
                }
                std::vector<token::Token> Attribute::getM_Value()
                {
                    return m_Value;
                }
            }
        }
    }
}