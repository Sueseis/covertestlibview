/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "HeldPair.h"

/**
 * HeldPair implementation
 * 
 * Capsule class for an integer and string attribute:
 * 
 * Used to store indices and paths.
 */

namespace LibView
{
    namespace model
    {
        HeldPair::HeldPair(std::string path, int64_t index) : path(path), index(index)
        {

        }
    }
}
