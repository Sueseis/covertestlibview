#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "ModelFacade.h"
#include "AObserver.h"

namespace LibView
{
    namespace model
    {
        class HolderObserver : public AObserver
        {
        public:

            /**
             * Constructor for the HolderObserver.
             *
             * @param modelFacade (ModelFacade*) : Reference to the facade to push updates
             * @param modelState (ModelState*) : Reference to the ModelState
             * @param modelState (AttributeHolder*) : Reference to the AttributeHolder, subject
             * @param modelState (LibFileHolder*) : Reference to the LibFileHolder, subject

             */
            HolderObserver(
                ModelFacade* modelFacade, 
                ModelState* modelState, 
                AttributeHolder* attributeHolder, 
                LibFileHolder* libFileHolder);

            /**
             * Updates last added/removed lists in ModelState and notifies facade.
             */
            void update() override;

            /**
             * Gets the indicies from a path_index_pair.
             *
             * @param pairList (std::vector<path_index_pair>) : List of the pairs of which to extract the indicies
             * 
             * @return std::vector<index_t> : List of all extracted indicies
             */
            std::vector<index_t> getIndiciesFromPairs(std::vector<path_index_pair> pairList);

        private: 
            ModelFacade* m_pModelFacade;
            ModelState* m_pModelState;
            AttributeHolder* m_pAttributeHolder;
            LibFileHolder* m_pLibFileHolder;
        };
    }
}