/**
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "ModelState.h"

 /**
  * ModelState implementation
  *
  * Encapsulates the state of the model.
  * Including: All loaded LibFiles, All loaded Attributes mapped to an index, and the last added/removed of the Attributes and the LibFiles.
  * gets communicated to the presenter.
  *
  */

namespace LibView
{
    namespace model
    {


        ModelState::ModelState()
        {
            //TODO fix initialisation of members
        }


        model::data_structure::LibertyFileData* ModelState::getLibFile(const index_t& index) const
        {
            m_LibFileHolder->getLibFileList();
            return nullptr; // TODO Fix
        }

        std::string ModelState::getAttribute(const index_t& index) const
        {
            return m_LibFileHolder->getAttributeHolder()->getAttribute(index); 
        }

        index_t ModelState::getAttribute(const std::string& path) const
        {
            return m_LibFileHolder->getAttributeHolder()->getAttribute(path);
        }

        const AttributeMap* ModelState::getAttributes() const
        {
            return m_AttributeMap;
        }

        std::vector<model::path_index_pair> ModelState::getLastAddedAttributes() const
        {

            return m_LibFileHolder->getAttributeHolder()->getLastAdded();
        }

        std::vector<model::path_index_pair> ModelState::getLastRemovedAttributes() const
        {
            return m_LibFileHolder->getAttributeHolder()->getLastRemoved();
        }

        std::vector<index_t> ModelState::getLastAddedLibFiles() const
        {
            return m_LastAddedLibFiles;
        }

        std::vector<index_t> ModelState::getLastRemovedLibFiles() const
        {
            return m_LastRemovedLibFiles;
        }

        std::vector<model::data_structure::LibertyFileData*> ModelState::getLibFileList() const
        {
            return m_LibFileHolder->getLibFileList();
        }

        void ModelState::updateLastChangedLibFiles(std::vector<index_t> addedLibFiles, std::vector<index_t> removedLibFiles)
        {
            m_LastAddedLibFiles = addedLibFiles;
            m_LastRemovedLibFiles = removedLibFiles;
            
        }

        //void ModelState::updateLastChangedAttributes(std::vector<index_t> addedAttributes, std::vector<index_t> //removedAttributes)
        //{
        //    m_LastAddedAttributes = addedAttributes;
        //    m_LastRemovedAttributes = removedAttributes;
        //}

 



    }
}