#include "PresetHolder.h"

#include "presets/TestPreset.h"
#include "presets/Leakage.h"
#include "presets/TimingDelay.h"
#include "presets/TimingTransition.h"
#include "presets/AllPowerTables.h"

namespace LibView
{
    namespace model
    {
        PresetHolder::PresetHolder()
            :
            m_Presets(std::vector<APreset*>())
        {
            //m_Presets.push_back(new TestPreset());
            m_Presets.push_back(new Leakage());
            m_Presets.push_back(new TimingDelay());
            m_Presets.push_back(new TimingTransition());
            m_Presets.push_back(new AllPowerTables());
        }
        PresetHolder::~PresetHolder()
        {
            for(APreset* preset : m_Presets)
            {
                delete preset;
            }
        }
    }
}