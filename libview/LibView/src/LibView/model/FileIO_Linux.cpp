#ifdef LIBVIEW_PLATFORM_LINUX


#include "FileIO.h"
#include "LibView/core/Macros.h"
#include "LibView/exception/FileCloseException.h"
#include "LibView/exception/FileOpenException.h"

#include <stdio.h>
#include <stdlib.h>

namespace LibView
{
    namespace model
    {
        std::string FileIO::readFile(std::string path)
        {
            return readFile(path.c_str());
        }

        std::string FileIO::readFile(const char* path)
        {
            FILE* pFile = nullptr;

            pFile = fopen(path, "rb");

            if(pFile == nullptr)
            {
                throw exception::FileOpenException(LIBVIEW_LOCATION);
            }


            fseek(pFile, 0L, SEEK_END);
            long fileSize = ftell(pFile);
            fseek(pFile, 0L, SEEK_SET);

            long bufferSize = fileSize + LIBVIEW_NULL_TERMINATION_BUFFER_SIZE;

            char* buffer = (char*) std::malloc(sizeof(char) * bufferSize);

            fread(buffer, sizeof(char), fileSize, pFile);

            int error = fclose(pFile);

            if(error != 0)
            {
                throw exception::FileCloseException(LIBVIEW_LOCATION);
            }

            buffer[bufferSize - 1] = 0;

            std::string result = buffer;

            std::free(buffer);

            return result;
        }
    }
}
#endif