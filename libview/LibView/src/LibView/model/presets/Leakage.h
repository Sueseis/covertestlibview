#pragma once

#include "LibView/model/APreset.h"

namespace LibView
{
    namespace model
    {
        class Leakage : public APreset
        {
        public:

            inline Leakage() :
                APreset("Leakage")
            {
                addPath("(.*)(leakage_power)(.*)");
            }
        };
    }
}