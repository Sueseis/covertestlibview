#pragma once

#include "LibView/model/APreset.h"

namespace LibView
{
    namespace model
    {
        class TimingDelay : public APreset
        {
        public:

            inline TimingDelay() :
                APreset("TimingDelay")
            {
                addPath("(.*)(cell_rise)(.*)");
                addPath("(.*)(cell_fall)(.*)");
            }
        };
    }
}