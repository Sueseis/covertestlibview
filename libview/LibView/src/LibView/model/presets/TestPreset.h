#pragma once

#include "LibView/model/APreset.h"

namespace LibView
{
    namespace model
    {
        class TestPreset : public APreset
        {
        public:

            inline TestPreset():
                APreset("TestPreset")
            {
                addPath("(.*)(cell)(.*)(X2)(.*)");      
            }
        };
    }
}