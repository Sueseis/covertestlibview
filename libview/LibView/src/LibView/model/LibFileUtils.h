#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <vector>
#include <string>
#include "SortingParameter.h"

namespace LibView
{
    namespace model
    {

        class LibFileUtils
        {
        protected:

            /**
             * sorts all LibFiles according to a sorting parameter.
             * returns a sorted list of the libFiles.
             * @param sortingParameter
             * @param allLibFiles
             * @param ascending
             */
            static std::vector<int64_t> sortLibFileListAccordingTo(SortingParameter param, std::vector<int64_t> allLibFiles, bool ascending);

            /**
             * returns a list of all attributes that match a given search term.
             * iterates all attributes and if it matches put the attribute in the list.
             * @param searchTerm
             */
            static std::vector<int64_t> getMatchingAttributesList(std::string searchTerm);

            /**
             * methode to calculate the aberrations beetween two parameters
             * @param param1
             * @param param2
             */
            static double calculateDleta(double param1, double param2);
        };

    }
}