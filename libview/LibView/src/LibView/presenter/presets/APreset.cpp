/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "APreset.h"

 /**
  * APreset implementation
  *
  * Is the AbstractParent Class for all Presets.
  * gives them the ability to change their related attributes.
  */

namespace LibView
{
	namespace presenter
	{
		/*
		* Default constructor
		*/
		
		APreset::APreset()
		{
		}

		/**
		 * adds the new related attributes to a Preset
		 */
		void APreset::add() {

		}

		/**
		 * removes the old(not occuring anymore) attributes from a preset.
		 */
		void APreset::remove() {

		}

		/**
		 * returns the attributes related to a selected preset in a list.
		 * @return std::vector<index:int64_t>
		 */
		std::vector<int64_t> APreset::get() {
			return std::vector<int64_t>(); // TODO FIX

		}
	}
}
