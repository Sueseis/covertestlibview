#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <vector>
#include <stdint.h>


namespace LibView
{
	namespace presenter
	{

		class APreset {
		public:
			APreset();
			/**
			 * adds the new related attributes to a Preset
			 */
			void add();

			/**
			 * removes the old(not occuring anymore) attributes from a preset.
			 */
			void remove();

			/**
			 * returns the attributes related to a selected preset in a list.
			 */
			std::vector<int64_t> get();
		private:
			std::vector<int64_t> m_ListOfStringIndices;
			std::vector<int64_t> m_ListOfAttributeIndices;
		};
	}
}