#include "PresetDataStorage.h"



namespace LibView
{
    namespace presenter
    {

        void PresetDataStorage::generatePresetDataFor(std::vector<model::data_structure::LibertyFileData*> libFiles)

        {
            for(size_t i = 0; i < libFiles.size(); i++)
            {
                index_t libFileIndex = libFiles[i]->getLibFileIndex();

                PresetPowerTablesData* p_PresetPowerTablesData = new PresetPowerTablesData(libFiles[i]);

                PresetTimingTransitionData* p_PresetTimingTransitionData = new PresetTimingTransitionData(libFiles[i]);

                PresetTimingDelayData* p_PresetTimingDelayData = new PresetTimingDelayData(libFiles[i]);

                PresetLeakageData* p_PresetLeakageData = new PresetLeakageData(libFiles[i]);

                addNewLibFileToLeakageMap(libFileIndex, p_PresetLeakageData);

                addNewLibFileToPowerTableMap(libFileIndex, p_PresetPowerTablesData);

                addNewLibFileToTimingDelayMap(libFileIndex, p_PresetTimingDelayData);

                addNewLibFileToTimingTransitionMap(libFileIndex, p_PresetTimingTransitionData);
            }
        }

        void PresetDataStorage::removePresetDataFor(index_t IndexLibFile)
        {

        }

        const std::map<int64_t, APreset>& PresetDataStorage::getPresetMap(APreset preset)
        {
            return std::map<int64_t, APreset>(); // TODO Fix
        }

        void PresetDataStorage::updatePresetsLastAdded(std::vector<std::string> lastAddedAttributes)
        {

        }

        void PresetDataStorage::updatePresetsLastRemoved(std::vector<std::string> lastRemovedAttributes)
        {

        }



        void PresetDataStorage::addNewLibFileToPowerTableMap(index_t IndexLibFile, PresetPowerTablesData* p_PresetPowerTablesData)
        {
            m_MapPowerTables[IndexLibFile] = p_PresetPowerTablesData;
        }

        void PresetDataStorage::addNewLibFileToTimingTransitionMap(index_t IndexLibFile, PresetTimingTransitionData* p_PresetTimingTransitionData)
        {
            m_MapTimingTransition[IndexLibFile] = p_PresetTimingTransitionData;
        }

        void PresetDataStorage::addNewLibFileToTimingDelayMap(index_t IndexLibFile, PresetTimingDelayData* p_PresetTimingDelayData)
        {
            m_MapTimingDelay[IndexLibFile] = p_PresetTimingDelayData;
        }

        void PresetDataStorage::addNewLibFileToLeakageMap(index_t IndexLibFile, PresetLeakageData* p_PresetLeakageData)
        {
            m_MapLeakage[IndexLibFile] = p_PresetLeakageData;
        }


        void PresetDataStorage::removeOldLibfileFromAllMaps(index_t IndexLibfile)
        {
            PresetPowerTablesData* p_PowerTablePointer = m_MapPowerTables.at(IndexLibfile);
            PresetTimingTransitionData* p_TimingTransitionPointer = m_MapTimingTransition.at(IndexLibfile);
            PresetTimingDelayData* p_TimingDelayPointer = m_MapTimingDelay.at(IndexLibfile);
            PresetLeakageData* p_LeakageDataPointer = m_MapLeakage.at(IndexLibfile);


            m_MapPowerTables.erase(IndexLibfile);
            m_MapTimingTransition.erase(IndexLibfile);
            m_MapTimingDelay.erase(IndexLibfile);
            m_MapLeakage.erase(IndexLibfile);


            delete p_PowerTablePointer;
            delete p_TimingTransitionPointer;
            delete p_TimingDelayPointer;
            delete p_LeakageDataPointer;

            //TODO check for smart pointers
        }
    }
}
