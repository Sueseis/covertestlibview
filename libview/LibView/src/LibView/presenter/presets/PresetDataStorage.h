#pragma once 

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */
#include "APreset.h"
#include "APresetData.h"
#include "PresetPowerTablesData.h"
#include "PresetLeakageData.h"
#include "PresetTimingDelayData.h"
#include "PresetTimingTransitionData.h"
#include "LibView/model/data_structure/LibertyFileData.h"
#include <stdint.h>
#include <vector>
#include <map>
#include <string>

 /**
  * PresetDataStorage implementation
  *
  * Stores a Map for each Preset.
  * Maps (for each Preset) all LibFiles to their corresponding PresetDataObject.
  */

namespace LibView
{
	namespace presenter
	{
		class PresetDataStorage
		{
		public:

			/**
			 * @param libFiles
			 */
			void generatePresetDataFor(std::vector<model::data_structure::LibertyFileData*> libFiles);

			/**
			 * @param IndexLibFile
			 */
			void removePresetDataFor(index_t IndexLibFile);

			/**
			 * returns the map for the given preset.
			 * @param preset

			 //TODO what's the method's functionality ?
			 */
			const std::map<int64_t, APreset>& getPresetMap(APreset preset);

			/**
			 * @param lastAddedAttributes
			 */
			void updatePresetsLastAdded(std::vector<std::string> lastAddedAttributes);

			/**
			 * @param lastRemovedAttributes
			 */
			void updatePresetsLastRemoved(std::vector<std::string> lastRemovedAttributes);






			/**
			 * @param IndexLibFile
			 * @param p_PresetPowerTablesData
			 */
			void addNewLibFileToPowerTableMap(index_t IndexLibFile, PresetPowerTablesData* p_PresetPowerTablesData);
			
			/**
			 * @param IndexLibFile
			 * @param p_PresetTimingTransitionData
			 */
			void addNewLibFileToTimingTransitionMap(index_t IndexLibFile, PresetTimingTransitionData* p_PresetTimingTransitionData);
			
			/**
			 * @param IndexLibFile
			 * @param p_PresetTimingDelayData
			 */
			void addNewLibFileToTimingDelayMap(index_t IndexLibFile, PresetTimingDelayData* p_PresetTimingDelayData);
			
			/**
			 * @param IndexLibFile
			 * @param p_PresetLeakageData 
			 */
			void addNewLibFileToLeakageMap(index_t IndexLibFile, PresetLeakageData* p_PresetLeakageData);





			/**
			 * @param IndexLibfile
			 */
			void removeOldLibfileFromAllMaps(index_t IndexLibfile);
		private:
			std::map<index_t, PresetPowerTablesData*> m_MapPowerTables;
			std::map<index_t, PresetTimingTransitionData*> m_MapTimingTransition;
			std::map<index_t, PresetTimingDelayData*> m_MapTimingDelay;
			std::map<index_t, PresetLeakageData*> m_MapLeakage;
		};


	}
}