/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "PresetTimingDelay.h"

/**
 * PresetTimingDelay implementation
 * 
 * Placeholder for optional additional Presets.
 */
namespace LibView
{
    namespace presenter
    {
    }
}