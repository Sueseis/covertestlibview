/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "PresetLeakageData.h"

/**
 * PresetLeakageData implementation
 * 
 * PresetData Class  for the VoltagePreset.
 */

namespace LibView
{
    namespace presenter
    {
        /**
         * @param libFile*
         */
        PresetLeakageData::PresetLeakageData(model::data_structure::LibertyFileData* libFile)
        {

        }
    }
}