#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



#include "APresetData.h"
#include "LibView/model/data_structure/LibertyFileData.h"

namespace LibView
{
    namespace presenter
    {
        class PresetTimingDelayData : public APresetData
        {
        public:

            /**
             * @param libFile*
             */
            PresetTimingDelayData(model::data_structure::LibertyFileData* libFile);
        };

    }
}