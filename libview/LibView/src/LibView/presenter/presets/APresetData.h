#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "LibView/model/data_structure/LibertyFileData.h"
#include "LibView/presenter/HistogramData.h"
#include <vector>
namespace LibView
{
    namespace presenter
    {


        class APresetData
        {
        public:

            APresetData();

            /**
             * @param libFile*
             */
            APresetData(model::data_structure::LibertyFileData* libFile);
        private:
            double m_Variation;
            double m_Max;
            double m_Min;
            double m_Average;
            std::vector<void*> m_ListOfCustomAttributes; //TODO pls fix type <3
            HistogramData m_HistogramData;
        };
    }
}