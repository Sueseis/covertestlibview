/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "APresetData.h"

/**
 * APresetData implementation
 * 
 * Is the AbstractParent Class for all PresetData. 
 * Defines what statistical data  is calculated.
 * gets (for a given LibFile) all needed data from the APreset.
 * creates the PresetData object (for each preset) for the LibFile.
 */

namespace LibView
{
    namespace presenter
    {
        APresetData::APresetData()
        {
        }
        /**
         * @param libFile*
         */
        APresetData::APresetData(model::data_structure::LibertyFileData* libFile)
        {

        }

        
    }
}