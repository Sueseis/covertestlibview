#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibView/model/data_structure/LibertyFileData.h"

namespace LibView
{
    namespace presenter
    {


        class IMergeTemplate
        {
        public:

            /**
             * @param libFileOne
             * @param libFileTwo
             */
            virtual model::data_structure::LibertyFileData* apply(model::data_structure::LibertyFileData* libFileOne, model::data_structure::LibertyFileData* libFileTwo) const = 0;
        };


    }
}