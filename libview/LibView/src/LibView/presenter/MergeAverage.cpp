/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "MergeAverage.h"

/**
 * MergeAverage implementation
 * 
 * a template that implements the aplpy methode to merge two liberty files with 50 percent each
 */

namespace LibView
{
    namespace presenter
    {
        model::data_structure::LibertyFileData* MergeAverage::apply(model::data_structure::LibertyFileData* libFileOne, model::data_structure::LibertyFileData* libFileTwo) const
        {
            return nullptr;
        }
    }
}