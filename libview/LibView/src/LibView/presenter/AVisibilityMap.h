
#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080),Justin Guth (2218548)
 * @version 1.0
 */

#include "LibView/core/config.h"
#include "LibView/core/LibFileIndex.h"


#if LIBVIEW_AVIS_USE_UNORDERED_MAP


#include <unordered_map>
#define LIBVIEW_AVIS_MAP std::unordered_map<index_t, bool, index_t_hash>


#else

#include <map>
#define LIBVIEW_AVIS_MAP std::map<index_t, bool>

#endif

#include <vector>
#include "presets/APreset.h"

namespace LibView
{
    namespace presenter
    {

        class AVisibilityMap
        {
        public:

            /**
             * Sets the boolean in the map at a given index
             * Only works if index already present in the map.
             *
             * @param index (index_t) : Index of the map
             * @param visibility (bool) : Visibility status for this index
             *
             * @throws exception::MapIndexNotPresentException if index not included in the map
             */
            void setVisibilty(const index_t& index, bool visibility);

            /**
             * Gets the visibility at a given index.
             * Only works if index already present in the map.
             *
             * @param index (index_t) : Index of the map
             * @return bool : Visibility status for this index
             *
             * @throws exception::MapIndexNotPresentException if index not included in the map
             */
            bool getVisibility(const index_t& index) const;

            /**
             * Add an index key with the default visibility (true).
             *
             * @param index (index_t) : Index of the map
             *             
             * @throws exception::MapIndexAlreadyPresentException if index already included in the map
             */
            void addKey(const index_t& index);

            /**
             * Remove an index key from the map.
             *
             * @param index (index_t) : Index of the map to remove
             *
             * @throws exception::MapIndexNotPresentException if index not included in the 
             */
            void removeKey(const index_t& index);

            /**
             * sets the attribute visibility map to a selected preset. disables all other attributes.
             * @param preset
             */
            void setToPreset(APreset preset);

            /**
             * Adds new elements in the map.
             * Calls addKey() iteratively
             *
             * @param keyList (std::vector<index_t) : List of all keys to add
             *
             */
            void addKeys(std::vector<index_t> keyList);

            /**
             * Removes elements in the map.
             * Calls removeKey() iteratively
             *
             * @param keyList (std::vector<index_t) : List of all keys to remove
             *
             */
            void removeKeys(std::vector<index_t> keyList);

            const LIBVIEW_AVIS_MAP& getMap() const;

        protected:

            LIBVIEW_AVIS_MAP m_VisibilityMap;
        };

    }
}