/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "AVisibilityMap.h"
#include "LibView/exception/MapIndexNotPresentException.h"
#include "LibView/exception/MapIndexAlreadyPresentException.h"
#include "LibView/core/Macros.h"
 /**
  * AVisibilityMap implementation
  *
  * Is the AbstractParent Class for the visibilityMaps.
  * gives them the ability to manipulate their map.
  */

namespace LibView
{
    namespace presenter
    {


        void AVisibilityMap::setVisibilty(const index_t& index, bool visibility)
        {
            if(m_VisibilityMap.find(index) == m_VisibilityMap.end())
            {
                throw exception::MapIndexNotPresentException(LIBVIEW_LOCATION);
            }
            m_VisibilityMap[index] = visibility;
        }


        bool AVisibilityMap::getVisibility(const index_t& index) const
        {
            if(m_VisibilityMap.find(index) == m_VisibilityMap.end())
            {
                throw exception::MapIndexNotPresentException(LIBVIEW_LOCATION);
            }

            return m_VisibilityMap.at(index);
        }

        void AVisibilityMap::addKey(const index_t& index)
        {
            if(m_VisibilityMap.find(index) != m_VisibilityMap.end())
            {
                throw exception::MapIndexAlreadyPresentException(LIBVIEW_LOCATION);
            }
            m_VisibilityMap[index] = true;

        }


        void AVisibilityMap::removeKey(const index_t& index)
        {
            if(m_VisibilityMap.find(index) == m_VisibilityMap.end())
            {
                throw exception::MapIndexNotPresentException(LIBVIEW_LOCATION);
            }
            m_VisibilityMap.erase(index);
        }

        void AVisibilityMap::setToPreset(APreset preset)
        {
            //TODO to be implemented
        }


        void AVisibilityMap::addKeys(std::vector<index_t> keyList)
        {
            for(size_t i = 0; i < keyList.size(); i++)
            {
                addKey(keyList[i]);
            }
        }


        void AVisibilityMap::removeKeys(std::vector<index_t> keyList)
        {
            for(size_t i = 0; i < keyList.size(); i++)
            {
                removeKey(keyList[i]);
            }
        }
        const LIBVIEW_AVIS_MAP& AVisibilityMap::getMap() const
        {
            return m_VisibilityMap;
        }
    }
}