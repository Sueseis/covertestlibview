#include "PresenterFacade.h"
#include "LibView/model/ModelFacade.h"

#include "LibView/core/DebugLog.h"

namespace LibView
{
    namespace presenter
    {

        PresenterFacade::PresenterFacade(model::ModelFacade* modelFacade)
            :
            m_pView(),
            m_pModelFacade(modelFacade),
            m_attributeVisibility(),
            m_libFileVisibility(),
            m_PinnedLibFile(),
            m_programState(m_attributeVisibility, m_libFileVisibility, m_PinnedLibFile, modelFacade->getModelState())
        {
            m_pModelFacade->init(this);
        }

        PresenterFacade::~PresenterFacade()
        {
            delete m_pModelFacade;
        }




        void PresenterFacade::requestFileLoad()
        {
            //Call view, get file path etc

            std::vector<std::string> filePaths = m_pView->promptLibFileSelection();

            if(filePaths.size() == 0)
            {
                LIBVIEW_LOG("No Files selected. Aborting...");
                return;
            }



            LIBVIEW_LOG("Loading files...");


            for(const std::string& file : filePaths)
            {
                LIBVIEW_LOG(file);
            }



            m_pModelFacade->loadLibertyFiles(filePaths);

            for(index_t i : m_programState.getLastAddedLibFiles())
            {
                m_libFileVisibility.addKey(i);
            }

            //for(index_t i : m_programState.getLastRemovedLibFiles())
            //{
            //    m_libFileVisibility.removeKey(i);
            //}

            for(model::path_index_pair pip : m_programState.getLastAddedAttributes())
            {
                m_attributeVisibility.addKey(pip.getSecond());
            }

            //for(model::path_index_pair pip : m_programState.getLastRemovedAttributes())
            //{
            //    m_attributeVisibility.removeKey(pip.getSecond());
            //}


            // update view with new lib file data.... TODO: fix and put in right place...

            m_pView->receiveProgramState(m_programState);
        }

        void PresenterFacade::requestRemovalofLibFile(const index_t& index)
        {
            m_pModelFacade->removeLibertyFile(index);

            std::vector<index_t> toRemove;

            for(const std::pair<index_t, bool>& visPair : m_attributeVisibility.getMap())
            {
                if(m_pModelFacade->isLastRemovedAttribute(visPair.first))
                {
                    toRemove.push_back(visPair.first);
                }
            }

            for(const index_t& ind : toRemove)
            {
                m_attributeVisibility.removeKey(ind);
            }



            m_pView->receiveProgramState(m_programState);
        }


        void PresenterFacade::requestSortingOfLibFile(const index_t& groupToSortBy, bool ascending)
        {
            m_pModelFacade->sortBy(groupToSortBy, ascending);
        }


        void PresenterFacade::requestPinningOfLibFile(const index_t& index)
        {
            m_PinnedLibFile.setPinnedFile(index);
        }


        void PresenterFacade::requestLibertyFileVisibilityToggle(const index_t& index, bool visible)
        {
            m_libFileVisibility.setVisibilty(index, visible);

            m_pView->setLibFileVisibility(index, visible);
        }

        void PresenterFacade::requestAttributeVisibilityToggle(const index_t& index, bool visible)
        {
            m_attributeVisibility.setVisibilty(index, visible);

            std::vector<index_t> children = m_pModelFacade->getChildren(index);

            for(const index_t index : children)
            {
                m_attributeVisibility.setVisibilty(index, visible);
            }

            //LIBVIEW_LOG(m_pModelFacade->getAttributeName(index));

            m_pView->updateAttributeVisibilities(m_programState);


        }

        void PresenterFacade::requestSearchList(std::string searchTerm)
        {
            m_pModelFacade->getMatchingAttributes(searchTerm);
        }

        void PresenterFacade::requestGraphTypeChange(std::string graphType)
        {
            //TODO cf signature
        }


        void PresenterFacade::requestPresetSelection(APreset preset)
        {
            //TODO add preset selection
        }


        std::vector<index_t> PresenterFacade::requestChildrenIndices(const index_t& index) const
        {
            return m_pModelFacade->getChildren(index);
        }


        void PresenterFacade::notifyOfModelStateUpdate(model::ModelState& modelState)
        {
            updateAttributeVisibilityMap(modelState);
            updateLibFileVisibilityMap(modelState);
            updatePresets(modelState);
            updatePresetData(modelState);
        }


        void PresenterFacade::requestMerging(IMergeTemplate* templ, const index_t& libFileOne, const index_t& libFileTwo)
        {
            //TODO add merging
        }

        void PresenterFacade::init(view::IView* view)
        {
            m_pView = view;
        }

        void PresenterFacade::requestContentDisplay(const index_t& index)
        {
            //m_pView->setContentDisplayContent("Test set display");
            m_pView->setContentDisplayContent(m_pModelFacade->getLibFileByIndex(index)->getFileContents());

        }

        void PresenterFacade::requestPresetSelection(const index_t& index)
        {
            setAllAttributesInvisible();

            for(std::pair<index_t, bool> pair : m_attributeVisibility.getMap())
            {
                std::string attribPath = m_pModelFacade->getAttributeName(pair.first);
                if(m_pModelFacade->getPreset(index)->containsPath(attribPath))
                {
                    m_attributeVisibility.setVisibilty(pair.first, true);
                }
            }

            m_pView->updateAttributeVisibilities(m_programState);
            
        }

        void PresenterFacade::requestSearch(const std::string& reg)
        {
            setAllAttributesInvisible();

            for(std::pair<index_t, bool> pair : m_attributeVisibility.getMap())
            {
                std::string attribPath = m_pModelFacade->getAttributeName(pair.first);
                
                if(std::regex_match(attribPath, std::regex(reg)) || (attribPath.find(reg) != std::string::npos))
                {
                    m_attributeVisibility.setVisibilty(pair.first, true);
                }
            }

            m_pView->updateAttributeVisibilities(m_programState);
        }

        void PresenterFacade::updateAttributeVisibilityMap(model::ModelState& modelState)
        {
            std::vector<index_t> addK;
            std::vector<index_t> remK;

            for(const model::path_index_pair& pip : modelState.getLastAddedAttributes())
            {
                addK.push_back(pip.getSecond());
            }

            for(const model::path_index_pair& pip : modelState.getLastRemovedAttributes())
            {
                remK.push_back(pip.getSecond());
            }

            m_attributeVisibility.addKeys(addK);
            m_attributeVisibility.removeKeys(remK);

        }

        void PresenterFacade::updateLibFileVisibilityMap(model::ModelState& modelState)
        {
            m_libFileVisibility.addKeys(modelState.getLastAddedLibFiles());
            m_libFileVisibility.removeKeys(modelState.getLastRemovedLibFiles());

        }


        void PresenterFacade::updatePresets(model::ModelState& modelState)
        {
            //TODO iterate over last added and last removed attributes, change presets
        }


        void PresenterFacade::updatePresetData(model::ModelState& modelState)
        {
            //TODO iterate over last added and last removed attributes, change preset data
        }


        void PresenterFacade::setAttributeChildrenVisibilities(const index_t& index, bool visible)
        {

            for(index_t currentIndex : requestChildrenIndices(index))
            {
                m_attributeVisibility.setVisibilty(currentIndex, visible);
            }
        }

        void PresenterFacade::setAllAttributesInvisible()
        {
            m_attributeVisibility.setAllAttributesInvisible();
        }


        const std::vector<model::APreset*>& PresenterFacade::getPresetList() const
        {
            return m_pModelFacade->getPresetList();
        }
    }
}