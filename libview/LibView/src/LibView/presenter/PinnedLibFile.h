#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <stdint.h>
#include "LibView/core/LibFileIndex.h"

namespace LibView
{
    namespace presenter
    {

        class PinnedLibFile
        {
        public:
            /**
             * Default constructor for PinnedLibFile
             */
            PinnedLibFile();

            /**
             * Returns if any LibFile is currently pinned
             *
             * @return bool : Returns true if a file is currently pinned, else false
            */
            bool isFilePinned() const;

            /**
              * Returns if a LibFile with given index is currently pinned
              *
              * @param index (index_t) : The index of the file to be checked
              * @return bool : Returns true if index is currently pinned, else false
             */
            bool isFileIndexPinned(const index_t& index) const;

            /**
             * Returns the index of the currently pinned LibFile
             *
             * @return index_t : Index of the currently pinned LibFile
             *
             * @throws exception::NoFilePinnedException if index is irregular
             */
            index_t getPinnedFileIndex() const;

            /**
             * Sets a pinned LibFile with its index
             *
             * @param index (index_t) : Index to be set to the pinned file
             */
            void setPinnedFile(const index_t& index);

            /**
             * Sets the currently pinned file index to an irregular index
             */
            void unpin();
        private:
            index_t m_PinnedFile;
        };

    }
}