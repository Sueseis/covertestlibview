/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "PinnedLibFile.h"
#include "LibView/exception/presenter/NoFilePinnedException.h"
#include "LibView/core/Macros.h"

 /**
  * PinnedLibFile implementation
  *
  * Holds the information which Libfile is currently pinned.
  */


namespace LibView
{
    namespace presenter
    {



        PinnedLibFile::PinnedLibFile() :
            m_PinnedFile(index_t::requestNull())
        {}

        bool PinnedLibFile::isFilePinned() const
        {
            return !m_PinnedFile.isNull();
        }

        bool PinnedLibFile::isFileIndexPinned(const index_t& index) const
        {
            if(index != index_t::requestNull())
            {
                return index == m_PinnedFile;

            }
            return false;
        }

        index_t PinnedLibFile::getPinnedFileIndex() const
        {
            if(!isFilePinned())
            {
                throw exception::NoFilePinnedException(LIBVIEW_LOCATION);
            }
            return m_PinnedFile;
        }

        void PinnedLibFile::setPinnedFile(const index_t& index)
        {
            m_PinnedFile = index;
        }

        void PinnedLibFile::unpin()
        {
            m_PinnedFile = index_t::requestNull();
        }
    }
}