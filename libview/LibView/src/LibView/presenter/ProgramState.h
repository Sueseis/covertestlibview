#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph B�hrer (2070080),Justin Guth (2218548)
 * @version 1.0
 */

#include <stdint.h>
#include "LibFileVisibilityMap.h"
#include "AttributeVisibilityMap.h"
#include "LibView/model/data_structure/LibertyFileData.h"
#include "LibView/model/ModelState.h"
#include "PinnedLibFile.h"
#include <vector>


namespace LibView
{
	namespace presenter
	{
		/**
		 * Stores const references to the programs data. Makes only const getters available. 
		 * Solely functions as a data throughput and accessibility filter.
		 */
		class ProgramState {
		public:

			/**
			 * constructor method for the Programstate
			 *
			 * @param attributeVisibilityMap (const AttributeVisibilityMap&) : The reference to the AttributeVisibilityMap held in the presenter.
			 * @param libFileVisibilityMap (const LibFileVisibilityMap&) : The reference to the LibFileVisibilityMap held in the presenter.
			 * @param pinnedLibFile (const PinnedLibFile&) : The reference to the PinnedLibFile held by the presenter
			 * @param modelState (const model::ModelState&) : The reference to the model::ModelState held by the presenter
			 */
			ProgramState(
				const AttributeVisibilityMap& attributeVisibilityMap,
				const LibFileVisibilityMap& libFileVisibilityMap,
				const PinnedLibFile& pinnedLibFile,
				const model::ModelState& modelState
			);


			/**
			 * Returns the visibility for a given lib file index.
			 *
			 * @param index (index_t) : The index of the Lib File.
			 *
			 * @return bool : The visibility of the lib file.
			 *
			 * @throws exception::LibFileIndexNotFoundException if LibFile index does not exist.
			 */
			bool getLibFileVisibility(const index_t& index) const;

			/**
			 * Returns the visibility for a given attribute index.
			 *
			 * @param index (index_t) : The index of the attribute.
			 *
			 * @return bool : The visibility of the attribute.
			 *
			 * @throws exception::AttributeIndexNotFoundException if attribute index does not exist.
			 */
			bool getAttributeVisibility(const index_t& index) const;
			
			
			/**
			 * returns the attribute visibility for a given path
			 */
			bool getAttributeVisibility(const std::string& path) const;

			/**
			 * Returns a const reference to the libfile data for a libfile with the given index.
			 * 
			 * @param index (index_t) : The index of the LibFile.
			 *
			 * @return model::data_structure::LibertyFileData* : The pointer to the data at this index.
			 *
			 * @throws exception::LibFileIndexNotFoundException if LibFile index does not exist. 
			 */
			model::data_structure::LibertyFileData* getLibFile(const index_t& index) const;

			/**
			 * returns a list of all loaded lib files
			 */
			std::vector< model::data_structure::LibertyFileData*> getLibFileList() const;

			/**
			 * Returns a const reference to the libfile group 
			 *
			 * @param index (index_t) : The index of the attribute.
			 *
			 * @throws exception::AttributeIndexNotFoundException if attribute index does not exist.
			 */
			std::string getAttribute(const index_t& index) const;
			
			
			/**
			 * returns the index of an attribute with a given path
			 */
			index_t getAttribute(const std::string& path) const;


			/**
			 * returns a list of last added liberty files
			 */
			std::vector<index_t> getLastAddedLibFiles() const;
			
			/**
			 * returns a list of last removed liberty files
			 */
			std::vector<index_t> getLastRemovedLibFiles() const;

			/**
			 * returns a list of last added attributes
			 */
			std::vector<model::path_index_pair> getLastAddedAttributes() const;
			
			/**
			 * returns a list of last removed attributes
			 */
			std::vector<model::path_index_pair> getLastRemovedAttributes() const;


			/**
			 * Returns the const reference of the currently pinned libFile (Does not check for whether a file is pinned
			 *
			 * @return model::data_structure::LibertyFileData* : The reference of the pinned lib file
			 */
			model::data_structure::LibertyFileData* getPinnedFileIndex() const;
			
			
			/**
			 * returns a list of all attributes with their visibilities
			 */
			const LIBVIEW_AVIS_MAP& getAttributeList() const;


		private:

			bool m_LibFilesAdded;
			bool m_LibFilesRemoved;



			/**
			 * A reference to the PinnedLibFile instance
			 */
			const PinnedLibFile& m_pinnedFile;
			
			/**
			 * A reference to the AttributeVisibilityMap instance
			 */
			const AttributeVisibilityMap& m_attributeVisibility;
			
			/**
			 * A reference to the LibFileVisibilityMap instance
			 */
			const LibFileVisibilityMap& m_libFileVisibility;
			
			/**
			 * A reference to the model::ModelState instance
			 */
			const model::ModelState& m_ModelState;

		};
	}
}