#pragma once

#include <QStandardItemModel>
#include "AttributeVisibilityToggleTreeItem.h"
#include "ViewKnower.h"


namespace LibView
{
    namespace view
    {
        class AttributeVisibilityToggleTreeItemModel : public QStandardItemModel, public ViewKnower
        {
            Q_OBJECT

        private:

            bool m_IsInitialised;


        public:
            
            AttributeVisibilityToggleTreeItemModel(int rows, int columns, IView* view);
            inline void setInit() { m_IsInitialised = true; }
            inline void setDeInit() { m_IsInitialised = false; }

        public slots:

            void onItemChanged(QStandardItem*);
        };
    }
}