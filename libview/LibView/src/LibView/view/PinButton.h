#pragma once

#ifndef LIBVIEW_TEST


#include "LibView/qt/Qt_PinButton.h"

namespace LibView
{
    namespace view
    {
        class PinButton : private Qt_PinButton
        {
            friend class LibFileHeader;

        private:

            QPushButton* m_PinButtonPushButton;

        public:

            PinButton(QWidget* parent = Q_NULLPTR);

        };
    }
}


#endif // !LIBVIEW_TEST
