#pragma once


#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_LibFileHeader.h"
#include "PinButton.h"

#include <stdint.h>
#include <string>

namespace LibView
{
    namespace view
    {
        class LibFileHeader : public Qt_LibFileHeader
        {
        private:

            //QLabel* m_TitleLabel;
            QLabel* m_NameLabel;
            //QWidget* m_PinFilePlaceholder;
            //PinButton* m_PinButton;

            std::string m_Title;
            std::string m_Name;
            int64_t m_LibFileIndex;

        public:

            LibFileHeader(QWidget* parent = Q_NULLPTR);

            //inline void setTitle(std::string title) { m_Title = title; /*m_TitleLabel->setText(title.c_str()); */ };
            inline void setName(std::string name) { m_Name = name; m_NameLabel->setText(name.c_str()); };
            inline void assignIndex(uint64_t index) { m_LibFileIndex = index; }

        };
    }
}


#endif // !LIBVIEW_TEST
