#include "PresetDisplayOverview.h"

#ifndef LIBVIEW_TEST


namespace LibView
{
    namespace view
    {
        void PresetDisplayOverview::initUI()
        {
            setLayout(m_MainLayout);
            m_MainLayout->addWidget(m_PresetTitle, 0,0,1,1);
            m_MainLayout->addWidget(m_PresetDataBox, 2,0,1,1);

        }



        PresetDisplayOverview::PresetDisplayOverview(QWidget* parent):
            Qt_PresetDisplayOverview(parent),
            m_MainLayout(new QGridLayout(this)),
            m_PresetTitle(new PresetTitle(this)),
            m_PresetDataBox(new PresetDataBox(this))
        {
            initUI();
        }



    }
}

#endif