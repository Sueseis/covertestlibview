#pragma once

#include "LibView/qt/Qt_LibFileDataColumn.h"
#include "ILibViewGuiElement.h"
#include "LibFileHeader.h"

#include "LibView/core/LibFileIndex.h"

#include "LibView/model/data_structure/LibertyFileData.h"

#include <QGridLayout>

#include "GroupDisplayContainer.h"

namespace LibView
{
    namespace view
    {
        class LibFileDataColumn : public Qt_LibFileDataColumn, public ILibViewGuiElement
        {
        private:

            IView* m_View;
            LibFileHeader* m_Header;

            QVBoxLayout* m_Layout;

            index_t m_Index;

            void createDisplay(const model::data_structure::LibertyFileData* data, const presenter::ProgramState& state);

            void renderGroup(model::data_structure::LibertyFileGroup* group, const presenter::ProgramState& state, GroupDisplayContainer* parent = nullptr);

            std::vector<GroupDisplayContainer*> m_GroupDisplayContainers;
            std::vector<GroupDisplayContainer*> m_Roots;

        public:

            IView* getViewPointer() const override;

            LibFileDataColumn(const presenter::ProgramState& state, const model::data_structure::LibertyFileData* data, QWidget* parent = Q_NULLPTR, IView* viewPointer = nullptr);

            index_t getIndex() const;

            void updateVisibilities(const presenter::ProgramState& state);

        };
    }
}