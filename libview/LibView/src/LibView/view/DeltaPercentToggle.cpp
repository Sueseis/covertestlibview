#ifndef LIBVIEW_TEST


#include "DeltaPercentToggle.h"


namespace LibView
{
    namespace view
    {
        DeltaPercentToggle::DeltaPercentToggle(QWidget* parent) :

            Qt_DeltaPercentToggle(parent),
            
            m_DeltaButton(findChild<QRadioButton*>("DeltaSwitchDeltaButton", Qt::FindChildOption::FindChildrenRecursively)), 
            
            m_PercentileButton(findChild<QRadioButton*>("DeltaSwitchPercentageButton", Qt::FindChildOption::FindChildrenRecursively))
        {}
        bool DeltaPercentToggle::isDeltaSelected() const
        {
            return m_DeltaButton->isChecked();
        }
        bool DeltaPercentToggle::isPercentileSelected() const
        {
            return m_PercentileButton->isChecked();
        }
      
    }
}


#endif