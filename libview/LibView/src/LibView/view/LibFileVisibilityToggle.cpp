#include "LibFileVisibilityToggle.h"

#include "RemoveButton.h"



#ifndef LIBVIEW_TEST


namespace LibView
{
    namespace view
    {
        LibFileVisibilityToggle::LibFileVisibilityToggle(const std::string& name, const index_t& index, QWidget* parent, IView* pView) :
            Qt_LibFileVisibilityToggle(parent),
            m_Name(name),
            m_View(pView),
            m_Index(index),
            m_RemoveButtonPlaceHolder(findChild<QWidget*>("RemoveButtonPlaceHolder", Qt::FindChildOption::FindChildrenRecursively)),
            m_LibFileVisibilityToggle(findChild<QCheckBox*>("LibFileVisibilityToggleCheckBox", Qt::FindChildOption::FindChildrenRecursively))
        {


            initUI();

            m_LibFileVisibilityToggle->setChecked(true);

            connect(m_LibFileVisibilityToggle, SIGNAL(clicked()), this, SLOT(requestLibFileVisibility()));
        }

        void LibFileVisibilityToggle::requestLibFileVisibility()
        {
            m_View->requestLibFileVisibility(m_Index, m_LibFileVisibilityToggle->isChecked());
        }

        IView* LibFileVisibilityToggle::getViewPointer() const
        {
            return m_View;
        }

        void LibFileVisibilityToggle::initUI()
        {
            layout()->setContentsMargins(10, 0, 0, 0);
            layout()->setSpacing(0);
            m_LibFileVisibilityToggle->setText(m_Name.c_str());
            QGridLayout* removeButtonLayout = new QGridLayout(m_RemoveButtonPlaceHolder);
            removeButtonLayout->setMargin(0);
            removeButtonLayout->setSpacing(0);

            RemoveButton* removeButton = new RemoveButton(m_Index, m_RemoveButtonPlaceHolder, "x", m_View);
            m_RemoveButtonPlaceHolder->setLayout(removeButtonLayout);
            removeButtonLayout->addWidget(removeButton, 0, 0, 1, 1);
           
        }
    }
}

#endif