#pragma once


#ifndef LIBVIEW_TEST


#include "LibView/qt/Qt_DeltaPercentToggle.h"


namespace LibView
{
    namespace view
    {
        class DeltaPercentToggle : public Qt_DeltaPercentToggle
        {
        private:

            QRadioButton* m_DeltaButton;
            QRadioButton* m_PercentileButton;



        public:

            DeltaPercentToggle(QWidget* parent = Q_NULLPTR);

            bool isDeltaSelected() const;
            bool isPercentileSelected() const;

        };
    }
}


#endif