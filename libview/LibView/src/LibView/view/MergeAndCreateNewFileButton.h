#pragma once

#ifndef LIBVIEW_TEST
#include"LibView/qt/Qt_MergeAndCreateNewFileButton.h"

namespace LibView
{
    namespace view
    {
        class MergeAndCreateNewFileButton : private Qt_MergeAndCreateNewFileButton
        {
        private:
            QPushButton* m_MergeAndCreateNewFileButton;

        public:
            MergeAndCreateNewFileButton(QWidget* parent = Q_NULLPTR);
        };
    }
}
#endif