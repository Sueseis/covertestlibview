#pragma once

#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_PresetTitle.h"

namespace LibView
{
    namespace view
    {
        class PresetTitle : public Qt_PresetTitle
        {
        private:

            QLabel* m_TitlePrefixLabel;
            QLabel* m_TitleNameLabel;

        public:

            PresetTitle(QWidget* parent = Q_NULLPTR);
            ~PresetTitle();


            inline void setTitleName(const std::string& title) { m_TitleNameLabel->setText(QString(title.c_str())); }
            inline void setTitlePrefix(const std::string& prefix) { m_TitleNameLabel->setText(QString(prefix.c_str())); }
        };

    }
}
#endif