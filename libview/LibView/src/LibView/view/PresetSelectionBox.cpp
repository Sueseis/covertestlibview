#include "PresetSelectionBox.h"

#ifndef LIBVIEW_TEST


namespace LibView
{
    namespace view
    {
        PresetSelectionBox::PresetSelectionBox(IView* view, QWidget* parent) :
            Qt_PresetSelectionBox(parent),
            ViewKnower(view),
            m_MainLayout(new QVBoxLayout(this)),
            m_Rows(std::vector<PresetVisibilityToggle*>())
        {
            initUI();
        }

        void PresetSelectionBox::addPresetRow(const std::string& presetName, const index_t& index)
        {
            PresetButton* button = new PresetButton(getView(), index, presetName, this);
            m_MainLayout->addWidget(button);


        }
        void PresetSelectionBox::initUI()
        {
            setLayout(m_MainLayout);

        }

        void PresetSelectionBox::loadPresets()
        {
            for(const model::APreset* preset : getView()->getPresetList())
            {
                addPresetRow(preset->getName(), preset->index());
            }
        }


    }
}
#endif