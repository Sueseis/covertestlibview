#pragma once


//#ifndef LIBVIEW_TEST
#include "LibView/qt/Qt_LoadLibFileButton.h"
#include "ILibViewGuiElement.h"

namespace LibView
{
    namespace view
    {
        class LoadLibFileButton : public Qt_LoadLibFileButton, public ILibViewGuiElement
        {
            Q_OBJECT
        private:
            
            QPushButton* m_LoadLibFileButton;
            IView* m_ViewPointer;

        private slots:

            void exit_app();
            void onButtonClick();



        public:
            LoadLibFileButton(QWidget* parent = Q_NULLPTR, IView* viewPointer = nullptr);

            IView* getViewPointer() const override;
            
         };
    }
}


//#endif