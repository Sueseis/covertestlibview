#ifndef LIBVIEW_TEST
#include "ExportCsvButton.h"

namespace LibView
{
    namespace view
    {
        ExportCsvButton::ExportCsvButton(QWidget* parent) : 
            Qt_ExportCsvButton(parent),
            m_ExportCsvButton(findChild<QPushButton*>("ExportCsvButtonPushButton",
            Qt::FindChildOption::FindChildrenRecursively))
        {}
    }
}
#endif
