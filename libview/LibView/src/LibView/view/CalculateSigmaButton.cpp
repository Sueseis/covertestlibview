#ifndef LIBVIEW_TEST
#include "CalculateSigmaButton.h"

namespace LibView
{
    namespace view
    {
        CalculateSigmaButton::CalculateSigmaButton(QWidget* parent) :
            Qt_CalculateSigmaButton(parent),
            m_CalculateSigmaButton(findChild<QPushButton*>("CalculateSigmaButtonPushButton", Qt::FindChildOption::FindChildrenRecursively))
        {}
    }
}



#endif