#pragma once
#ifndef LIBVIEW_TEST
#include"LibView/qt/Qt_CalculateSigmaButton.h"

namespace LibView
{
    namespace view
    {
        class CalculateSigmaButton : private Qt_CalculateSigmaButton
        {
        private:
            QPushButton* m_CalculateSigmaButton;
        public:
            CalculateSigmaButton(QWidget* parent = Q_NULLPTR);
        };
    }
}
#endif