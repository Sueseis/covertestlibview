#include "PresetDataBox.h"

#ifndef LIBVIEW_TEST


namespace LibView
{
    namespace view
    {
        PresetDataBox::PresetDataBox(QWidget* parent):
            Qt_PresetDataBox(parent),
            m_MainLayout(new QGridLayout(this)),
            m_Rows(std::vector <PresetDataRow*>()),
            m_NumberOfRows(0)
        {}

        void PresetDataBox::addDataRow(const std::string& title, const std::string& value)
        {
            PresetDataRow* newRow = new PresetDataRow(title, value, this);
            m_Rows.push_back(newRow);
            m_MainLayout->addWidget(newRow, m_NumberOfRows, 0, 1, 1);
            m_NumberOfRows++;

        }

    }
}
#endif