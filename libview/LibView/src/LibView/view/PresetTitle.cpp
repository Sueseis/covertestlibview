#include "PresetTitle.h"

#ifndef LIBVIEW_TEST

namespace LibView
{
    namespace view
    {
        PresetTitle::PresetTitle(QWidget* parent) :
            Qt_PresetTitle(parent),
            m_TitleNameLabel(findChild<QLabel*> ("PresetTitleName", Qt::FindChildOption::FindChildrenRecursively)),
            m_TitlePrefixLabel(findChild<QLabel*> ("PresetTitlePrefix", Qt::FindChildOption::FindChildrenRecursively))
        {}

        PresetTitle::~PresetTitle()
        {}
    }
}
#endif