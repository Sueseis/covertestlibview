#include "GroupDisplayContainer.h"

namespace LibView
{
    namespace view
    {
        void GroupDisplayContainer::setSubGroupContainerVisible()
        {
            if(!m_IsRoot)
            {
                ((GroupDisplayContainer*) parent())->setSubGroupContainerVisible();
            }

            m_SubGroupContainer->setVisible(true);
        }


        GroupDisplayContainer::GroupDisplayContainer(bool isRoot, index_t& index, std::string name, std::string value, QWidget* parent) :
            QWidget(parent),
            m_IsRoot(isRoot),
            m_SubGroups(std::vector<GroupDisplayContainer*>()),
            m_IsDataVisible(true),
            m_Index(index),
            m_AttributeRows(0),
            m_AttributeTable(new QWidget(this)),
            m_SubGroupContainer(new QWidget(this)),
            m_AttributeTableLayout(new QGridLayout()),
            m_SubGroupContainerLayout(new QVBoxLayout())
        {
            QVBoxLayout* mainLayout = new QVBoxLayout(this);

            setObjectName("GroupDisplayContainer");

            setStyleSheet(
                
                R"(
                
                QWidget#GroupDisplayContainer {

                    border: 1px solid #9b9b9b;
                }

                   
                )");

            m_AttributeTable->setObjectName("AttributeTable");

            m_AttributeTable->setStyleSheet(
                R"(
                QWidget#AttributeTable {

                    border: 1px dashed #9b9b9b;
                }
                )"
            );

            m_SubGroupContainer->setObjectName("SubGroupContainer");
            m_AttributeTableLayout->setParent(m_AttributeTable);
            m_SubGroupContainerLayout->setParent(m_SubGroupContainer);

            m_SubGroupContainer->setLayout(m_SubGroupContainerLayout);
            m_AttributeTable->setLayout(m_AttributeTableLayout);

            mainLayout->addWidget(new QLabel(name.c_str()));
            mainLayout->addWidget(m_AttributeTable);
            mainLayout->addWidget(m_SubGroupContainer);

            setLayout(mainLayout);
        }
        GroupDisplayContainer::~GroupDisplayContainer()
        {}
        void GroupDisplayContainer::insertAttribute(const std::string & name, const std::string & value)
        {
            m_AttributeTableLayout->addWidget(new QLabel(name.c_str()), m_AttributeRows, 0, 1, 1);
            m_AttributeTableLayout->addWidget(new QLabel(value.c_str()), m_AttributeRows, 1, 1, 1);
            ++m_AttributeRows;
        }
        void GroupDisplayContainer::insertSubGroup(GroupDisplayContainer* groupDisplay)
        {
            groupDisplay->setParent(this);
            m_SubGroupContainerLayout->addWidget(groupDisplay);
            m_SubGroups.push_back(groupDisplay);
        }

        void GroupDisplayContainer::setDataVisible(bool visibility)
        {
            m_IsDataVisible = visibility;
            m_AttributeTable->setVisible(visibility);

            if(m_SubGroups.size() == 0)
            {
                setVisible(visibility);
            }
        }

        bool GroupDisplayContainer::updateOverallVisibility()
        {
            if(m_SubGroups.size() == 0)
            {
                return m_IsDataVisible;
            }            
            
            bool returnTrue = false;
            
            for(GroupDisplayContainer* childDisplay : m_SubGroups)
            {
                if(childDisplay->updateOverallVisibility())
                {
                    returnTrue = true;
                }
            }

            setVisible(returnTrue);

            return returnTrue;
        }        
    }
}