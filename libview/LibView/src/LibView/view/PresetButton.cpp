#include "PresetButton.h"

namespace LibView
{
    namespace view
    {
        PresetButton::PresetButton(IView* pView, const index_t& index, const std::string& name, QWidget* parent) :
            QPushButton(name.c_str(), parent),
            ViewKnower(pView),
            m_Index(index)
        {
            connect(this, SIGNAL(clicked()), this, SLOT(onButtonClick()));
        }


        void PresetButton::onButtonClick()
        {
            getView()->requestPresetLoad(m_Index);
        }
    }
}