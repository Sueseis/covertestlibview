#include "MergeAndCreateNewFileButton.h"


#ifndef LIBVIEW_TEST
namespace LibView
{
    namespace view
    {
        MergeAndCreateNewFileButton::MergeAndCreateNewFileButton(QWidget* parent): 
            Qt_MergeAndCreateNewFileButton(parent),
            m_MergeAndCreateNewFileButton(findChild<QPushButton*>("MergeAndCreateNewFileButtonPushButton",
                Qt::FindChildOption::FindChildrenRecursively))
        {}
    }
}

#endif