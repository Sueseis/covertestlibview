#pragma once

#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_LibFileSelectionComboBox.h"
#include "LibView/core/LibFileIndex.h"
#include "IView.h"
#include "ILibViewGuiElement.h"

namespace LibView
{
    namespace view
    {
        class LibFileSelectionComboBox :
            public Qt_LibFileSelectionComboBox,
            public ILibViewGuiElement
        {
            Q_OBJECT
        private:
            QComboBox* m_LibFileSelectionComboBox;
            IView* m_ViewPointer;
            std::vector<index_t> m_Indices;
            void initUI();

        public slots:

            void onBoxSelected(int selectedIndex);

        public:
            LibFileSelectionComboBox(QWidget* parent = Q_NULLPTR, IView* viewPointer = nullptr);
            void clear();
            void remove(const index_t& index);

            void addLibFile(const std::string& name, const index_t& index);
            IView* getViewPointer() const override;
            void loadLibFile(const model::data_structure::LibertyFileData* fileData);

        };
    }
}
#endif