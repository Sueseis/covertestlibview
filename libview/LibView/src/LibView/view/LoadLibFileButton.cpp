


#include "LibView/core/DebugLog.h"
#include "LoadLibFileButton.h"
#include <iostream>

#include "SideMenu.h"

namespace LibView
{
    namespace view
    {
        LoadLibFileButton::LoadLibFileButton(QWidget* parent, IView* viewPointer) :
            Qt_LoadLibFileButton(parent),
            m_LoadLibFileButton(findChild<QPushButton*>("LoadLibFileButton", Qt::FindChildOption::FindChildrenRecursively)),

            m_ViewPointer(viewPointer)
        {
          //connect(m_LoadLibFileButton, SIGNAL(clicked()), this, SLOT(exit_app()));
          connect(m_LoadLibFileButton, SIGNAL(clicked()), this, SLOT(onButtonClick()));
          //connect(m_LoadLibFileButton, SIGNAL(clicked()), m_otherClassPointer, SLOT(nameOfMethod()));
         
        }
        IView* LoadLibFileButton::getViewPointer() const
        {
            //return ((ILibViewGuiElement*) (parent()))->getViewPointer();
            return m_ViewPointer;
        }
        void LoadLibFileButton::exit_app()
        {
            m_LoadLibFileButton->setText("test123");
            LIBVIEW_LOG("test");
        }
        
        void LoadLibFileButton::onButtonClick() {


            IView* view = getViewPointer();
            view->notifyPresenterOfFileLoadRequest();
        }
    }
}
