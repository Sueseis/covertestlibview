#include "LibFileDataColumn.h"
#include "LibView/model/data_structure/LibertyFileGroup.h"



namespace LibView
{
    namespace view
    {
        void LibFileDataColumn::createDisplay(const model::data_structure::LibertyFileData* fileData, const presenter::ProgramState& state)
        {
            model::data_structure::LibertyFileGroup* libraryGroup = fileData->getLibraryGroup();

            renderGroup(libraryGroup, state);
        }

        void LibFileDataColumn::renderGroup(model::data_structure::LibertyFileGroup* givenGroup, const presenter::ProgramState& state, GroupDisplayContainer* parent)
        {
            std::string path = givenGroup->getPath();
            index_t index = state.getAttribute(path);


            GroupDisplayContainer* row = new GroupDisplayContainer(
                givenGroup->getParent() == nullptr,
                index,
                givenGroup->getDisplayName().c_str(),
                "group.gertvalue",
                this);

            if(parent == nullptr)
            {
                m_Roots.push_back(row);
            }

            std::vector<std::string> attributeNames = givenGroup->getOrderedAttributes();
            std::vector<std::string> attributeValues = givenGroup->getOrderedAttributeValues();

            for(size_t i = 0; i < attributeNames.size(); i++)
            {
                row->insertAttribute(attributeNames[i], attributeValues[i]);
            }


            row->setVisible(state.getAttributeVisibility(givenGroup->getPath()));
            m_GroupDisplayContainers.push_back(row);

            if(parent != nullptr)
            {
                parent->insertSubGroup(row);
            }
            else
            {
                m_Layout->addWidget(row);
            }

            for(model::data_structure::LibertyFileGroup* group : givenGroup->getChildren())
            {
                renderGroup(group, state, row);
            }
        }

        IView* LibFileDataColumn::getViewPointer() const
        {
            return m_View;
        }

        LibFileDataColumn::LibFileDataColumn(const presenter::ProgramState& state, const model::data_structure::LibertyFileData* data, QWidget* parent, IView* viewPointer)
            :
            Qt_LibFileDataColumn(parent),
            m_GroupDisplayContainers(std::vector<GroupDisplayContainer*>()),
            m_Header(new LibFileHeader(this)),
            m_View(viewPointer),
            m_Layout(new QVBoxLayout(this)),
            m_Index(data->getLibFileIndex())
        {
            //m_Header->setTitle("");
            m_Header->setName(data->getDisplayName());
            m_Layout->setAlignment(Qt::AlignTop);
            m_Layout->addWidget(m_Header);
            setLayout(m_Layout);


            //setStyleSheet(
            //
            //    R"(
            //    QWidget { 
            //        
            //        border: 1px solid red;
            //    }
            //
            //    )"
            //
            //);

            createDisplay(data, state);
        }
        index_t LibFileDataColumn::getIndex() const
        {
            return m_Index;
        }
        void LibFileDataColumn::updateVisibilities(const presenter::ProgramState& state)
        {
            for(GroupDisplayContainer* row : m_GroupDisplayContainers)
            {
                index_t& index = row->getIndex();
                bool visibility = state.getAttributeVisibility(index);
                row->setDataVisible(visibility);
            }

            for(GroupDisplayContainer* root : m_Roots)
            {
                root->updateOverallVisibility();
            }
        }
    }
}