#pragma once
#ifndef LIBVIEW_TEST
#include "LibView/qt/Qt_ExportCsvButton.h"
namespace LibView
{
    namespace view
    {
        class ExportCsvButton : public Qt_ExportCsvButton
        {
        private:
            QPushButton* m_ExportCsvButton;
        public:
            ExportCsvButton(QWidget* parent = Q_NULLPTR);
        };
    }
}



#endif