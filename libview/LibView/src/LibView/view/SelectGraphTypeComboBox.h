#pragma once


#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_SelectGraphTypeComboBox.h"

namespace LibView
{
    namespace view
    {
        class SelectGraphTypeComboBox : public Qt_SelectGraphTypeComboBox
        {
        private: 
            QComboBox* m_SelectGraphTypeComboBox;

        public:
            SelectGraphTypeComboBox(QWidget* parent = Q_NULLPTR);
        };
    }
}




#endif