#pragma once

#ifndef LIBVIEW_TEST



#include <QMainWindow>

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include <QGridLayout>
#include <QSplitter>
#include "MainTabBar.h"

#include "ILibViewGuiElement.h"

#include "IView.h"

namespace LibView
{

    namespace view
    {


        class LibViewMainWindow : public QMainWindow, public ILibViewGuiElement
        {

        private:

            QWidget* m_Centralwidget;
            QGridLayout* m_CentralWidgetLayout;
            MainTabBar* m_MainTabBar;
            QMenuBar* m_Menubar;
            QStatusBar* m_Statusbar;
            SideMenu* m_SideMenu;
            IView* m_ViewPointer;

            QSplitter* m_Splitter;

            void initUI();

        public:

            void setContentDisplay(const std::string& text);

            LibViewMainWindow(IView* viewPointer);

            IView* getViewPointer() const override;

            void setContentDisplayContent(const std::string& text);

            void clearLibFiles();
            void remove(const index_t& index);
            void loadLibFile(const presenter::ProgramState& state, const model::data_structure::LibertyFileData* fileData);
            void loadAttributeList(const presenter::ProgramState& state);

            void setLibFileVisibility(const index_t& index, bool visibility);

            void updateAttributeVisibilities(const presenter::ProgramState& state);

            void redrawLibFiles(const presenter::ProgramState& state);

            void init();
        };
    }
}

#endif