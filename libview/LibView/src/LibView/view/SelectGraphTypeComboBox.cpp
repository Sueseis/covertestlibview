#ifndef LIBVIEW_TEST


#include "SelectGraphTypeComboBox.h"

namespace LibView
{
    namespace view
    {
        SelectGraphTypeComboBox::SelectGraphTypeComboBox(QWidget* parent) : 
            Qt_SelectGraphTypeComboBox(parent),
            m_SelectGraphTypeComboBox(findChild<QComboBox*>
            ("SelectGraphTypeComboBoxComboBox", Qt::FindChildOption::FindChildrenRecursively))
        {}

    }
}
#endif