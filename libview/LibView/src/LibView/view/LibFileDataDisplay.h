#pragma once

#include "LibView/qt/Qt_LibFileDataDisplay.h"
#include "LibView/core/LibFileIndex.h"
#include "ILibViewGuiElement.h"
#include "LibView/model/data_structure/LibertyFileData.h"
#include "LibFileDataColumn.h"

#include <vector>

#include <QGridLayout>
#include <QScrollArea>

namespace LibView
{
    namespace view
    {
        class LibFileDataDisplay : public Qt_LibFileDataDisplay, public ILibViewGuiElement
        {
        private:

            IView* m_View;

            IView* getViewPointer() const override;

            //QGridLayout* m_Layout;

            QScrollArea* m_ScrollArea;

            std::vector<LibFileDataColumn*> m_LoadedLibFiles;


        public:

            LibFileDataDisplay(QWidget* parent = Q_NULLPTR, IView* pView = nullptr);

            void loadLibFile(const presenter::ProgramState& state, const model::data_structure::LibertyFileData* data);
            void remove(const index_t& index);

            void setLibFileVisibility(const index_t& index, bool visibility);
            void redrawLibFiles(const presenter::ProgramState& state);
        };
    }
}