#ifndef LIBVIEW_TEST
#include "CreateNewLvfButton.h"

namespace LibView
{
    namespace view
    {
        CreateNewLvfButton::CreateNewLvfButton(QWidget* parent) : 
            Qt_CreateNewLvfButton(parent),
            m_CreateNewLvfButton(findChild<QPushButton*>("CreateNewLvfButton", Qt::FindChildOption::FindChildrenRecursively))
        {}
    }
}
#endif