#pragma once

#ifndef LIBVIEW_TEST
#include "LibView/qt/Qt_PresetSelectionBox.h"
#include "LibView/view/PresetVisibilityToggle.h"

#include "ViewKnower.h"
#include "PresetButton.h"


namespace LibView
{
    namespace view
    {
        



        class PresetSelectionBox : public Qt_PresetSelectionBox, public ViewKnower
        {
        private:
            QVBoxLayout* m_MainLayout;
            std::vector<PresetVisibilityToggle*> m_Rows;

        public:
            PresetSelectionBox(IView* view, QWidget* parent = Q_NULLPTR);

            void addPresetRow(const std::string& presetName, const index_t& index);
            
            void initUI();
        
            void loadPresets();

        };
    }
}
#endif