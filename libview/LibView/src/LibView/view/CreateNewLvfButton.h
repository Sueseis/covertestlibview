#pragma once

#ifndef LIBVIEW_TEST
#include "LibView/qt/Qt_CreateNewLvfButton.h"

namespace LibView
{
    namespace view
    {
        class CreateNewLvfButton : private Qt_CreateNewLvfButton
        {
        private:
            QPushButton* m_CreateNewLvfButton;
        public:
            CreateNewLvfButton(QWidget* parent = Q_NULLPTR);
        };
    }
}

#endif