#pragma once

#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_SideMenu.h"
#include "AttributeSelectionBox.h"
#include "LibFileSelectionBox.h"
#include "PresetSelectionBox.h"
#include "LoadLibFileButton.h"

#include "ILibViewGuiElement.h"

#include <QSplitter>

namespace LibView
{
    namespace view
    {
        class SideMenu : public Qt_SideMenu , public ILibViewGuiElement
        {
        private:
            QGridLayout* m_MainLayout;
            AttributeSelectionBox* m_AttributeSelectionBox;
            LibFileSelectionBox* m_LibFileSelectionBox;
            PresetSelectionBox* m_PresetSelectionBox;
            LoadLibFileButton* m_LoadLibFileButton;

            IView* m_ViewPointer;

            QSplitter* m_Splitter;

            void initUI();

        public:
                SideMenu(QWidget* parent = Q_NULLPTR, IView* viewPointer = nullptr);

                IView* getViewPointer() const override;

                void loadLibFile(const model::data_structure::LibertyFileData* fileData);
                void loadAttributeList(const presenter::ProgramState& state);
                void remove(const index_t& index);
                void updateAttributeVisibilities(const presenter::ProgramState& state);
                void init();

        };
    }
}
#endif