#ifndef LIBVIEW_TEST

#pragma once

#include "IView.h"
#include "LibViewMainWindow.h"

namespace LibView {
	namespace view {
		
		class QtView : public IView {

		protected:

			LibViewMainWindow* m_MainWindow;
			void initUI();

		public:

			~QtView();
			QtView(presenter::PresenterFacade* presenterPointer);
			void notifyPresenterOfFileLoadRequest() override;
		
			presenter::PresenterFacade* getPresenterPointer() override;

			std::vector<std::string> promptLibFileSelection() override;


			void onContentDisplayFileSelection(const index_t& index) override;


			void receiveProgramState(const presenter::ProgramState& programState)  override;


			void setContentDisplayContent(const std::string& text) override;

			void libFileRemovalRequested(const index_t& index) override;

			void requestLibFileVisibility(const index_t& index, bool visibility) override;

			void setLibFileVisibility(const index_t& index, bool visibility) override;

			void requestAttributeVisibilityToggle(const index_t& index, bool visibility) override;

			void updateAttributeVisibilities(const presenter::ProgramState& state) override;

			const std::vector<model::APreset*>& getPresetList() const override;

			void requestPresetLoad(const index_t& index) override;

			void onSearchRequested(const std::string& reg) override;

		private:

			presenter::PresenterFacade* m_pPresenter = nullptr;
		
			//presenter::ProgramState m_LastState;
		

		};
	}
}

#endif // !LIBVIEW_TEST