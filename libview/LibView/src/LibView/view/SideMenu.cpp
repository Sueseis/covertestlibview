#include "SideMenu.h"

#ifndef LIBVIEW_TEST


namespace LibView
{
    namespace view
    {
        void SideMenu::initUI()
        {
            setLayout(m_MainLayout);

            m_MainLayout->addWidget(m_Splitter, 0, 0, 1, 1);

            m_Splitter->addWidget(m_AttributeSelectionBox);
            m_Splitter->addWidget(m_PresetSelectionBox);
            m_Splitter->addWidget(m_LibFileSelectionBox);

            m_Splitter->setOrientation(Qt::Orientation::Vertical);

            m_MainLayout->addWidget(m_LoadLibFileButton, 1, 0, 1, 1);

            

        }

        SideMenu::SideMenu(QWidget* parent, IView* viewPointer) :
            Qt_SideMenu(parent),
            m_Splitter(new QSplitter(this)),
            m_MainLayout(new QGridLayout(this)),
            m_AttributeSelectionBox(new AttributeSelectionBox(this, viewPointer)),
            m_LibFileSelectionBox(new LibFileSelectionBox(this, viewPointer)),
            m_LoadLibFileButton(new LoadLibFileButton(this, viewPointer)),
            m_PresetSelectionBox(new PresetSelectionBox(viewPointer, this)),
            m_ViewPointer(viewPointer)
        {
            m_AttributeSelectionBox->setParent(m_Splitter);
            m_LibFileSelectionBox->setParent(m_Splitter);
            m_PresetSelectionBox->setParent(m_Splitter);

            initUI();
        }
        IView* SideMenu::getViewPointer() const
        {
            return m_ViewPointer;
            
        }
        void SideMenu::loadLibFile(const model::data_structure::LibertyFileData* fileData)
        {
            m_LibFileSelectionBox->addLibFileRow(fileData->getDisplayName(), fileData->getLibFileIndex());
        }
        void SideMenu::loadAttributeList(const presenter::ProgramState& state)
        {
            m_AttributeSelectionBox->setAttributeList(state);
        }
        void SideMenu::remove(const index_t& index)
        {
            m_LibFileSelectionBox->remove(index);
        }
        void SideMenu::updateAttributeVisibilities(const presenter::ProgramState& state)
        {
            m_AttributeSelectionBox->updateAttributeVisibilities(state);
        }
        void SideMenu::init()
        {
            m_PresetSelectionBox->loadPresets();
        }
    }
}
#endif