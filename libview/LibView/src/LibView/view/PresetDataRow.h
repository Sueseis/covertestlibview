#pragma once

#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_PresetDataRow.h"

namespace LibView
{
    namespace view
    {

        class PresetDataRow : private Qt_PresetDataRow
        {
            friend class PresetDataBox;

        private:

            QLabel* m_TitleLabel;
            QLabel* m_ValueLabel;

        public:

            PresetDataRow(QWidget* parent = Q_NULLPTR);
            PresetDataRow(const std::string& title, const std::string& value, QWidget* parent = Q_NULLPTR);
            ~PresetDataRow();


            inline void setTitle(const std::string& title) { m_TitleLabel->setText(QString(title.c_str())); }
            inline void setValue(const std::string& prefix) { m_ValueLabel->setText(QString(prefix.c_str())); }
        };
    }
}
#endif