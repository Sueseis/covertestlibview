#pragma once



#include <QLabel>
#include <QWidget>
#include <QGridLayout>

#include "LibView/core/LibFileIndex.h"

namespace LibView
{
    namespace view
    {
        class GroupDisplayContainer : public QWidget
        {
        private:

            int m_AttributeRows;

            QWidget* m_AttributeTable;
            QGridLayout* m_AttributeTableLayout;

            QWidget* m_SubGroupContainer;
            QVBoxLayout* m_SubGroupContainerLayout;

            bool m_IsRoot;
            bool m_IsDataVisible;

            index_t m_Index;

            void setSubGroupContainerVisible();

            std::vector< GroupDisplayContainer*> m_SubGroups;

        public:

            GroupDisplayContainer(bool isRoot, index_t& index, std::string name, std::string value, QWidget* parent = Q_NULLPTR);
            ~GroupDisplayContainer();

            void insertAttribute(const std::string& name, const std::string& value);
            void insertSubGroup(GroupDisplayContainer* groupDisplay);

            void setDataVisible(bool visibility);
            bool updateOverallVisibility();

            inline index_t& getIndex() { return m_Index; };

            

        };
    }

}
