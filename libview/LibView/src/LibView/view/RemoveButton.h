#pragma once


#ifndef LIBVIEW_TEST

#include "LibView/core/LibFileIndex.h"
#include "ILibViewGuiElement.h"

#include "LibView/qt/Qt_RemoveButton.h"
namespace LibView
{
    namespace view
    {
        class RemoveButton : public Qt_RemoveButton, public ILibViewGuiElement
        {
            Q_OBJECT

        private:
            index_t m_Index;
            const char* m_Text;

            QPushButton* m_RemoveButton;
            IView* m_ViewPointer;


        private slots:

            void onButtonClick();


        
        
        public:
            RemoveButton(const index_t& index, QWidget* parent = Q_NULLPTR, const char* text = "x", IView* pView = nullptr);
            IView* getViewPointer() const override;

        };

    }
}

#endif