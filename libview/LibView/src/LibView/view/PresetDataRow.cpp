#ifndef LIBVIEW_TEST


#include "PresetDataRow.h"

LibView::view::PresetDataRow::PresetDataRow(QWidget* parent) :
    Qt_PresetDataRow(),
    m_TitleLabel(findChild<QLabel*>("PresetDataRowTitle", Qt::FindChildOption::FindChildrenRecursively)),
    m_ValueLabel(findChild<QLabel*>("PresetDataRowValue", Qt::FindChildOption::FindChildrenRecursively))
{}

LibView::view::PresetDataRow::PresetDataRow(const std::string & title, const std::string & value, QWidget* parent)
    : PresetDataRow(parent)
{
    setTitle(title);
    setValue(value);
}

LibView::view::PresetDataRow::~PresetDataRow()
{}


#endif