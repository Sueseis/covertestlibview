#ifndef LIBVIEW_TEST

#include "PresetVisibilityToggle.h"

namespace LibView
{
    namespace view
    {
        PresetVisibilityToggle::PresetVisibilityToggle(const std::string& name, QWidget* parent) :
            Qt_PresetVisibilityToggle(parent),
            m_Name(name),
            m_PresetVisibilityToggle(findChild<QRadioButton*>("PresetVisibilityToggleRadioButton",
                Qt::FindChildOption::FindChildrenRecursively))
        {
        
        }
        void PresetVisibilityToggle::initUI()
        {
            m_PresetVisibilityToggle->setText(m_Name.c_str());
        }
                
    }
}
#endif