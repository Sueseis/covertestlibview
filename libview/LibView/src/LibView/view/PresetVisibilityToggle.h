#pragma once


#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_PresetVisibilityToggle.h"

namespace LibView
{
    namespace view
    {
        class PresetVisibilityToggle : public Qt_PresetVisibilityToggle
        {
        private: QRadioButton* m_PresetVisibilityToggle;
               std::string m_Name;

               void initUI();

        public:
            PresetVisibilityToggle(const std::string& name, QWidget* parent = Q_NULLPTR);
        };
    }
}

#endif