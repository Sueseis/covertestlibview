#ifndef LIBVIEW_TEST

#include "PinButton.h"

namespace LibView {
    
    namespace view{
        PinButton::PinButton(QWidget* parent) :
            Qt_PinButton(parent),
            m_PinButtonPushButton (findChild<QPushButton*>("PinButtonPushButton", Qt::FindChildOption::FindChildrenRecursively))
        {}
    }
}

#endif // !LIBVIEW_TEST
