#pragma once

#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_PresetDisplayOverview.h"
#include <QGridLayout>
#include "PresetTitle.h"
#include "PresetDataBox.h"

namespace LibView
{
    namespace view
    {
        class PresetDisplayOverview :public Qt_PresetDisplayOverview 
        {
        private:
            QGridLayout* m_MainLayout;
            PresetDataBox* m_PresetDataBox;
            PresetTitle* m_PresetTitle;
            
            
            void initUI();


        public:
            PresetDisplayOverview(QWidget* parent = Q_NULLPTR);
            
            inline void addDataRow(const std::string& title, const std::string& value)
            {
                m_PresetDataBox->addDataRow(title, value);
            }

            inline void setTitleName(const std::string& title)
            {
                m_PresetTitle->setTitleName(title);
            }
        };
    }
}

#endif