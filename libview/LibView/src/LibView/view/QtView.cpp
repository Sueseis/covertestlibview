#ifndef LIBVIEW_TEST



#include "QtView.h"
#include "DeltaPercentToggle.h"
#include "PresetDataBox.h"
#include "PresetDisplayOverview.h"

#include "LibView/core/DebugLog.h"

#include "LibView/presenter/PresenterFacade.h"

#include <QFileDialog>



namespace LibView
{

    namespace view
    {
        QtView::~QtView()
        {
            delete m_pPresenter;
            delete m_MainWindow;
        }

        QtView::QtView(presenter::PresenterFacade* presenterPointer) :
            m_MainWindow(new LibViewMainWindow(this)),
            m_pPresenter(presenterPointer)
        {

            m_pPresenter->init(this);

            initUI();

        }

        void QtView::notifyPresenterOfFileLoadRequest()
        {
            LIBVIEW_LOG("lib file load request");
            getPresenterPointer()->requestFileLoad();
        }

        presenter::PresenterFacade* QtView::getPresenterPointer()
        {
            return m_pPresenter;
        }

        std::vector<std::string> QtView::promptLibFileSelection()
        {
            std::vector<std::string> result;

            QFileDialog dialog;
            dialog.setFileMode(QFileDialog::ExistingFiles);
            dialog.setNameFilter("LibFiles (*.lib *.txt *lvf)");

            QStringList fileNames;

            if(dialog.exec())
                fileNames = dialog.selectedFiles();

            for(const QString& name : fileNames)
            {
                result.push_back(name.toStdString());
            }

            return result;
        }

        void QtView::onContentDisplayFileSelection(const index_t& index)
        {

            m_pPresenter->requestContentDisplay(index);
        }

        void QtView::receiveProgramState(const presenter::ProgramState& programState)
        {
            // TODO : Check for last added and removed flag in programstate
            
            std::vector<index_t> lastAdded = programState.getLastAddedLibFiles();

            for(model::data_structure::LibertyFileData* fileData : programState.getLibFileList())
            {
                for(index_t index : lastAdded)
                {
                    if(fileData->getLibFileIndex() == index)
                    {
                        m_MainWindow->loadLibFile(programState, fileData);
                    }
                }
            }

            std::vector<index_t> lastRemoved = programState.getLastRemovedLibFiles();

            for(index_t index : lastRemoved)
            {
                m_MainWindow->remove(index);
            }

            m_MainWindow->loadAttributeList(programState);
        }

        void QtView::setContentDisplayContent(const std::string& text)
        {
            m_MainWindow->setContentDisplayContent(text);
        }

        void QtView::libFileRemovalRequested(const index_t& index)
        {
            m_pPresenter->requestRemovalofLibFile(index);
        }

        void QtView::requestLibFileVisibility(const index_t& index, bool visibility)
        {
            m_pPresenter->requestLibertyFileVisibilityToggle(index, visibility);
        }

        void QtView::setLibFileVisibility(const index_t& index, bool visibility)
        {
            m_MainWindow->setLibFileVisibility(index, visibility);
        }

        void QtView::requestAttributeVisibilityToggle(const index_t& index, bool visibility)
        {
            m_pPresenter->requestAttributeVisibilityToggle(index, visibility);
        }

        void QtView::updateAttributeVisibilities(const presenter::ProgramState& state)
        {
            m_MainWindow->updateAttributeVisibilities(state);
            m_MainWindow->redrawLibFiles(state);
        }

        const std::vector<model::APreset*>& QtView::getPresetList() const
        {
            return m_pPresenter->getPresetList();
        }

        void QtView::requestPresetLoad(const index_t& index)
        {
            m_pPresenter->requestPresetSelection(index);
        }

        void QtView::onSearchRequested(const std::string& reg)
        {
            m_pPresenter->requestSearch(reg);
        }

        void QtView::initUI()
        {
            //BarChart *barChart = new BarChart();
            //barChart->show();
            m_MainWindow->init();
            m_MainWindow->show();
        }
    }
}
#endif // !LIBVIEW_TEST