#ifndef LIBVIEW_TEST


#include "RemoveButton.h"
namespace LibView
{
    namespace view
    {
        RemoveButton::RemoveButton(const index_t& index, QWidget* parent, const char* text, IView* pView) :
            Qt_RemoveButton(parent),
            m_Index(index),
            m_Text(text),
            m_ViewPointer(pView),
            m_RemoveButton(findChild < QPushButton*>("RemoveLibFilePushButton", Qt::FindChildOption::FindChildrenRecursively))
        {
            m_RemoveButton->setText(m_Text);
            
            //m_RemoveButton->setMaximumWidth(15);
            //m_RemoveButton->setMaximumHeight(15);
            m_RemoveButton->setMaximumWidth(m_RemoveButton->height() - 2);
            m_RemoveButton->setMaximumHeight(m_RemoveButton->width());

            layout()->setMargin(0);

            connect(m_RemoveButton, SIGNAL(clicked()), this, SLOT(onButtonClick()));

        }

        void RemoveButton::onButtonClick()
        {
            IView* view = getViewPointer();
            view->libFileRemovalRequested(m_Index);
        }

        IView* RemoveButton::getViewPointer() const
        {
            return m_ViewPointer;
        }
    }
}

#endif