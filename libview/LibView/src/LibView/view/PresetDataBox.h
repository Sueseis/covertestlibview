#pragma once

#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_PresetDataBox.h"
#include"LibView/view/PresetDataRow.h"
#include"LibView/core/LibFileIndex.h"
#include <string>
#include <vector>



namespace LibView
{
    namespace view
    {
        class PresetDataBox : public Qt_PresetDataBox
        {
        private:
            QGridLayout* m_MainLayout;
            std::vector<PresetDataRow*> m_Rows;
            int m_NumberOfRows;
        
        public:
            PresetDataBox(QWidget* parent = Q_NULLPTR);

            void addDataRow(const std::string& title, const std::string& value);

        };
    }

}



#endif