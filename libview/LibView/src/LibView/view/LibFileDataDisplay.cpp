#include "LibFileDataDisplay.h"


namespace LibView
{
    namespace view
    {
        IView* LibFileDataDisplay::getViewPointer() const
        {
            return m_View;
        }
        LibFileDataDisplay::LibFileDataDisplay(QWidget* parent, IView* pView)
            :
            m_ScrollArea(findChild<QScrollArea*>("ScrollArea", Qt::FindChildOption::FindChildrenRecursively)),
            m_View(pView),
            //m_Layout(new QGridLayout(this)),
            m_LoadedLibFiles(std::vector<LibFileDataColumn*>()),
            Qt_LibFileDataDisplay(parent)
        {
            QWidget* contentWidget = new QWidget(m_ScrollArea);
            QHBoxLayout* scrollLayout = new QHBoxLayout(contentWidget);
            scrollLayout->setAlignment(Qt::AlignTop);
            //scrollLayout->setContentsMargins(5, 0, 0, 0);
            //scrollLayout->setSpacing(0);
            contentWidget->setLayout(scrollLayout);
            m_ScrollArea->setWidget(contentWidget);

            //setLayout(m_Layout);

            //m_Layout->addWidget(m_ScrollArea, 0, 0 , 1, 1);


            //QHBoxLayout* scrollLayout = new QHBoxLayout(m_ScrollArea->widget());
            //scrollLayout->setAlignment(Qt::AlignLeft);
            //m_ScrollArea->widget()->setLayout(scrollLayout);
        }
        void LibFileDataDisplay::loadLibFile(const presenter::ProgramState& state, const LibView::model::data_structure::LibertyFileData* data)
        {
            LibFileDataColumn* column = new LibFileDataColumn(state, data);
            m_LoadedLibFiles.push_back(column);
            m_ScrollArea->widget()->layout()->addWidget(column);
        }
        void LibFileDataDisplay::remove(const index_t& index)
        {
            size_t counter = 0;

            for (LibFileDataColumn* column : m_LoadedLibFiles) 
            {
                if(column->getIndex() == index)
                {
                    column->deleteLater();
                    break;
                }

                ++counter;
            }

            m_LoadedLibFiles.erase(m_LoadedLibFiles.begin() + counter);
        }
        void LibFileDataDisplay::setLibFileVisibility(const index_t& index, bool visibility)
        {
            //size_t counter = 0;

            for(LibFileDataColumn* column : m_LoadedLibFiles)
            {
                if(column->getIndex() == index)
                {
                    column->setVisible(visibility);
                    break;
                }
            }
        }
        void LibFileDataDisplay::redrawLibFiles(const presenter::ProgramState& state)
        {
            for(LibFileDataColumn* column : m_LoadedLibFiles)
            {
                column->updateVisibilities(state);
            }
        }
    }
}