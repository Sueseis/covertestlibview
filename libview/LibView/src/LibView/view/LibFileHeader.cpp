#include "LibFileHeader.h"


#ifndef LIBVIEW_TEST

namespace LibView
{
    namespace view
    {
        LibFileHeader::LibFileHeader(QWidget* parent) :
            Qt_LibFileHeader(parent),
            //m_TitleLabel(findChild<QLabel*>("LibFileHeaderTitleLabel", Qt::FindChildOption::FindChildrenRecursively)),
            m_NameLabel(findChild<QLabel*>("LibFileHeaderNameLabel", Qt::FindChildOption::FindChildrenRecursively)),
            //m_PinFilePlaceholder(findChild<QWidget*>("PinLibFilePlaceholder", Qt::FindChildOption::FindChildrenRecursively)),
            //m_PinButton(new PinButton()),
            m_Title("LibFile"),
            m_LibFileIndex(0)
        {
            //QGridLayout* layout = new QGridLayout(m_PinFilePlaceholder);
            //layout->addWidget(m_PinButton, 0, 0, 1, 1);
            //m_PinFilePlaceholder->setLayout(layout);
        }
        
    }
}

#endif // !LIBVIEW_TEST
