#pragma once

#include <QWidget>
#include "uic/ui_Qt_SelectGraphTypeComboBox.h"

class Qt_SelectGraphTypeComboBox : public QWidget 
{
	Q_OBJECT

public:
  Qt_SelectGraphTypeComboBox(QWidget *parent = Q_NULLPTR);
  ~Qt_SelectGraphTypeComboBox(); 

private:
  Ui::Qt_SelectGraphTypeComboBox ui;
};
