#pragma once

#include <QWidget>
#include "uic/ui_Qt_PresetDisplayOverview.h"

class Qt_PresetDisplayOverview : public QWidget 
{
	Q_OBJECT

public:
  Qt_PresetDisplayOverview(QWidget *parent = Q_NULLPTR);
  ~Qt_PresetDisplayOverview(); 

private:
  Ui::Qt_PresetDisplayOverview ui;
};
