#pragma once

#include <QWidget>
#include "uic/ui_Qt_ExportCsvButton.h"

class Qt_ExportCsvButton : public QWidget 
{
	Q_OBJECT

public:
  Qt_ExportCsvButton(QWidget *parent = Q_NULLPTR);
  ~Qt_ExportCsvButton(); 

private:
  Ui::Qt_ExportCsvButton ui;
};
