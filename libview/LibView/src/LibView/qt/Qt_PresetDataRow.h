#pragma once

#include <QWidget>
#include "uic/ui_Qt_PresetDataRow.h"

class Qt_PresetDataRow : public QWidget 
{
	Q_OBJECT

public:
  Qt_PresetDataRow(QWidget *parent = Q_NULLPTR);
  ~Qt_PresetDataRow(); 

private:
  Ui::Qt_PresetDataRow ui;
};
