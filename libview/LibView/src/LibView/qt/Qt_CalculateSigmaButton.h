#pragma once

#include <QWidget>
#include "uic/ui_Qt_CalculateSigmaButton.h"

class Qt_CalculateSigmaButton : public QWidget 
{
	Q_OBJECT

public:
  Qt_CalculateSigmaButton(QWidget *parent = Q_NULLPTR);
  ~Qt_CalculateSigmaButton(); 

private:
  Ui::Qt_CalculateSigmaButton ui;
};
