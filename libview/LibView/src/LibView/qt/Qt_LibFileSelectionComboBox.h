#pragma once

#include <QWidget>
#include "uic/ui_Qt_LibFileSelectionComboBox.h"

class Qt_LibFileSelectionComboBox : public QWidget 
{
	Q_OBJECT

public:
  Qt_LibFileSelectionComboBox(QWidget *parent = Q_NULLPTR);
  ~Qt_LibFileSelectionComboBox(); 

private:
  Ui::Qt_LibFileSelectionComboBox ui;
};
