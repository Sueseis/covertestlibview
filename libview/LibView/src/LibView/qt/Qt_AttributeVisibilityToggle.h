#pragma once

#include <QWidget>
#include "uic/ui_Qt_AttributeVisibilityToggle.h"

class Qt_AttributeVisibilityToggle : public QWidget 
{
	Q_OBJECT

public:
  Qt_AttributeVisibilityToggle(QWidget *parent = Q_NULLPTR);
  ~Qt_AttributeVisibilityToggle(); 

private:
  Ui::Qt_AttributeVisibilityToggle ui;
};
