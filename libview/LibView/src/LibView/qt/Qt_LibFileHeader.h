#pragma once

#include <QWidget>
#include "uic/ui_Qt_LibFileHeader.h"

class Qt_LibFileHeader : public QWidget 
{
	Q_OBJECT

public:
  Qt_LibFileHeader(QWidget *parent = Q_NULLPTR);
  ~Qt_LibFileHeader(); 

private:
  Ui::Qt_LibFileHeader ui;
};
