/********************************************************************************
** Form generated from reading UI file 'Qt_PresetDataRow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_PRESETDATAROW_H
#define UI_QT_PRESETDATAROW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_PresetDataRow
{
public:
    QGridLayout *gridLayout;
    QLabel *PresetDataRowTitle;
    QLabel *PresetDataRowValue;

    void setupUi(QWidget *Qt_PresetDataRow)
    {
        if (Qt_PresetDataRow->objectName().isEmpty())
            Qt_PresetDataRow->setObjectName(QString::fromUtf8("Qt_PresetDataRow"));
        Qt_PresetDataRow->resize(400, 300);
        gridLayout = new QGridLayout(Qt_PresetDataRow);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        PresetDataRowTitle = new QLabel(Qt_PresetDataRow);
        PresetDataRowTitle->setObjectName(QString::fromUtf8("PresetDataRowTitle"));

        gridLayout->addWidget(PresetDataRowTitle, 0, 0, 1, 1);

        PresetDataRowValue = new QLabel(Qt_PresetDataRow);
        PresetDataRowValue->setObjectName(QString::fromUtf8("PresetDataRowValue"));

        gridLayout->addWidget(PresetDataRowValue, 0, 1, 1, 1);


        retranslateUi(Qt_PresetDataRow);

        QMetaObject::connectSlotsByName(Qt_PresetDataRow);
    } // setupUi

    void retranslateUi(QWidget *Qt_PresetDataRow)
    {
        Qt_PresetDataRow->setWindowTitle(QCoreApplication::translate("Qt_PresetDataRow", "Form", nullptr));
        PresetDataRowTitle->setText(QCoreApplication::translate("Qt_PresetDataRow", "Title:", nullptr));
        PresetDataRowValue->setText(QCoreApplication::translate("Qt_PresetDataRow", "Undefined", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_PresetDataRow: public Ui_Qt_PresetDataRow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_PRESETDATAROW_H
