/********************************************************************************
** Form generated from reading UI file 'Qt_MergeAndCreateNewFileButton.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_MERGEANDCREATENEWFILEBUTTON_H
#define UI_QT_MERGEANDCREATENEWFILEBUTTON_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_MergeAndCreateNewFileButton
{
public:
    QGridLayout *gridLayout;
    QPushButton *MergeAndCreateNewFileButtonPushButton;

    void setupUi(QWidget *Qt_MergeAndCreateNewFileButton)
    {
        if (Qt_MergeAndCreateNewFileButton->objectName().isEmpty())
            Qt_MergeAndCreateNewFileButton->setObjectName(QString::fromUtf8("Qt_MergeAndCreateNewFileButton"));
        Qt_MergeAndCreateNewFileButton->resize(400, 300);
        gridLayout = new QGridLayout(Qt_MergeAndCreateNewFileButton);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        MergeAndCreateNewFileButtonPushButton = new QPushButton(Qt_MergeAndCreateNewFileButton);
        MergeAndCreateNewFileButtonPushButton->setObjectName(QString::fromUtf8("MergeAndCreateNewFileButtonPushButton"));

        gridLayout->addWidget(MergeAndCreateNewFileButtonPushButton, 0, 0, 1, 1);


        retranslateUi(Qt_MergeAndCreateNewFileButton);

        QMetaObject::connectSlotsByName(Qt_MergeAndCreateNewFileButton);
    } // setupUi

    void retranslateUi(QWidget *Qt_MergeAndCreateNewFileButton)
    {
        Qt_MergeAndCreateNewFileButton->setWindowTitle(QCoreApplication::translate("Qt_MergeAndCreateNewFileButton", "Form", nullptr));
        MergeAndCreateNewFileButtonPushButton->setText(QCoreApplication::translate("Qt_MergeAndCreateNewFileButton", "PushButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_MergeAndCreateNewFileButton: public Ui_Qt_MergeAndCreateNewFileButton {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_MERGEANDCREATENEWFILEBUTTON_H
