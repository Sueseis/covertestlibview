/********************************************************************************
** Form generated from reading UI file 'Qt_GroupDisplay.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_GROUPDISPLAY_H
#define UI_QT_GROUPDISPLAY_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_GroupDisplay
{
public:

    void setupUi(QWidget *Qt_GroupDisplay)
    {
        if (Qt_GroupDisplay->objectName().isEmpty())
            Qt_GroupDisplay->setObjectName(QString::fromUtf8("Qt_GroupDisplay"));
        Qt_GroupDisplay->resize(400, 300);

        retranslateUi(Qt_GroupDisplay);

        QMetaObject::connectSlotsByName(Qt_GroupDisplay);
    } // setupUi

    void retranslateUi(QWidget *Qt_GroupDisplay)
    {
        Qt_GroupDisplay->setWindowTitle(QCoreApplication::translate("Qt_GroupDisplay", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_GroupDisplay: public Ui_Qt_GroupDisplay {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_GROUPDISPLAY_H
