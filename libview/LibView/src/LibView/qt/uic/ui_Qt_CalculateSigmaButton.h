/********************************************************************************
** Form generated from reading UI file 'Qt_CalculateSigmaButton.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_CALCULATESIGMABUTTON_H
#define UI_QT_CALCULATESIGMABUTTON_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_CalculateSigmaButton
{
public:
    QGridLayout *gridLayout;
    QPushButton *CalculateSigmaButtonPushButton;

    void setupUi(QWidget *Qt_CalculateSigmaButton)
    {
        if (Qt_CalculateSigmaButton->objectName().isEmpty())
            Qt_CalculateSigmaButton->setObjectName(QString::fromUtf8("Qt_CalculateSigmaButton"));
        Qt_CalculateSigmaButton->resize(400, 300);
        gridLayout = new QGridLayout(Qt_CalculateSigmaButton);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        CalculateSigmaButtonPushButton = new QPushButton(Qt_CalculateSigmaButton);
        CalculateSigmaButtonPushButton->setObjectName(QString::fromUtf8("CalculateSigmaButtonPushButton"));

        gridLayout->addWidget(CalculateSigmaButtonPushButton, 0, 0, 1, 1);


        retranslateUi(Qt_CalculateSigmaButton);

        QMetaObject::connectSlotsByName(Qt_CalculateSigmaButton);
    } // setupUi

    void retranslateUi(QWidget *Qt_CalculateSigmaButton)
    {
        Qt_CalculateSigmaButton->setWindowTitle(QCoreApplication::translate("Qt_CalculateSigmaButton", "Form", nullptr));
        CalculateSigmaButtonPushButton->setText(QCoreApplication::translate("Qt_CalculateSigmaButton", "calculate Sigma", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_CalculateSigmaButton: public Ui_Qt_CalculateSigmaButton {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_CALCULATESIGMABUTTON_H
