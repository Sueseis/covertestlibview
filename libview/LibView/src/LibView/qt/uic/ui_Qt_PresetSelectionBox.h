/********************************************************************************
** Form generated from reading UI file 'Qt_PresetSelectionBox.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_PRESETSELECTIONBOX_H
#define UI_QT_PRESETSELECTIONBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_PresetSelectionBox
{
public:

    void setupUi(QWidget *Qt_PresetSelectionBox)
    {
        if (Qt_PresetSelectionBox->objectName().isEmpty())
            Qt_PresetSelectionBox->setObjectName(QString::fromUtf8("Qt_PresetSelectionBox"));
        Qt_PresetSelectionBox->resize(400, 300);

        retranslateUi(Qt_PresetSelectionBox);

        QMetaObject::connectSlotsByName(Qt_PresetSelectionBox);
    } // setupUi

    void retranslateUi(QWidget *Qt_PresetSelectionBox)
    {
        Qt_PresetSelectionBox->setWindowTitle(QCoreApplication::translate("Qt_PresetSelectionBox", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_PresetSelectionBox: public Ui_Qt_PresetSelectionBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_PRESETSELECTIONBOX_H
