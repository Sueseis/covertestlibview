/********************************************************************************
** Form generated from reading UI file 'Qt_AttributeVisibilityToggle.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_ATTRIBUTEVISIBILITYTOGGLE_H
#define UI_QT_ATTRIBUTEVISIBILITYTOGGLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_AttributeVisibilityToggle
{
public:
    QGridLayout *gridLayout;
    QCheckBox *AttributeVisibilityToggleCheckBox;

    void setupUi(QWidget *Qt_AttributeVisibilityToggle)
    {
        if (Qt_AttributeVisibilityToggle->objectName().isEmpty())
            Qt_AttributeVisibilityToggle->setObjectName(QString::fromUtf8("Qt_AttributeVisibilityToggle"));
        Qt_AttributeVisibilityToggle->resize(400, 300);
        gridLayout = new QGridLayout(Qt_AttributeVisibilityToggle);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        AttributeVisibilityToggleCheckBox = new QCheckBox(Qt_AttributeVisibilityToggle);
        AttributeVisibilityToggleCheckBox->setObjectName(QString::fromUtf8("AttributeVisibilityToggleCheckBox"));

        gridLayout->addWidget(AttributeVisibilityToggleCheckBox, 0, 0, 1, 1);


        retranslateUi(Qt_AttributeVisibilityToggle);

        QMetaObject::connectSlotsByName(Qt_AttributeVisibilityToggle);
    } // setupUi

    void retranslateUi(QWidget *Qt_AttributeVisibilityToggle)
    {
        Qt_AttributeVisibilityToggle->setWindowTitle(QCoreApplication::translate("Qt_AttributeVisibilityToggle", "Form", nullptr));
        AttributeVisibilityToggleCheckBox->setText(QCoreApplication::translate("Qt_AttributeVisibilityToggle", "CheckBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_AttributeVisibilityToggle: public Ui_Qt_AttributeVisibilityToggle {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_ATTRIBUTEVISIBILITYTOGGLE_H
