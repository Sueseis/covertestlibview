/********************************************************************************
** Form generated from reading UI file 'Qt_MainTabBar.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_MAINTABBAR_H
#define UI_QT_MAINTABBAR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_MainTabBar
{
public:
    QGridLayout *gridLayout;
    QTabWidget *MainTabBar;
    QWidget *TabOverview;
    QWidget *TabContentDisplay;

    void setupUi(QWidget *Qt_MainTabBar)
    {
        if (Qt_MainTabBar->objectName().isEmpty())
            Qt_MainTabBar->setObjectName(QString::fromUtf8("Qt_MainTabBar"));
        Qt_MainTabBar->resize(836, 555);
        gridLayout = new QGridLayout(Qt_MainTabBar);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        MainTabBar = new QTabWidget(Qt_MainTabBar);
        MainTabBar->setObjectName(QString::fromUtf8("MainTabBar"));
        TabOverview = new QWidget();
        TabOverview->setObjectName(QString::fromUtf8("TabOverview"));
        MainTabBar->addTab(TabOverview, QString());
        TabContentDisplay = new QWidget();
        TabContentDisplay->setObjectName(QString::fromUtf8("TabContentDisplay"));
        MainTabBar->addTab(TabContentDisplay, QString());

        gridLayout->addWidget(MainTabBar, 0, 0, 1, 1);


        retranslateUi(Qt_MainTabBar);

        MainTabBar->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Qt_MainTabBar);
    } // setupUi

    void retranslateUi(QWidget *Qt_MainTabBar)
    {
        Qt_MainTabBar->setWindowTitle(QCoreApplication::translate("Qt_MainTabBar", "Form", nullptr));
        MainTabBar->setTabText(MainTabBar->indexOf(TabOverview), QCoreApplication::translate("Qt_MainTabBar", "Overview", nullptr));
        MainTabBar->setTabText(MainTabBar->indexOf(TabContentDisplay), QCoreApplication::translate("Qt_MainTabBar", "Content Display", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_MainTabBar: public Ui_Qt_MainTabBar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_MAINTABBAR_H
