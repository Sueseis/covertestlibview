/********************************************************************************
** Form generated from reading UI file 'Qt_LibFileDataColumn.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_LIBFILEDATACOLUMN_H
#define UI_QT_LIBFILEDATACOLUMN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_LibFileDataColumn
{
public:

    void setupUi(QWidget *Qt_LibFileDataColumn)
    {
        if (Qt_LibFileDataColumn->objectName().isEmpty())
            Qt_LibFileDataColumn->setObjectName(QString::fromUtf8("Qt_LibFileDataColumn"));
        Qt_LibFileDataColumn->resize(400, 300);

        retranslateUi(Qt_LibFileDataColumn);

        QMetaObject::connectSlotsByName(Qt_LibFileDataColumn);
    } // setupUi

    void retranslateUi(QWidget *Qt_LibFileDataColumn)
    {
        Qt_LibFileDataColumn->setWindowTitle(QCoreApplication::translate("Qt_LibFileDataColumn", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_LibFileDataColumn: public Ui_Qt_LibFileDataColumn {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_LIBFILEDATACOLUMN_H
