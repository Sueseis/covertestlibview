/********************************************************************************
** Form generated from reading UI file 'Qt_AttributeDisplayRow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_ATTRIBUTEDISPLAYROW_H
#define UI_QT_ATTRIBUTEDISPLAYROW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_AttributeDisplayRow
{
public:
    QGridLayout *gridLayout;
    QLabel *AttributeValue;
    QLabel *attributeName;

    void setupUi(QWidget *Qt_AttributeDisplayRow)
    {
        if (Qt_AttributeDisplayRow->objectName().isEmpty())
            Qt_AttributeDisplayRow->setObjectName(QString::fromUtf8("Qt_AttributeDisplayRow"));
        Qt_AttributeDisplayRow->resize(400, 300);
        gridLayout = new QGridLayout(Qt_AttributeDisplayRow);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        AttributeValue = new QLabel(Qt_AttributeDisplayRow);
        AttributeValue->setObjectName(QString::fromUtf8("AttributeValue"));

        gridLayout->addWidget(AttributeValue, 1, 1, 1, 1);

        attributeName = new QLabel(Qt_AttributeDisplayRow);
        attributeName->setObjectName(QString::fromUtf8("attributeName"));

        gridLayout->addWidget(attributeName, 1, 0, 1, 1);


        retranslateUi(Qt_AttributeDisplayRow);

        QMetaObject::connectSlotsByName(Qt_AttributeDisplayRow);
    } // setupUi

    void retranslateUi(QWidget *Qt_AttributeDisplayRow)
    {
        Qt_AttributeDisplayRow->setWindowTitle(QCoreApplication::translate("Qt_AttributeDisplayRow", "Form", nullptr));
        AttributeValue->setText(QCoreApplication::translate("Qt_AttributeDisplayRow", "TextLabel", nullptr));
        attributeName->setText(QCoreApplication::translate("Qt_AttributeDisplayRow", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_AttributeDisplayRow: public Ui_Qt_AttributeDisplayRow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_ATTRIBUTEDISPLAYROW_H
