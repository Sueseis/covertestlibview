/********************************************************************************
** Form generated from reading UI file 'Qt_PinButton.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_PINBUTTON_H
#define UI_QT_PINBUTTON_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_PinButton
{
public:
    QGridLayout *gridLayout;
    QPushButton *PinButtonPushButton;

    void setupUi(QWidget *Qt_PinButton)
    {
        if (Qt_PinButton->objectName().isEmpty())
            Qt_PinButton->setObjectName(QString::fromUtf8("Qt_PinButton"));
        Qt_PinButton->resize(400, 300);
        gridLayout = new QGridLayout(Qt_PinButton);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        PinButtonPushButton = new QPushButton(Qt_PinButton);
        PinButtonPushButton->setObjectName(QString::fromUtf8("PinButtonPushButton"));
        PinButtonPushButton->setFocusPolicy(Qt::WheelFocus);

        gridLayout->addWidget(PinButtonPushButton, 0, 0, 1, 1);


        retranslateUi(Qt_PinButton);

        QMetaObject::connectSlotsByName(Qt_PinButton);
    } // setupUi

    void retranslateUi(QWidget *Qt_PinButton)
    {
        Qt_PinButton->setWindowTitle(QCoreApplication::translate("Qt_PinButton", "Form", nullptr));
        PinButtonPushButton->setText(QCoreApplication::translate("Qt_PinButton", "pin", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_PinButton: public Ui_Qt_PinButton {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_PINBUTTON_H
