/********************************************************************************
** Form generated from reading UI file 'Qt_ExportCsvButton.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_EXPORTCSVBUTTON_H
#define UI_QT_EXPORTCSVBUTTON_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_ExportCsvButton
{
public:
    QGridLayout *gridLayout;
    QPushButton *ExportCsvButtonPushButton;

    void setupUi(QWidget *Qt_ExportCsvButton)
    {
        if (Qt_ExportCsvButton->objectName().isEmpty())
            Qt_ExportCsvButton->setObjectName(QString::fromUtf8("Qt_ExportCsvButton"));
        Qt_ExportCsvButton->resize(400, 300);
        gridLayout = new QGridLayout(Qt_ExportCsvButton);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        ExportCsvButtonPushButton = new QPushButton(Qt_ExportCsvButton);
        ExportCsvButtonPushButton->setObjectName(QString::fromUtf8("ExportCsvButtonPushButton"));

        gridLayout->addWidget(ExportCsvButtonPushButton, 0, 0, 1, 1);


        retranslateUi(Qt_ExportCsvButton);

        QMetaObject::connectSlotsByName(Qt_ExportCsvButton);
    } // setupUi

    void retranslateUi(QWidget *Qt_ExportCsvButton)
    {
        Qt_ExportCsvButton->setWindowTitle(QCoreApplication::translate("Qt_ExportCsvButton", "Form", nullptr));
        ExportCsvButtonPushButton->setText(QCoreApplication::translate("Qt_ExportCsvButton", "export CSV", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_ExportCsvButton: public Ui_Qt_ExportCsvButton {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_EXPORTCSVBUTTON_H
