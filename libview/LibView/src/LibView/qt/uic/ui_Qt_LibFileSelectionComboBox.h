/********************************************************************************
** Form generated from reading UI file 'Qt_LibFileSelectionComboBox.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_LIBFILESELECTIONCOMBOBOX_H
#define UI_QT_LIBFILESELECTIONCOMBOBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_LibFileSelectionComboBox
{
public:
    QGridLayout *gridLayout;
    QComboBox *LibFileSelectionComboBox;
    QLabel *Title;

    void setupUi(QWidget *Qt_LibFileSelectionComboBox)
    {
        if (Qt_LibFileSelectionComboBox->objectName().isEmpty())
            Qt_LibFileSelectionComboBox->setObjectName(QString::fromUtf8("Qt_LibFileSelectionComboBox"));
        Qt_LibFileSelectionComboBox->resize(310, 67);
        gridLayout = new QGridLayout(Qt_LibFileSelectionComboBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        LibFileSelectionComboBox = new QComboBox(Qt_LibFileSelectionComboBox);
        LibFileSelectionComboBox->setObjectName(QString::fromUtf8("LibFileSelectionComboBox"));

        gridLayout->addWidget(LibFileSelectionComboBox, 1, 0, 1, 1);

        Title = new QLabel(Qt_LibFileSelectionComboBox);
        Title->setObjectName(QString::fromUtf8("Title"));

        gridLayout->addWidget(Title, 0, 0, 1, 1);


        retranslateUi(Qt_LibFileSelectionComboBox);

        QMetaObject::connectSlotsByName(Qt_LibFileSelectionComboBox);
    } // setupUi

    void retranslateUi(QWidget *Qt_LibFileSelectionComboBox)
    {
        Qt_LibFileSelectionComboBox->setWindowTitle(QCoreApplication::translate("Qt_LibFileSelectionComboBox", "Form", nullptr));
        Title->setText(QCoreApplication::translate("Qt_LibFileSelectionComboBox", "Selected Liberty File:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_LibFileSelectionComboBox: public Ui_Qt_LibFileSelectionComboBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_LIBFILESELECTIONCOMBOBOX_H
