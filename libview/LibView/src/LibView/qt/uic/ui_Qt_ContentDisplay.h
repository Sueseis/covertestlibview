/********************************************************************************
** Form generated from reading UI file 'Qt_ContentDisplay.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_CONTENTDISPLAY_H
#define UI_QT_CONTENTDISPLAY_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_ContentDisplay
{
public:
    QGridLayout *gridLayout;
    QTextBrowser *TextBrowser;

    void setupUi(QWidget *Qt_ContentDisplay)
    {
        if (Qt_ContentDisplay->objectName().isEmpty())
            Qt_ContentDisplay->setObjectName(QString::fromUtf8("Qt_ContentDisplay"));
        Qt_ContentDisplay->resize(400, 300);
        gridLayout = new QGridLayout(Qt_ContentDisplay);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        TextBrowser = new QTextBrowser(Qt_ContentDisplay);
        TextBrowser->setObjectName(QString::fromUtf8("TextBrowser"));

        gridLayout->addWidget(TextBrowser, 0, 0, 1, 1);


        retranslateUi(Qt_ContentDisplay);

        QMetaObject::connectSlotsByName(Qt_ContentDisplay);
    } // setupUi

    void retranslateUi(QWidget *Qt_ContentDisplay)
    {
        Qt_ContentDisplay->setWindowTitle(QCoreApplication::translate("Qt_ContentDisplay", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_ContentDisplay: public Ui_Qt_ContentDisplay {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_CONTENTDISPLAY_H
