/********************************************************************************
** Form generated from reading UI file 'Qt_SearchBar.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_SEARCHBAR_H
#define UI_QT_SEARCHBAR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_SearchBar
{
public:
    QGridLayout *gridLayout;
    QLineEdit *SearchLineEdit;
    QPushButton *SearchBarSearchButton;

    void setupUi(QWidget *Qt_SearchBar)
    {
        if (Qt_SearchBar->objectName().isEmpty())
            Qt_SearchBar->setObjectName(QString::fromUtf8("Qt_SearchBar"));
        Qt_SearchBar->resize(222, 50);
        gridLayout = new QGridLayout(Qt_SearchBar);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        SearchLineEdit = new QLineEdit(Qt_SearchBar);
        SearchLineEdit->setObjectName(QString::fromUtf8("SearchLineEdit"));
        SearchLineEdit->setMinimumSize(QSize(100, 0));
        SearchLineEdit->setMaxLength(32766);
        SearchLineEdit->setClearButtonEnabled(false);

        gridLayout->addWidget(SearchLineEdit, 0, 0, 1, 1);

        SearchBarSearchButton = new QPushButton(Qt_SearchBar);
        SearchBarSearchButton->setObjectName(QString::fromUtf8("SearchBarSearchButton"));

        gridLayout->addWidget(SearchBarSearchButton, 0, 1, 1, 1);


        retranslateUi(Qt_SearchBar);

        QMetaObject::connectSlotsByName(Qt_SearchBar);
    } // setupUi

    void retranslateUi(QWidget *Qt_SearchBar)
    {
        Qt_SearchBar->setWindowTitle(QCoreApplication::translate("Qt_SearchBar", "Form", nullptr));
        SearchLineEdit->setPlaceholderText(QCoreApplication::translate("Qt_SearchBar", "Search...", nullptr));
        SearchBarSearchButton->setText(QCoreApplication::translate("Qt_SearchBar", "Search", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_SearchBar: public Ui_Qt_SearchBar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_SEARCHBAR_H
