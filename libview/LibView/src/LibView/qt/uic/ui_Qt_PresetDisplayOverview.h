/********************************************************************************
** Form generated from reading UI file 'Qt_PresetDisplayOverview.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_PRESETDISPLAYOVERVIEW_H
#define UI_QT_PRESETDISPLAYOVERVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_PresetDisplayOverview
{
public:

    void setupUi(QWidget *Qt_PresetDisplayOverview)
    {
        if (Qt_PresetDisplayOverview->objectName().isEmpty())
            Qt_PresetDisplayOverview->setObjectName(QString::fromUtf8("Qt_PresetDisplayOverview"));
        Qt_PresetDisplayOverview->resize(400, 300);

        retranslateUi(Qt_PresetDisplayOverview);

        QMetaObject::connectSlotsByName(Qt_PresetDisplayOverview);
    } // setupUi

    void retranslateUi(QWidget *Qt_PresetDisplayOverview)
    {
        Qt_PresetDisplayOverview->setWindowTitle(QCoreApplication::translate("Qt_PresetDisplayOverview", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_PresetDisplayOverview: public Ui_Qt_PresetDisplayOverview {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_PRESETDISPLAYOVERVIEW_H
