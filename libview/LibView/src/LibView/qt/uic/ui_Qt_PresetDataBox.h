/********************************************************************************
** Form generated from reading UI file 'Qt_PresetDataBox.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_PRESETDATABOX_H
#define UI_QT_PRESETDATABOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_PresetDataBox
{
public:

    void setupUi(QWidget *Qt_PresetDataBox)
    {
        if (Qt_PresetDataBox->objectName().isEmpty())
            Qt_PresetDataBox->setObjectName(QString::fromUtf8("Qt_PresetDataBox"));
        Qt_PresetDataBox->resize(400, 300);

        retranslateUi(Qt_PresetDataBox);

        QMetaObject::connectSlotsByName(Qt_PresetDataBox);
    } // setupUi

    void retranslateUi(QWidget *Qt_PresetDataBox)
    {
        Qt_PresetDataBox->setWindowTitle(QCoreApplication::translate("Qt_PresetDataBox", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_PresetDataBox: public Ui_Qt_PresetDataBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_PRESETDATABOX_H
