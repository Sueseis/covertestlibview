/********************************************************************************
** Form generated from reading UI file 'Qt_LibFileHeader.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_LIBFILEHEADER_H
#define UI_QT_LIBFILEHEADER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_LibFileHeader
{
public:
    QGridLayout *gridLayout;
    QLabel *LibFileHeaderNameLabel;

    void setupUi(QWidget *Qt_LibFileHeader)
    {
        if (Qt_LibFileHeader->objectName().isEmpty())
            Qt_LibFileHeader->setObjectName(QString::fromUtf8("Qt_LibFileHeader"));
        Qt_LibFileHeader->resize(400, 300);
        gridLayout = new QGridLayout(Qt_LibFileHeader);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        LibFileHeaderNameLabel = new QLabel(Qt_LibFileHeader);
        LibFileHeaderNameLabel->setObjectName(QString::fromUtf8("LibFileHeaderNameLabel"));

        gridLayout->addWidget(LibFileHeaderNameLabel, 0, 0, 1, 1);


        retranslateUi(Qt_LibFileHeader);

        QMetaObject::connectSlotsByName(Qt_LibFileHeader);
    } // setupUi

    void retranslateUi(QWidget *Qt_LibFileHeader)
    {
        Qt_LibFileHeader->setWindowTitle(QCoreApplication::translate("Qt_LibFileHeader", "Form", nullptr));
        LibFileHeaderNameLabel->setText(QCoreApplication::translate("Qt_LibFileHeader", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_LibFileHeader: public Ui_Qt_LibFileHeader {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_LIBFILEHEADER_H
