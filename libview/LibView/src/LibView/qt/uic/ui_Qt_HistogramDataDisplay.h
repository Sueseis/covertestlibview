/********************************************************************************
** Form generated from reading UI file 'Qt_HistogramDataDisplay.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_HISTOGRAMDATADISPLAY_H
#define UI_QT_HISTOGRAMDATADISPLAY_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_HistogramDataDisplay
{
public:

    void setupUi(QWidget *Qt_HistogramDataDisplay)
    {
        if (Qt_HistogramDataDisplay->objectName().isEmpty())
            Qt_HistogramDataDisplay->setObjectName(QString::fromUtf8("Qt_HistogramDataDisplay"));
        Qt_HistogramDataDisplay->resize(400, 300);

        retranslateUi(Qt_HistogramDataDisplay);

        QMetaObject::connectSlotsByName(Qt_HistogramDataDisplay);
    } // setupUi

    void retranslateUi(QWidget *Qt_HistogramDataDisplay)
    {
        Qt_HistogramDataDisplay->setWindowTitle(QCoreApplication::translate("Qt_HistogramDataDisplay", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_HistogramDataDisplay: public Ui_Qt_HistogramDataDisplay {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_HISTOGRAMDATADISPLAY_H
