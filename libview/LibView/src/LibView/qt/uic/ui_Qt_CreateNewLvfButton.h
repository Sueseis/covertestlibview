/********************************************************************************
** Form generated from reading UI file 'Qt_CreateNewLvfButton.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_CREATENEWLVFBUTTON_H
#define UI_QT_CREATENEWLVFBUTTON_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_CreateNewLvfButton
{
public:
    QGridLayout *gridLayout;
    QPushButton *CreateNewLvfButton;

    void setupUi(QWidget *Qt_CreateNewLvfButton)
    {
        if (Qt_CreateNewLvfButton->objectName().isEmpty())
            Qt_CreateNewLvfButton->setObjectName(QString::fromUtf8("Qt_CreateNewLvfButton"));
        Qt_CreateNewLvfButton->resize(400, 300);
        gridLayout = new QGridLayout(Qt_CreateNewLvfButton);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        CreateNewLvfButton = new QPushButton(Qt_CreateNewLvfButton);
        CreateNewLvfButton->setObjectName(QString::fromUtf8("CreateNewLvfButton"));

        gridLayout->addWidget(CreateNewLvfButton, 0, 0, 1, 1);


        retranslateUi(Qt_CreateNewLvfButton);

        QMetaObject::connectSlotsByName(Qt_CreateNewLvfButton);
    } // setupUi

    void retranslateUi(QWidget *Qt_CreateNewLvfButton)
    {
        Qt_CreateNewLvfButton->setWindowTitle(QCoreApplication::translate("Qt_CreateNewLvfButton", "Form", nullptr));
        CreateNewLvfButton->setText(QCoreApplication::translate("Qt_CreateNewLvfButton", "create new LVF", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_CreateNewLvfButton: public Ui_Qt_CreateNewLvfButton {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_CREATENEWLVFBUTTON_H
