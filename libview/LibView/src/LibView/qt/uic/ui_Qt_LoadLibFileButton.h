/********************************************************************************
** Form generated from reading UI file 'Qt_LoadLibFileButton.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_LOADLIBFILEBUTTON_H
#define UI_QT_LOADLIBFILEBUTTON_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_LoadLibFileButton
{
public:
    QGridLayout *gridLayout;
    QPushButton *LoadLibFileButton;

    void setupUi(QWidget *Qt_LoadLibFileButton)
    {
        if (Qt_LoadLibFileButton->objectName().isEmpty())
            Qt_LoadLibFileButton->setObjectName(QString::fromUtf8("Qt_LoadLibFileButton"));
        Qt_LoadLibFileButton->resize(400, 300);
        gridLayout = new QGridLayout(Qt_LoadLibFileButton);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        LoadLibFileButton = new QPushButton(Qt_LoadLibFileButton);
        LoadLibFileButton->setObjectName(QString::fromUtf8("LoadLibFileButton"));

        gridLayout->addWidget(LoadLibFileButton, 0, 0, 1, 1);


        retranslateUi(Qt_LoadLibFileButton);

        QMetaObject::connectSlotsByName(Qt_LoadLibFileButton);
    } // setupUi

    void retranslateUi(QWidget *Qt_LoadLibFileButton)
    {
        Qt_LoadLibFileButton->setWindowTitle(QCoreApplication::translate("Qt_LoadLibFileButton", "Form", nullptr));
        LoadLibFileButton->setText(QCoreApplication::translate("Qt_LoadLibFileButton", "load LibFile", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_LoadLibFileButton: public Ui_Qt_LoadLibFileButton {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_LOADLIBFILEBUTTON_H
