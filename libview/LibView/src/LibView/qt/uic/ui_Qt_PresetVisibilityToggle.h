/********************************************************************************
** Form generated from reading UI file 'Qt_PresetVisibilityToggle.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_PRESETVISIBILITYTOGGLE_H
#define UI_QT_PRESETVISIBILITYTOGGLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_PresetVisibilityToggle
{
public:
    QGridLayout *gridLayout;
    QRadioButton *PresetVisibilityToggleRadioButton;

    void setupUi(QWidget *Qt_PresetVisibilityToggle)
    {
        if (Qt_PresetVisibilityToggle->objectName().isEmpty())
            Qt_PresetVisibilityToggle->setObjectName(QString::fromUtf8("Qt_PresetVisibilityToggle"));
        Qt_PresetVisibilityToggle->resize(400, 300);
        gridLayout = new QGridLayout(Qt_PresetVisibilityToggle);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        PresetVisibilityToggleRadioButton = new QRadioButton(Qt_PresetVisibilityToggle);
        PresetVisibilityToggleRadioButton->setObjectName(QString::fromUtf8("PresetVisibilityToggleRadioButton"));

        gridLayout->addWidget(PresetVisibilityToggleRadioButton, 0, 0, 1, 1);


        retranslateUi(Qt_PresetVisibilityToggle);

        QMetaObject::connectSlotsByName(Qt_PresetVisibilityToggle);
    } // setupUi

    void retranslateUi(QWidget *Qt_PresetVisibilityToggle)
    {
        Qt_PresetVisibilityToggle->setWindowTitle(QCoreApplication::translate("Qt_PresetVisibilityToggle", "Form", nullptr));
        PresetVisibilityToggleRadioButton->setText(QCoreApplication::translate("Qt_PresetVisibilityToggle", "RadioButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_PresetVisibilityToggle: public Ui_Qt_PresetVisibilityToggle {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_PRESETVISIBILITYTOGGLE_H
