/********************************************************************************
** Form generated from reading UI file 'Qt_DeltaPercentToggle.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_DELTAPERCENTTOGGLE_H
#define UI_QT_DELTAPERCENTTOGGLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_DeltaPercentToggle
{
public:
    QGridLayout *gridLayout;
    QGroupBox *DeltaSwitchGroupBox;
    QGridLayout *gridLayout_2;
    QRadioButton *DeltaSwitchDeltaButton;
    QRadioButton *DeltaSwitchPercentageButton;

    void setupUi(QWidget *Qt_DeltaPercentToggle)
    {
        if (Qt_DeltaPercentToggle->objectName().isEmpty())
            Qt_DeltaPercentToggle->setObjectName(QString::fromUtf8("Qt_DeltaPercentToggle"));
        Qt_DeltaPercentToggle->resize(400, 300);
        gridLayout = new QGridLayout(Qt_DeltaPercentToggle);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        DeltaSwitchGroupBox = new QGroupBox(Qt_DeltaPercentToggle);
        DeltaSwitchGroupBox->setObjectName(QString::fromUtf8("DeltaSwitchGroupBox"));
        gridLayout_2 = new QGridLayout(DeltaSwitchGroupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        DeltaSwitchDeltaButton = new QRadioButton(DeltaSwitchGroupBox);
        DeltaSwitchDeltaButton->setObjectName(QString::fromUtf8("DeltaSwitchDeltaButton"));
        DeltaSwitchDeltaButton->setChecked(true);

        gridLayout_2->addWidget(DeltaSwitchDeltaButton, 0, 0, 1, 1);

        DeltaSwitchPercentageButton = new QRadioButton(DeltaSwitchGroupBox);
        DeltaSwitchPercentageButton->setObjectName(QString::fromUtf8("DeltaSwitchPercentageButton"));

        gridLayout_2->addWidget(DeltaSwitchPercentageButton, 1, 0, 1, 1);


        gridLayout->addWidget(DeltaSwitchGroupBox, 0, 0, 1, 1);


        retranslateUi(Qt_DeltaPercentToggle);

        QMetaObject::connectSlotsByName(Qt_DeltaPercentToggle);
    } // setupUi

    void retranslateUi(QWidget *Qt_DeltaPercentToggle)
    {
        Qt_DeltaPercentToggle->setWindowTitle(QCoreApplication::translate("Qt_DeltaPercentToggle", "Form", nullptr));
        DeltaSwitchGroupBox->setTitle(QCoreApplication::translate("Qt_DeltaPercentToggle", "Display differences as:", nullptr));
        DeltaSwitchDeltaButton->setText(QCoreApplication::translate("Qt_DeltaPercentToggle", "Delta", nullptr));
        DeltaSwitchPercentageButton->setText(QCoreApplication::translate("Qt_DeltaPercentToggle", "Percentile", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_DeltaPercentToggle: public Ui_Qt_DeltaPercentToggle {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_DELTAPERCENTTOGGLE_H
