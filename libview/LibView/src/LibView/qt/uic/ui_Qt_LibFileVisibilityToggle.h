/********************************************************************************
** Form generated from reading UI file 'Qt_LibFileVisibilityToggle.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_LIBFILEVISIBILITYTOGGLE_H
#define UI_QT_LIBFILEVISIBILITYTOGGLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_LibFileVisibilityToggle
{
public:
    QGridLayout *gridLayout;
    QCheckBox *LibFileVisibilityToggleCheckBox;
    QWidget *RemoveButtonPlaceHolder;

    void setupUi(QWidget *Qt_LibFileVisibilityToggle)
    {
        if (Qt_LibFileVisibilityToggle->objectName().isEmpty())
            Qt_LibFileVisibilityToggle->setObjectName(QString::fromUtf8("Qt_LibFileVisibilityToggle"));
        Qt_LibFileVisibilityToggle->resize(120, 43);
        gridLayout = new QGridLayout(Qt_LibFileVisibilityToggle);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        LibFileVisibilityToggleCheckBox = new QCheckBox(Qt_LibFileVisibilityToggle);
        LibFileVisibilityToggleCheckBox->setObjectName(QString::fromUtf8("LibFileVisibilityToggleCheckBox"));

        gridLayout->addWidget(LibFileVisibilityToggleCheckBox, 1, 0, 1, 1);

        RemoveButtonPlaceHolder = new QWidget(Qt_LibFileVisibilityToggle);
        RemoveButtonPlaceHolder->setObjectName(QString::fromUtf8("RemoveButtonPlaceHolder"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(RemoveButtonPlaceHolder->sizePolicy().hasHeightForWidth());
        RemoveButtonPlaceHolder->setSizePolicy(sizePolicy);

        gridLayout->addWidget(RemoveButtonPlaceHolder, 1, 1, 1, 1);


        retranslateUi(Qt_LibFileVisibilityToggle);

        QMetaObject::connectSlotsByName(Qt_LibFileVisibilityToggle);
    } // setupUi

    void retranslateUi(QWidget *Qt_LibFileVisibilityToggle)
    {
        Qt_LibFileVisibilityToggle->setWindowTitle(QCoreApplication::translate("Qt_LibFileVisibilityToggle", "Form", nullptr));
        LibFileVisibilityToggleCheckBox->setText(QCoreApplication::translate("Qt_LibFileVisibilityToggle", "CheckBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_LibFileVisibilityToggle: public Ui_Qt_LibFileVisibilityToggle {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_LIBFILEVISIBILITYTOGGLE_H
