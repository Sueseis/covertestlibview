/********************************************************************************
** Form generated from reading UI file 'Qt_LibFileDataDisplay.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_LIBFILEDATADISPLAY_H
#define UI_QT_LIBFILEDATADISPLAY_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_LibFileDataDisplay
{
public:
    QGridLayout *gridLayout;
    QScrollArea *ScrollArea;
    QWidget *scrollAreaWidgetContents;

    void setupUi(QWidget *Qt_LibFileDataDisplay)
    {
        if (Qt_LibFileDataDisplay->objectName().isEmpty())
            Qt_LibFileDataDisplay->setObjectName(QString::fromUtf8("Qt_LibFileDataDisplay"));
        Qt_LibFileDataDisplay->resize(400, 300);
        gridLayout = new QGridLayout(Qt_LibFileDataDisplay);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        ScrollArea = new QScrollArea(Qt_LibFileDataDisplay);
        ScrollArea->setObjectName(QString::fromUtf8("ScrollArea"));
        ScrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 376, 276));
        ScrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(ScrollArea, 0, 0, 1, 1);


        retranslateUi(Qt_LibFileDataDisplay);

        QMetaObject::connectSlotsByName(Qt_LibFileDataDisplay);
    } // setupUi

    void retranslateUi(QWidget *Qt_LibFileDataDisplay)
    {
        Qt_LibFileDataDisplay->setWindowTitle(QCoreApplication::translate("Qt_LibFileDataDisplay", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_LibFileDataDisplay: public Ui_Qt_LibFileDataDisplay {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_LIBFILEDATADISPLAY_H
