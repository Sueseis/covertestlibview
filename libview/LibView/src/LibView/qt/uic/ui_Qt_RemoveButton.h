/********************************************************************************
** Form generated from reading UI file 'Qt_RemoveButton.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_REMOVEBUTTON_H
#define UI_QT_REMOVEBUTTON_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_RemoveButton
{
public:
    QGridLayout *gridLayout;
    QPushButton *RemoveLibFilePushButton;

    void setupUi(QWidget *Qt_RemoveButton)
    {
        if (Qt_RemoveButton->objectName().isEmpty())
            Qt_RemoveButton->setObjectName(QString::fromUtf8("Qt_RemoveButton"));
        Qt_RemoveButton->resize(116, 50);
        gridLayout = new QGridLayout(Qt_RemoveButton);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        RemoveLibFilePushButton = new QPushButton(Qt_RemoveButton);
        RemoveLibFilePushButton->setObjectName(QString::fromUtf8("RemoveLibFilePushButton"));

        gridLayout->addWidget(RemoveLibFilePushButton, 0, 0, 1, 1);


        retranslateUi(Qt_RemoveButton);

        QMetaObject::connectSlotsByName(Qt_RemoveButton);
    } // setupUi

    void retranslateUi(QWidget *Qt_RemoveButton)
    {
        Qt_RemoveButton->setWindowTitle(QCoreApplication::translate("Qt_RemoveButton", "Form", nullptr));
        RemoveLibFilePushButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Qt_RemoveButton: public Ui_Qt_RemoveButton {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_REMOVEBUTTON_H
