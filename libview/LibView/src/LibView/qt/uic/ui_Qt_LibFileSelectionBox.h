/********************************************************************************
** Form generated from reading UI file 'Qt_LibFileSelectionBox.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_LIBFILESELECTIONBOX_H
#define UI_QT_LIBFILESELECTIONBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_LibFileSelectionBox
{
public:
    QGridLayout *gridLayout;
    QScrollArea *ScrollArea;
    QWidget *ScrollAreaWidgetContents;

    void setupUi(QWidget *Qt_LibFileSelectionBox)
    {
        if (Qt_LibFileSelectionBox->objectName().isEmpty())
            Qt_LibFileSelectionBox->setObjectName(QString::fromUtf8("Qt_LibFileSelectionBox"));
        Qt_LibFileSelectionBox->resize(400, 300);
        gridLayout = new QGridLayout(Qt_LibFileSelectionBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        ScrollArea = new QScrollArea(Qt_LibFileSelectionBox);
        ScrollArea->setObjectName(QString::fromUtf8("ScrollArea"));
        ScrollArea->setWidgetResizable(true);
        ScrollAreaWidgetContents = new QWidget();
        ScrollAreaWidgetContents->setObjectName(QString::fromUtf8("ScrollAreaWidgetContents"));
        ScrollAreaWidgetContents->setGeometry(QRect(0, 0, 376, 276));
        ScrollArea->setWidget(ScrollAreaWidgetContents);

        gridLayout->addWidget(ScrollArea, 0, 0, 1, 1);


        retranslateUi(Qt_LibFileSelectionBox);

        QMetaObject::connectSlotsByName(Qt_LibFileSelectionBox);
    } // setupUi

    void retranslateUi(QWidget *Qt_LibFileSelectionBox)
    {
        Qt_LibFileSelectionBox->setWindowTitle(QCoreApplication::translate("Qt_LibFileSelectionBox", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_LibFileSelectionBox: public Ui_Qt_LibFileSelectionBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_LIBFILESELECTIONBOX_H
