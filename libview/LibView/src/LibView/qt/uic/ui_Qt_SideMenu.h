/********************************************************************************
** Form generated from reading UI file 'Qt_SideMenu.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_SIDEMENU_H
#define UI_QT_SIDEMENU_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_SideMenu
{
public:

    void setupUi(QWidget *Qt_SideMenu)
    {
        if (Qt_SideMenu->objectName().isEmpty())
            Qt_SideMenu->setObjectName(QString::fromUtf8("Qt_SideMenu"));
        Qt_SideMenu->resize(400, 300);

        retranslateUi(Qt_SideMenu);

        QMetaObject::connectSlotsByName(Qt_SideMenu);
    } // setupUi

    void retranslateUi(QWidget *Qt_SideMenu)
    {
        Qt_SideMenu->setWindowTitle(QCoreApplication::translate("Qt_SideMenu", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_SideMenu: public Ui_Qt_SideMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_SIDEMENU_H
