/********************************************************************************
** Form generated from reading UI file 'Qt_SelectGraphTypeComboBox.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_SELECTGRAPHTYPECOMBOBOX_H
#define UI_QT_SELECTGRAPHTYPECOMBOBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_SelectGraphTypeComboBox
{
public:
    QGridLayout *gridLayout;
    QComboBox *SelectGraphTypeComboBoxComboBox;

    void setupUi(QWidget *Qt_SelectGraphTypeComboBox)
    {
        if (Qt_SelectGraphTypeComboBox->objectName().isEmpty())
            Qt_SelectGraphTypeComboBox->setObjectName(QString::fromUtf8("Qt_SelectGraphTypeComboBox"));
        Qt_SelectGraphTypeComboBox->resize(400, 300);
        gridLayout = new QGridLayout(Qt_SelectGraphTypeComboBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        SelectGraphTypeComboBoxComboBox = new QComboBox(Qt_SelectGraphTypeComboBox);
        SelectGraphTypeComboBoxComboBox->setObjectName(QString::fromUtf8("SelectGraphTypeComboBoxComboBox"));

        gridLayout->addWidget(SelectGraphTypeComboBoxComboBox, 0, 0, 1, 1);


        retranslateUi(Qt_SelectGraphTypeComboBox);

        QMetaObject::connectSlotsByName(Qt_SelectGraphTypeComboBox);
    } // setupUi

    void retranslateUi(QWidget *Qt_SelectGraphTypeComboBox)
    {
        Qt_SelectGraphTypeComboBox->setWindowTitle(QCoreApplication::translate("Qt_SelectGraphTypeComboBox", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_SelectGraphTypeComboBox: public Ui_Qt_SelectGraphTypeComboBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_SELECTGRAPHTYPECOMBOBOX_H
