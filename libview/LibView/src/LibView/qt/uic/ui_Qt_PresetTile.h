/********************************************************************************
** Form generated from reading UI file 'Qt_PresetTile.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_PRESETTILE_H
#define UI_QT_PRESETTILE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_PresetTile
{
public:
    QGridLayout *gridLayout;
    QLabel *PresetTitlePrefix;
    QLabel *PresetTitleName;

    void setupUi(QWidget *Qt_PresetTile)
    {
        if (Qt_PresetTile->objectName().isEmpty())
            Qt_PresetTile->setObjectName(QString::fromUtf8("Qt_PresetTile"));
        Qt_PresetTile->resize(400, 300);
        gridLayout = new QGridLayout(Qt_PresetTile);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        PresetTitlePrefix = new QLabel(Qt_PresetTile);
        PresetTitlePrefix->setObjectName(QString::fromUtf8("PresetTitlePrefix"));

        gridLayout->addWidget(PresetTitlePrefix, 0, 0, 1, 1);

        PresetTitleName = new QLabel(Qt_PresetTile);
        PresetTitleName->setObjectName(QString::fromUtf8("PresetTitleName"));

        gridLayout->addWidget(PresetTitleName, 0, 1, 1, 1);


        retranslateUi(Qt_PresetTile);

        QMetaObject::connectSlotsByName(Qt_PresetTile);
    } // setupUi

    void retranslateUi(QWidget *Qt_PresetTile)
    {
        Qt_PresetTile->setWindowTitle(QCoreApplication::translate("Qt_PresetTile", "Form", nullptr));
        PresetTitlePrefix->setText(QCoreApplication::translate("Qt_PresetTile", "Preset:", nullptr));
        PresetTitleName->setText(QCoreApplication::translate("Qt_PresetTile", "Undefined", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_PresetTile: public Ui_Qt_PresetTile {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_PRESETTILE_H
