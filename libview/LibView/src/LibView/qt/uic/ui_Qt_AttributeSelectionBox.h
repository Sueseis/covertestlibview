/********************************************************************************
** Form generated from reading UI file 'Qt_AttributeSelectionBox.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_ATTRIBUTESELECTIONBOX_H
#define UI_QT_ATTRIBUTESELECTIONBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_AttributeSelectionBox
{
public:
    QGridLayout *gridLayout;
    QWidget *SearchBarPlaceholder;
    QTreeView *TreeView;

    void setupUi(QWidget *Qt_AttributeSelectionBox)
    {
        if (Qt_AttributeSelectionBox->objectName().isEmpty())
            Qt_AttributeSelectionBox->setObjectName(QString::fromUtf8("Qt_AttributeSelectionBox"));
        Qt_AttributeSelectionBox->resize(400, 300);
        gridLayout = new QGridLayout(Qt_AttributeSelectionBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        SearchBarPlaceholder = new QWidget(Qt_AttributeSelectionBox);
        SearchBarPlaceholder->setObjectName(QString::fromUtf8("SearchBarPlaceholder"));

        gridLayout->addWidget(SearchBarPlaceholder, 0, 0, 1, 1);

        TreeView = new QTreeView(Qt_AttributeSelectionBox);
        TreeView->setObjectName(QString::fromUtf8("TreeView"));

        gridLayout->addWidget(TreeView, 1, 0, 1, 1);


        retranslateUi(Qt_AttributeSelectionBox);

        QMetaObject::connectSlotsByName(Qt_AttributeSelectionBox);
    } // setupUi

    void retranslateUi(QWidget *Qt_AttributeSelectionBox)
    {
        Qt_AttributeSelectionBox->setWindowTitle(QCoreApplication::translate("Qt_AttributeSelectionBox", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_AttributeSelectionBox: public Ui_Qt_AttributeSelectionBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_ATTRIBUTESELECTIONBOX_H
