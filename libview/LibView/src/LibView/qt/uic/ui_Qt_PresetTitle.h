/********************************************************************************
** Form generated from reading UI file 'Qt_PresetTitle.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_PRESETTITLE_H
#define UI_QT_PRESETTITLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Qt_PresetTitle
{
public:
    QGridLayout *gridLayout;
    QLabel *PresetTitlePrefix;
    QLabel *PresetTitleName;

    void setupUi(QWidget *Qt_PresetTitle)
    {
        if (Qt_PresetTitle->objectName().isEmpty())
            Qt_PresetTitle->setObjectName(QString::fromUtf8("Qt_PresetTitle"));
        Qt_PresetTitle->resize(400, 300);
        gridLayout = new QGridLayout(Qt_PresetTitle);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        PresetTitlePrefix = new QLabel(Qt_PresetTitle);
        PresetTitlePrefix->setObjectName(QString::fromUtf8("PresetTitlePrefix"));

        gridLayout->addWidget(PresetTitlePrefix, 0, 0, 1, 1);

        PresetTitleName = new QLabel(Qt_PresetTitle);
        PresetTitleName->setObjectName(QString::fromUtf8("PresetTitleName"));

        gridLayout->addWidget(PresetTitleName, 0, 1, 1, 1);


        retranslateUi(Qt_PresetTitle);

        QMetaObject::connectSlotsByName(Qt_PresetTitle);
    } // setupUi

    void retranslateUi(QWidget *Qt_PresetTitle)
    {
        Qt_PresetTitle->setWindowTitle(QCoreApplication::translate("Qt_PresetTitle", "Form", nullptr));
        PresetTitlePrefix->setText(QCoreApplication::translate("Qt_PresetTitle", "Preset:", nullptr));
        PresetTitleName->setText(QCoreApplication::translate("Qt_PresetTitle", "Undefined", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Qt_PresetTitle: public Ui_Qt_PresetTitle {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_PRESETTITLE_H
