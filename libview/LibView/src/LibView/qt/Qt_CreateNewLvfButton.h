#pragma once

#include <QWidget>
#include "uic/ui_Qt_CreateNewLvfButton.h"

class Qt_CreateNewLvfButton : public QWidget 
{
	Q_OBJECT

public:
  Qt_CreateNewLvfButton(QWidget *parent = Q_NULLPTR);
  ~Qt_CreateNewLvfButton(); 

private:
  Ui::Qt_CreateNewLvfButton ui;
};
