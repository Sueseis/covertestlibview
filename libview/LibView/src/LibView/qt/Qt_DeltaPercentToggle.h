#pragma once

#include <QWidget>
#include "uic/ui_Qt_DeltaPercentToggle.h"

class Qt_DeltaPercentToggle : public QWidget 
{
	Q_OBJECT

public:
  Qt_DeltaPercentToggle(QWidget *parent = Q_NULLPTR);
  ~Qt_DeltaPercentToggle(); 

private:
  Ui::Qt_DeltaPercentToggle ui;
};
