#pragma once

#include <QWidget>
#include "uic/ui_Qt_PinButton.h"

class Qt_PinButton : public QWidget 
{
	Q_OBJECT

public:
  Qt_PinButton(QWidget *parent = Q_NULLPTR);
  ~Qt_PinButton(); 

private:
  Ui::Qt_PinButton ui;
};
