#pragma once

#include <QWidget>
#include "uic/ui_Qt_GroupDisplay.h"

class Qt_GroupDisplay : public QWidget 
{
	Q_OBJECT

public:
  Qt_GroupDisplay(QWidget *parent = Q_NULLPTR);
  ~Qt_GroupDisplay(); 

private:
  Ui::Qt_GroupDisplay ui;
};
