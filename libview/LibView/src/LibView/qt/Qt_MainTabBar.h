#pragma once

#include <QWidget>
#include "uic/ui_Qt_MainTabBar.h"

class Qt_MainTabBar : public QWidget 
{
	Q_OBJECT

public:
  Qt_MainTabBar(QWidget *parent = Q_NULLPTR);
  ~Qt_MainTabBar(); 

private:
  Ui::Qt_MainTabBar ui;
};
