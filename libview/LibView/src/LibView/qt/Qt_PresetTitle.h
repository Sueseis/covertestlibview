#pragma once

#include <QWidget>
#include "uic/ui_Qt_PresetTitle.h"

class Qt_PresetTitle : public QWidget 
{
	Q_OBJECT

public:
  Qt_PresetTitle(QWidget *parent = Q_NULLPTR);
  ~Qt_PresetTitle(); 

private:
  Ui::Qt_PresetTitle ui;
};
