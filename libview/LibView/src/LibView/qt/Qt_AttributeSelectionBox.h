#pragma once

#include <QWidget>
#include "uic/ui_Qt_AttributeSelectionBox.h"

class Qt_AttributeSelectionBox : public QWidget 
{
	Q_OBJECT

public:
  Qt_AttributeSelectionBox(QWidget *parent = Q_NULLPTR);
  ~Qt_AttributeSelectionBox(); 

private:
  Ui::Qt_AttributeSelectionBox ui;
};
