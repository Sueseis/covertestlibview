#pragma once

#include <QWidget>
#include "uic/ui_Qt_MergeAndCreateNewFileButton.h"

class Qt_MergeAndCreateNewFileButton : public QWidget 
{
	Q_OBJECT

public:
  Qt_MergeAndCreateNewFileButton(QWidget *parent = Q_NULLPTR);
  ~Qt_MergeAndCreateNewFileButton(); 

private:
  Ui::Qt_MergeAndCreateNewFileButton ui;
};
