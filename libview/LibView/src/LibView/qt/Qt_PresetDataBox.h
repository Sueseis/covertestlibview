#pragma once

#include <QWidget>
#include "uic/ui_Qt_PresetDataBox.h"

class Qt_PresetDataBox : public QWidget 
{
	Q_OBJECT

public:
  Qt_PresetDataBox(QWidget *parent = Q_NULLPTR);
  ~Qt_PresetDataBox(); 

private:
  Ui::Qt_PresetDataBox ui;
};
