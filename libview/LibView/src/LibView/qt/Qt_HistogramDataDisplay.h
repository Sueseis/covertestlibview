#pragma once

#include <QWidget>
#include "uic/ui_Qt_HistogramDataDisplay.h"

class Qt_HistogramDataDisplay : public QWidget 
{
	Q_OBJECT

public:
  Qt_HistogramDataDisplay(QWidget *parent = Q_NULLPTR);
  ~Qt_HistogramDataDisplay(); 

private:
  Ui::Qt_HistogramDataDisplay ui;
};
