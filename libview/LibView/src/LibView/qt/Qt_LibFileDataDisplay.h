#pragma once

#include <QWidget>
#include "uic/ui_Qt_LibFileDataDisplay.h"

class Qt_LibFileDataDisplay : public QWidget 
{
	Q_OBJECT

public:
  Qt_LibFileDataDisplay(QWidget *parent = Q_NULLPTR);
  ~Qt_LibFileDataDisplay(); 

private:
  Ui::Qt_LibFileDataDisplay ui;
};
