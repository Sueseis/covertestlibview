#pragma once

#include <QWidget>
#include "uic/ui_Qt_PresetVisibilityToggle.h"

class Qt_PresetVisibilityToggle : public QWidget 
{
	Q_OBJECT

public:
  Qt_PresetVisibilityToggle(QWidget *parent = Q_NULLPTR);
  ~Qt_PresetVisibilityToggle(); 

private:
  Ui::Qt_PresetVisibilityToggle ui;
};
