#pragma once

/** @file 
 *
 * This header defines Global macros and constants for use throughout the project.
 */



 /**
  * Defines the space to allocate additionally to a char buffer to account for the 0x00 null temination character.
  */
#define LIBVIEW_NULL_TERMINATION_BUFFER_SIZE 1


#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

/**
 * Outputs the current location in a file.
 */
#define LIBVIEW_LOCATION __FILE__ " : " TOSTRING(__LINE__)