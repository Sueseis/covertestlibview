#pragma once
#ifdef LIBVIEW_DEBUG

#include <iostream>

#define LIBVIEW_LOG(X) std::cout<< X << std::endl 
#define LIBVIEW_LOG_BEGIN std::cout 
#define LIBVIEW_LOG_ELEMENT(X)  << X 
#define LIBVIEW_LOG_END  << std::endl; 

#endif 




#ifdef LIBVIEW_TEST

#include <iostream>
#define LIBVIEW_LOG(X) std::cout<< X << std::endl 
#define LIBVIEW_LOG_BEGIN std::cout 
#define LIBVIEW_LOG_ELEMENT(X)  << X 
#define LIBVIEW_LOG_END  << std::endl; 

#endif

#ifdef LIBVIEW_RELEASE

#define LIBVIEW_LOG(X)
#define LIBVIEW_LOG_BEGIN 
#define LIBVIEW_LOG_ELEMENT(X)  
#define LIBVIEW_LOG_END 


#endif