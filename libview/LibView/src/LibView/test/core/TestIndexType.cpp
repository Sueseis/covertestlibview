#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"



#include "LibView/core/LibFileIndex.h"

#include <iostream>

SCENARIO("Created Indices Are unique", "[LibView::index_t]")
{
    GIVEN("Nothing")
    {
        WHEN("A new index is requested")
        {
            LibView::index_t::reset();
            LibView::index_t ind = LibView::index_t::requestUnique();

            THEN("The index info can be retrieved")
            {
                REQUIRE(ind.index() == 0);
                REQUIRE(ind.isNull() == false);
            }
        }
    }

    GIVEN("An instance of index")
    {
        LibView::index_t ind1 = LibView::index_t::requestUnique();
        
        WHEN("A new index is assigned its value")
        {
            LibView::index_t ind2 = ind1;

            THEN("The index info can be retrieved")
            {
                REQUIRE(ind2.index() == ind1.index());
                REQUIRE(ind2.isNull() == ind1.isNull());
            }
        }

        WHEN("A new index is created ")
        {
            LibView::index_t ind2 = LibView::index_t::requestUnique();
            LibView::index_t ind3 = LibView::index_t::requestUnique();

            THEN("The index info is not equal")
            {
                REQUIRE(ind2.index() != ind3.index());
            }
        }
    }
}

#endif