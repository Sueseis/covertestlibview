#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"



#include "LibView/model/AttributeMap.h"
#include "LibView/exception/model/AttributeIndexAlreadyFoundException.h"
#include "LibView/exception/model/AttributeIndexNotFoundException.h"
#include "LibView/exception/model/AttributePathAlreadyFoundException.h"
#include "LibView/exception/model/AttributePathNotFoundException.h"


SCENARIO("Values can be removed from an AttributeMap instance.", "[LibView::model::AttributeMap]")
{
    GIVEN("An AttributeMap instance with datasets.")
    {
        LibView::model::AttributeMap map;

        LibView::index_t index1 = LibView::index_t::requestUnique();
        LibView::index_t index2 = LibView::index_t::requestUnique();

        REQUIRE_NOTHROW(map.add("path", index1, 2));
        REQUIRE_NOTHROW(map.add("path2", index2, 2));

        WHEN("A path is removed")
        {
            REQUIRE_NOTHROW(map.remove("path"));

            THEN("The remaining values are present in the underlying map data structure.")
            {
                REQUIRE(map.m_AttributeMap["path2"].getFirst() == index2);
                REQUIRE(map.m_AttributeMap["path2"].getSecond() == 2);
                REQUIRE(map.m_ReverseMap[index2] == "path2");
            }

            THEN("The removed values can no longer be found in the underlying map data structure.")
            {
                REQUIRE(map.m_AttributeMap.find("path") == map.m_AttributeMap.end());
                REQUIRE(map.m_ReverseMap.find(index1) == map.m_ReverseMap.end());
            }

            THEN("Getting the removed values throws an exception.")
            {
                REQUIRE_THROWS_AS(map["path"], LibView::exception::AttributePathNotFoundException);
                REQUIRE_THROWS_AS(map[index1], LibView::exception::AttributeIndexNotFoundException);
            }
        }
    }
}

#endif