#if 0
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/BasicTable.h"


SCENARIO("BasicTable Values can be set through setters and get through getters.", "[LibView::model::data_structure::BasicTable]")
{
    GIVEN("An empty BasicTable instance.")
    {
        LibView::model::data_structure::BasicTable basicTable;
        LibView::model::DynamicDimensionArray dda(3);


        WHEN("values is set.")
        {
            dda.reserve(10);
            for(size_t i = 0; i < 10; i++)
               dda[i].reserve(5);
            dda[9][1] << 5 << 8 << 42 << 17;
            dda[1][3] << 6 << 69;

            REQUIRE_NOTHROW(basicTable.setValues(dda));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE_NOTHROW(basicTable.getValues()->getSize());
            }
        }
    }
}

#endif
#endif