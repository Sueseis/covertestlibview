#if 0
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"


#include <iostream>


#include"LibView/model/data_structure/LibertyFileGroup.h"
#include <LibView/model/data_structure/Library.h>
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("LibertyFileGroup Values can be set through setters and get through getters.", "[LibView::model::data_structure::LibertyFileGroup]")
{
    GIVEN("LibertyFileGroup class with path.")
    {
        LibView::model::data_structure::LibertyFileGroup lfg = LibView::model::data_structure::LibertyFileGroup(nullptr); //TODO
        //lfg.setPath("lfg");
        REQUIRE_NOTHROW(lfg.setPath("lfg()"));

        WHEN("groupName is set.")
        {
            REQUIRE_NOTHROW(lfg.setGroupName("randomUndifinedGroup"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg.getGroupName() == "randomUndifinedGroup");
            }
        }

        WHEN("name is set.")
        {
            REQUIRE_NOTHROW(lfg.setName("typical"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg.getName() == "typical");
            }
        }

        WHEN("path is set.")
        {
            REQUIRE_NOTHROW(lfg.setPath("library/random/idk"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg.getPath() == "library/random/idk");
            }
        }
/**
        FIX std::string = nullptr befor using
        
        WHEN("parent is set.")
        {
            // NOTE:
            // LibView::model::data_structure::Library* library; only creates the pointer and initializes it as nullptr. to also create the object call new ...
            LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library();

            REQUIRE_NOTHROW(lfg.setParent(library));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg.getParent() == library);
            }

            delete library;
        }

        WHEN("child is set.")
        {
            LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library();;
            LibView::model::data_structure::Library* library2 = new LibView::model::data_structure::Library();;
            LibView::model::data_structure::Library* library3 = new LibView::model::data_structure::Library();;
            library->setPath("lfg()/one()");
            library2->setPath("lfg()/two()");
            library3->setPath("lfg()/three()");
            REQUIRE_NOTHROW(lfg.setChild(library));
            REQUIRE_NOTHROW(lfg.setChild(library2));
            REQUIRE_NOTHROW(lfg.setChild(library3));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg.getChild("one()") == library);
                REQUIRE(lfg.getChild("two()") == library2);
                REQUIRE(lfg.getChild("three()") == library3);
            }

            delete library;
            delete library2;
            delete library3;
        }

        WHEN("path is set.")
        {
            REQUIRE_NOTHROW(lfg.setUndifinedAttribute("undifinedAttribute", "42"));
            REQUIRE_NOTHROW(lfg.setUndifinedAttribute("undifinedAttribute2", "420"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg.getUndifinedAttribute("undifinedAttribute") == "42");
                REQUIRE(lfg.getUndifinedAttribute("undifinedAttribute2") == "420");
            }
        }*/

    }
}
#endif
#endif

/** DELTA TEST
WHEN("path is set.")
        {
            REQUIRE_NOTHROW(lfg.setDelta(42, 3, 5));
            REQUIRE_NOTHROW(lfg.setDelta(69, 1, 8));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg.getDelta(3, 5) == 42);
                REQUIRE(lfg.getDelta(1, 8) == 69);
            }
        }
*/