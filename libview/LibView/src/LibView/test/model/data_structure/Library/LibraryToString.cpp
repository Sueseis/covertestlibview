#if 0

#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"


#include"LibView/model/data_structure/Library.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

#include <iostream>

SCENARIO("The Library class can be returned as a string in the form of a liberty file group.", "[LibView::model::data_structure::Library]")
{
	GIVEN("An empty Library instance.")
	{
		LibView::model::data_structure::Library library = LibView::model::data_structure::Library(nullptr); //TODO

		WHEN("Library is fully set.")
		{
			REQUIRE_NOTHROW(library.setName("typical"));
			REQUIRE_NOTHROW(library.setDelay_module("table_lookup"));
			std::vector<std::string> temp;
			temp.push_back("report_delay_calculation");
			temp.push_back("report_power_calculation");
			REQUIRE_NOTHROW(library.setLibrary_features(temp));
			REQUIRE_NOTHROW(library.setTime_unit("1ns"));
			REQUIRE_NOTHROW(library.setCurrent_unit("1mA"));
			REQUIRE_NOTHROW(library.setVoltage_unit("1V"));
			REQUIRE_NOTHROW(library.setPulling_resistance_unit("1ohm"));
			REQUIRE_NOTHROW(library.setCapacitive_load_unit(1, "pf"));
			REQUIRE_NOTHROW(library.setLeakage_power_unit("1uW"));
			REQUIRE_NOTHROW(library.setInput_threshold_pct_fall(50));
			REQUIRE_NOTHROW(library.setInput_threshold_pct_rise(50));
			REQUIRE_NOTHROW(library.setOutput_threshold_pct_fall(50));
			REQUIRE_NOTHROW(library.setOutput_threshold_pct_rise(50));
			REQUIRE_NOTHROW(library.setSlew_lower_threshold_pct_fall(10));
			REQUIRE_NOTHROW(library.setSlew_lower_threshold_pct_rise(10));
			REQUIRE_NOTHROW(library.setSlew_upper_threshold_pct_fall(90));
			REQUIRE_NOTHROW(library.setSlew_upper_threshold_pct_rise(90));
			REQUIRE_NOTHROW(library.setSlew_derate_from_library(1));
			REQUIRE_NOTHROW(library.setNom_process(1));
			REQUIRE_NOTHROW(library.setNom_temperature(125));
			REQUIRE_NOTHROW(library.setNom_voltage(0.8));
			REQUIRE_NOTHROW(library.setDefault_cell_leakage_power(0));
			REQUIRE_NOTHROW(library.setDefault_fanout_load(0));
			REQUIRE_NOTHROW(library.setDefault_inout_pin_cap(0));
			REQUIRE_NOTHROW(library.setDefault_input_pin_cap(0));
			REQUIRE_NOTHROW(library.setDefault_output_pin_cap(0));
			REQUIRE_NOTHROW(library.setDefault_leakage_power_density(0));

			THEN("class can be returned as string.")
			{
				std::string temp = "library(typical) {\n ";
				std::string temp1 ="delay_model : table_lookup;\n";
				std::string temp2 ="library_features(report_delay_calculation, report_power_calculation);\n";
				std::string temp3 ="time_unit : 1ns;\n";
				std::string temp4 ="current_unit : 1mA;\n";
				std::string temp5 ="voltage_unit : 1V;\n";
				std::string temp6 ="pulling_resistance_unit : 1ohm;\n";
				std::string temp7 ="capacitive_load_unit(1, pf);\n";
				std::string temp8 ="leakage_power_unit : 1uW;\n";
				std::string temp9 ="input_threshold_pct_fall : 50;\n";
				std::string temp10 ="input_threshold_pct_rise : 50;\n";
				std::string temp11 ="output_threshold_pct_fall : 50;\n";
				std::string temp12 ="output_threshold_pct_rise : 50;\n";
				std::string temp13 ="slew_lower_threshold_pct_fall : 10;\n";
				std::string temp14 ="slew_lower_threshold_pct_rise : 10;\n";
				std::string temp15 ="slew_upper_threshold_pct_fall : 90;\n";
				std::string temp16 ="slew_upper_threshold_pct_rise : 90;\n";
				std::string temp17 ="slew_derate_from_library : 1;\n";
				std::string temp18 ="nom_process : 1;\n";
				std::string temp19 ="nom_temperature : 125;\n";
				std::string temp20 ="nom_voltage : 0.8;\n";
				std::string temp21 ="default_cell_leakage_power : 0\n";
				std::string temp22 ="default_fanout_load : 0;\n";
				std::string temp23 ="default_inout_pin_cap : 0;\n";
				std::string temp24 ="default_input_pin_cap : 0;\n";
				std::string temp25 ="default_output_pin_cap : 0;\n";
				std::string temp26 ="default_leakage_power_density : 0;\n\n";
				std::string result = temp1 + temp2 + temp3 + temp4 + temp5 + temp6 + temp7 + temp8 + temp9 + temp10 + temp11 + temp12 + temp13 + temp14 + temp15 + temp16 + temp17 + temp18 + temp19 + temp20 + temp21 + temp22 + temp23 + temp24 + temp25 + temp26;

				REQUIRE(library.toString() == result);
			}
		}

		WHEN("Library is partly set.")
		{
			REQUIRE_NOTHROW(library.setName("typical"));
			REQUIRE_NOTHROW(library.setDelay_module("table_lookup"));
			std::vector<std::string> temp;
			temp.push_back("report_delay_calculation");
			temp.push_back("report_power_calculation");
			REQUIRE_NOTHROW(library.setLibrary_features(temp));
			REQUIRE_NOTHROW(library.setTime_unit("1ns"));
			REQUIRE_NOTHROW(library.setCurrent_unit("1mA"));
			REQUIRE_NOTHROW(library.setVoltage_unit("1V"));
			REQUIRE_NOTHROW(library.setPulling_resistance_unit("1ohm"));
			REQUIRE_NOTHROW(library.setCapacitive_load_unit(1, "pf"));
			REQUIRE_NOTHROW(library.setLeakage_power_unit("1uW"));
			REQUIRE_NOTHROW(library.setInput_threshold_pct_fall(50));
			REQUIRE_NOTHROW(library.setInput_threshold_pct_rise(50));
			REQUIRE_NOTHROW(library.setOutput_threshold_pct_fall(50));
			REQUIRE_NOTHROW(library.setOutput_threshold_pct_rise(50));
			REQUIRE_NOTHROW(library.setSlew_lower_threshold_pct_fall(10));
			REQUIRE_NOTHROW(library.setSlew_lower_threshold_pct_rise(10));
			REQUIRE_NOTHROW(library.setSlew_upper_threshold_pct_fall(90));
			REQUIRE_NOTHROW(library.setSlew_upper_threshold_pct_rise(90));
			REQUIRE_NOTHROW(library.setSlew_derate_from_library(1));
			REQUIRE_NOTHROW(library.setNom_process(1));
			REQUIRE_NOTHROW(library.setNom_temperature(125));
			REQUIRE_NOTHROW(library.setNom_voltage(0.8));
			REQUIRE_NOTHROW(library.setDefault_cell_leakage_power(0));
			REQUIRE_NOTHROW(library.setDefault_fanout_load(0));

			THEN("class can be returned as string.")
			{
				std::string temp = "library(typical) {\n ";
				std::string temp1 = "delay_model : table_lookup;\n";
				std::string temp2 = "library_features(report_delay_calculation, report_power_calculation);\n";
				std::string temp3 = "time_unit : 1ns;\n";
				std::string temp4 = "current_unit : 1mA;\n";
				std::string temp5 = "voltage_unit : 1V;\n";
				std::string temp6 = "pulling_resistance_unit : 1ohm;\n";
				std::string temp7 = "capacitive_load_unit(1, pf);\n";
				std::string temp8 = "leakage_power_unit : 1uW;\n";
				std::string temp9 = "input_threshold_pct_fall : 50;\n";
				std::string temp10 = "input_threshold_pct_rise : 50;\n";
				std::string temp11 = "output_threshold_pct_fall : 50;\n";
				std::string temp12 = "output_threshold_pct_rise : 50;\n";
				std::string temp13 = "slew_lower_threshold_pct_fall : 10;\n";
				std::string temp14 = "slew_lower_threshold_pct_rise : 10;\n";
				std::string temp15 = "slew_upper_threshold_pct_fall : 90;\n";
				std::string temp16 = "slew_upper_threshold_pct_rise : 90;\n";
				std::string temp17 = "slew_derate_from_library : 1;\n";
				std::string temp18 = "nom_process : 1;\n";
				std::string temp19 = "nom_temperature : 125;\n";
				std::string temp20 = "nom_voltage: 0.8;\n";
				std::string temp21 = "default_cell_leakage_power : 0\n";
				std::string temp22 = "default_fanout_load : 0;\n\n";
				std::string result = temp1 + temp2 + temp3 + temp4 + temp5 + temp6 + temp7 + temp8 + temp9 + temp10 + temp11 + temp12 + temp13 + temp14 + temp15 + temp16 + temp17 + temp18 + temp19 + temp20 + temp21 + temp22;

				REQUIRE(library.toString() == result);
			}
		}
	}
}




#endif
#endif
