
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/Cell.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("Cell Values can be set through setters and get through getters.", "[LibView::model::data_structure::Cell]")
{
    
    GIVEN("An empty Cell instance.")
    {
        LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
        LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


        LibView::model::data_structure::Cell* cell = new LibView::model::data_structure::Cell(modelFacade);
        modelFacade->addAttribute("");

        //LibView::model::data_structure::Cell cell = LibView::model::data_structure::Cell(nullptr);

        WHEN("cell_leakage_power is set.")
        {
            REQUIRE_NOTHROW(cell->setCell_leakage_power(0.8));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(cell->getCell_leakage_power() == 0.8);
            }
        }

        delete presenter;
        
    }

   


    int i = 0;
}
#endif
