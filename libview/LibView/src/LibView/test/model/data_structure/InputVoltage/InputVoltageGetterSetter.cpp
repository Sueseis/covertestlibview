#ifdef LIBVIEW_TEST

#if 0

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/InputVoltage.h"
#include"LibView/model/data_structure/LibertyFileGroup.h"
#include"LibView/model/data_structure/Library.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"


SCENARIO("InputVoltage Values can be set through setters and get through getters.", "[LibView::model::data_structure::InputVoltage]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty InputVoltage instance, and a Library Parent with voltage_unit set to 100mV")
    {
        LibView::model::data_structure::InputVoltage inputVoltage(modelFacade);

        LibView::model::data_structure::LibertyFileGroup* library(nullptr);

        LibView::model::data_structure::Library* test = (LibView::model::data_structure::Library*) library;

        REQUIRE_NOTHROW(test->setVoltage_unit("100mV"));

        REQUIRE_NOTHROW(inputVoltage.setParent(library));


        WHEN("vih is set.")
        {
            REQUIRE_NOTHROW(inputVoltage.setVih("VDD + 0.8"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(inputVoltage.getVih()->getAsDouble() == 50.8);

                REQUIRE(inputVoltage.getVih()->getAsString() == "VDD + 0.8");
            }
        }

        WHEN("vil is set.")
        {
            REQUIRE_NOTHROW(inputVoltage.setVil("VDD + 0.8"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(inputVoltage.getVil()->getAsDouble() == 50.8);

                REQUIRE(inputVoltage.getVil()->getAsString() == "VDD + 0.8");
            }
        }
    }

    GIVEN("An empty InputVoltage instance, and a Library Parent with voltage_unit set to 10mV")
    {
        LibView::model::data_structure::InputVoltage inputVoltage = LibView::model::data_structure::InputVoltage(nullptr);

        LibView::model::data_structure::LibertyFileGroup* library = new LibView::model::data_structure::Library(nullptr);

        LibView::model::data_structure::Library* test = (LibView::model::data_structure::Library*) library;

        REQUIRE_NOTHROW(test->setVoltage_unit("10mV"));

        REQUIRE_NOTHROW(inputVoltage.setParent(library));


        WHEN("vimin is set.")
        {
            REQUIRE_NOTHROW(inputVoltage.setVimin("4.2 + VDD"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(inputVoltage.getVimin()->getAsDouble() == 504.2);

                REQUIRE(inputVoltage.getVimin()->getAsString() == "4.2 + VDD");
            }
        }

        WHEN("vimax is set.")
        {
            REQUIRE_NOTHROW(inputVoltage.setVimax("VCC"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(inputVoltage.getVimax()->getAsDouble() == 500);

                REQUIRE(inputVoltage.getVimax()->getAsString() == "VCC");
            }
        }
    }
        delete presenter;
}
#endif

#endif