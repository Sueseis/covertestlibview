#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"


//Helpers
#include <iostream>
#include <fstream> 
#include <stdio.h>
#include <stdlib.h>
#include "LibView/model/translator/reader/PreCalculatingCharacterReader.h" //Testet class

SCENARIO("An ASCII text file can be read by first setting the string, then reading character by character.")
{

	GIVEN("An example text file.")
	{

		//setup of the test file if not existing

		std::string text = "";

		for (int i = 0; i < 128; i++)
		{
			text += (char)i;
		}

		WHEN("One calls the reader with a given text.")
		{
			THEN("The characters making the text up can be accessed sequentialy.")
			{
				LibView::model::translator::reader::PreCalculatingCharacterReader reader(&text);
				int i = 0;
				while (reader.hasNextCharacter())
				{
					REQUIRE((char)i == reader.getNextCharacter()); //All ASCII characters can be read.
					i++;
				}
				REQUIRE(i == 128); //all characters read
				REQUIRE(!reader.hasNextCharacter());
				
				}
		
		}
	}
}
#endif // LIBVIEW_DEBUG