#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"


#include "LibView/exception/model/reader/NotASCIICharacterException.h"

//Helpers
#include <iostream>
#include <fstream> 
#include <stdio.h>
#include <stdlib.h>
#include "LibView/model/translator/reader/PreCalculatingCharacterReader.h" //Testet class

SCENARIO("The given file contains non ASCII characters.")
{

	GIVEN("Texts with non ascii character.")
	{

		//setup of the test file if not existing

		std::string text = "�";


		WHEN("One calls the reader with single non ASCII characater.")
		{
			THEN("The reader throws an error trying to get the next character.")
			{
				LibView::model::translator::reader::PreCalculatingCharacterReader reader(&text);

				REQUIRE(reader.hasNextCharacter());
				REQUIRE_THROWS_AS(reader.getNextCharacter(), LibView::exception::NOTASCIICharacterException);
			}
		}

		std::string text2 = "�";
		text += ((char)-1);
		text += '�';


		WHEN("One calls the reader with multiple non ASCII characater.")
		{
			THEN("The reader throws an error trying to get the next character, but recognizes that there is a character.")
			{
				LibView::model::translator::reader::PreCalculatingCharacterReader reader(&text2);

				REQUIRE(reader.hasNextCharacter());
				REQUIRE_THROWS_AS(reader.getNextCharacter(), LibView::exception::NOTASCIICharacterException);
			}
		}



	}
}
#endif // LIBVIEW_DEBUG