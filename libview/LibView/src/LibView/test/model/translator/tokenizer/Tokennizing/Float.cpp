#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include "LibView/model/translator/tokenizer/OnDemandTokenizer.h" //Testet class


 struct FloatTest {

	static void checkString(std::string tokenString) {

		LibView::model::translator::tokenizer::OnDemandTokenizer odTok = LibView::model::translator::tokenizer::OnDemandTokenizer(&tokenString);

		REQUIRE(odTok.hasNextToken());
		LibView::model::translator::token::Token createdToken = odTok.getNextToken();
		REQUIRE(!(odTok.hasNextToken()));

		REQUIRE(createdToken.getKey() == LibView::model::translator::token::TokenKeys::Float);
		REQUIRE(createdToken.getValue() == tokenString);

	}
};


SCENARIO("The tokenizer reads a float.")
{


	GIVEN("0.0 string.")
	{
		FloatTest::checkString("0.0");
	}

	GIVEN("00.0 string.")
	{
		FloatTest::checkString("00.0");
	}

	GIVEN("1.0 string.")
	{
		FloatTest::checkString("1.0");
	}
	GIVEN("Muliple digits after decimal point string.")
	{
		FloatTest::checkString("1.01");
	}
}
#endif