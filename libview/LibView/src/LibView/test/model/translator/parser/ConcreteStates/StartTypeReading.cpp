#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include "LibView/model/translator/parser/ConcreteStates/StartTypeReading.h" //Testet class

#include "LibView/model/translator/parser/StackParser.h"
#include "LibView/model/translator/tokenizer/OnDemandTokenizer.h"
#include <string>

//Helpers
#include <iostream>

#include <stdio.h>


SCENARIO("Parser is called with only Spaces.")
{

	GIVEN("Parser and tokenizer.")
	{
		
		std::string text = " 	\nhallo";

		LibView::model::translator::parser::StackParser parser;

		LibView::model::translator::tokenizer::OnDemandTokenizer* tokenizer = new
			LibView::model::translator::tokenizer::OnDemandTokenizer(&text);


		//parser.calculateAST(tokenizer);
	
	}
}
#endif // LIBVIEW_DEBUG