#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include "LibView/model/translator/token/Token.h"

SCENARIO("Initialisation of a new token as intended.")
{
	GIVEN("The token word and its postion in the text.")
	{
		//test data
		std::string word = "abcTest";
		uint64_t row = 30;
		uint64_t column = 24;
		LibView::model::translator::token::TokenKeys key = LibView::model::translator::token::TokenKeys::Name;

		//init

		WHEN("A token is created")
		{
			THEN("The values can be retrieved via the getters.")
			{
				LibView::model::translator::token::Token token(key, row, column, word);
				REQUIRE(token.getKey() == key);
				REQUIRE(token.getRow() == row);
				REQUIRE(token.getColumn() == column);
				REQUIRE(token.getValue() == word);
			}
		}


	}


}

#endif