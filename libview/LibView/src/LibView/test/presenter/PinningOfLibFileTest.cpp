#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"



#include "LibView/presenter/PinnedLibFile.h"
#include "LibView/exception/presenter/NoFilePinnedException.h"


#include <iostream>

SCENARIO("An index can be set and retrieved in the PinnedLibFile.", "[LibView::presenter::PinnedLibFile]")
{
    GIVEN("A PinnedLibFile object without set index")
    {

        LibView::index_t index1 = LibView::index_t::requestUnique();
        LibView::index_t index2 = LibView::index_t::requestUnique();
        LibView::presenter::PinnedLibFile pinnedLibFile;
        WHEN("Value is retrieved")
        {
            THEN("An exception is thrown")
            {
                REQUIRE_THROWS_AS(pinnedLibFile.getPinnedFileIndex(), LibView::exception::NoFilePinnedException);
            }
        }
        WHEN("An index is added")
        {
            pinnedLibFile.setPinnedFile(index1);

            THEN("The index can be retrieved through the getter")
            {

                REQUIRE(pinnedLibFile.getPinnedFileIndex() == index1);
            }
        }
       
        
    }

}


#endif