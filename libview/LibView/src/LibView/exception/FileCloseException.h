#pragma once
#include "LibViewException.h"


namespace LibView
{
    namespace exception
    {
        /**
         * File close Exception class for the LibView Project. Inherits from LibViewException
         */
        class FileCloseException :
            public LibViewException
        {
        public:

            /**
             * Default constructor of FileCloseException.
             */
            FileCloseException();
            
            
            /**
             * Overloaded constructor of FileCloseException. Initializes the location description to a given string.
             *
             * @param location (const char*) : The location description for the exception.
             */
            FileCloseException(const char* location);


            /**
             *
             *
             *
             *
             */
            const char* getName()   const noexcept override;

            

        };
    }
}