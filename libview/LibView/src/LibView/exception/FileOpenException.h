#pragma once
#include "LibViewException.h"

namespace LibView
{
    namespace exception
    {
        /**
         * File open Exception class for the LibView Project. Inherits from LibViewException
         */
        class FileOpenException :
            public LibViewException
        {
        public:

            /**
             * Default constructor of FileOpenException. 
             */
            FileOpenException();
            
            
            /**
             * Overloaded constructor of FileOpenException. Initializes the location description to a given string.
             *
             * @param location (const char*) : The location description for the exception.
             */
            FileOpenException(const char* location);
           
            /**
             *
             *
             *
             *
             */
            virtual const char* getName()  const noexcept override;

           
        };

    }
}