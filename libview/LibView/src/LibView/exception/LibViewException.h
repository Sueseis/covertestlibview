#pragma once

#include <exception>
#include <string>

namespace LibView
{
    namespace exception
    {

        /**
         * Base Exception class for the LibView Project. Inherits from std::exception
         */
        class LibViewException :
            public std::exception
        {
        protected:

            /**
             * Stores the location of the exception throw.
             */
            std::string m_Location;
            std::string m_Name;
            std::string m_Description = "Unknown";

            /**
             *
             *
             *
             *
             */
            virtual const char* getName() const noexcept = 0;
            void generateDescription();

        public:


            /**
             * Default constructor of LibViewException. Initializes m_Location to an unknown location information.
             */
            LibViewException();
            
            
            /**
             * Overloaded constructor of LibViewException. Initializes the location description to a given string.
             *
             * @param location (const char*) : The location description for the exception.
             */
            LibViewException(const char* location);


            /**
             * Returns the exception description.
             *
             * @return const char* : The exception description.
             */
            const char* what() const throw () override;



        };
    }
}