#include "MapIndexNotPresentException.h"


namespace LibView
{
    namespace exception
    {
        MapIndexNotPresentException::MapIndexNotPresentException() : LibViewException()
        {
            generateDescription();
        }
        MapIndexNotPresentException::MapIndexNotPresentException(const char* location) : LibViewException(location)
        {
            generateDescription();
        }
        const char* MapIndexNotPresentException::getName()  const noexcept
        {
            return "MapIndexNotPresentException";
        }
        
    }
}