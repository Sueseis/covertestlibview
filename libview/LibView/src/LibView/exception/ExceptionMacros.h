#include "LibView/exception/LibViewException.h"

#define LIBVIEW_DECLARE_EXCEPTION(NAME) class NAME : public LibViewException \
{ \
public:\
        NAME(); \
        NAME(const char* location); \
        virtual const char* getName()  const noexcept override;\
} 

#define LIBVIEW_DEFINE_EXCEPTION(NAME) NAME :: NAME () : LibViewException() { generateDescription(); } \
NAME :: NAME (const char* location) : LibViewException(location) { generateDescription(); } \
const char* NAME::getName()  const noexcept { return #NAME; }
