#include "TriedRetrievingEmptyVariableException.h"
namespace LibView
{
	namespace exception
	{
		LIBVIEW_DEFINE_EXCEPTION(TriedRetrievingEmptyVariableException);
	}
}