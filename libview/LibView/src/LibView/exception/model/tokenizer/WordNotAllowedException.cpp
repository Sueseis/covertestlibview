#include "WordNotAllowedException.h"
namespace LibView
{
	namespace exception
	{
		LIBVIEW_DEFINE_EXCEPTION(WordNotAllowedException);
	}
}