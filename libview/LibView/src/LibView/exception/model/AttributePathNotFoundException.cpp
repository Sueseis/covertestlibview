#include "AttributePathNotFoundException.h"
namespace LibView
{
    namespace exception
    {
        AttributePathNotFoundException::AttributePathNotFoundException() : LibViewException()
        {
            generateDescription();
        }
        AttributePathNotFoundException::AttributePathNotFoundException(const char* location) : LibViewException(location)
        {
            generateDescription();
        }
        const char* AttributePathNotFoundException::getName()  const noexcept
        {
            return "AttributePathNotFoundException";
        }
        
    }
}