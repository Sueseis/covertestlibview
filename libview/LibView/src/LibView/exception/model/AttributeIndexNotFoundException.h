#pragma once
#include "LibView/exception/LibViewException.h"


namespace LibView
{
    namespace exception
    {
        /**
         * File close Exception class for the LibView Project. Inherits from LibViewException
         */
        class AttributeIndexNotFoundException :
            public LibViewException
        {
        public:

            /**
             * Default constructor of AttributeIndexNotFoundException.
             */
            AttributeIndexNotFoundException();


            /**
             * Overloaded constructor of AttributeIndexNotFoundException. Initializes the location description to a given string.
             *
             * @param location (const char*) : The location description for the exception.
             */
            AttributeIndexNotFoundException(const char* location);

            /**
             *
             *
             *
             *
             */
            const char* getName()  const noexcept override;
            

        };
    }
}