#include "OutsideOfGroupDeclarationException.h"
namespace LibView
{
	namespace exception
	{
		LIBVIEW_DEFINE_EXCEPTION(OutsideOfGroupDeclarationException);
	}
}