#include "MoreGroupsClosedThanStartedException.h"
namespace LibView
{
	namespace exception
	{
		LIBVIEW_DEFINE_EXCEPTION(MoreGroupsClosedThanStartedException);
	}
}