#include "WordNotAllowedAtThisPositionException.h"
namespace LibView
{
	namespace exception
	{
		LIBVIEW_DEFINE_EXCEPTION(WordNotAllowedAtThisPositionException);
	}
}