#include "LibFileIndexNotFoundException.h"

namespace LibView
{
    namespace exception
    {
        LIBVIEW_DEFINE_EXCEPTION(LibFileIndexNotFoundException);
    }
}