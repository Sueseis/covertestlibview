#include "AttributePathAlreadyFoundException.h"
namespace LibView
{
    namespace exception
    {
        AttributePathAlreadyFoundException::AttributePathAlreadyFoundException() : LibViewException()
        {
            generateDescription();
        }
        AttributePathAlreadyFoundException::AttributePathAlreadyFoundException(const char* location) : LibViewException(location)
        {
            generateDescription();
        }
        const char* AttributePathAlreadyFoundException::getName()  const noexcept
        {
            return "AttributePathAlreadyFoundException";
        }
       
    }
}