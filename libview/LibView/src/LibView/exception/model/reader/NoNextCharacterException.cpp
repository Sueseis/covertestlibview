#include "NoNextCharacterException.h"
namespace LibView
{
	namespace exception
	{
		LIBVIEW_DEFINE_EXCEPTION(NoNextCharacterException);
	}
}