#include "AttributeIndexNotFoundException.h"
namespace LibView
{
    namespace exception
    {
        AttributeIndexNotFoundException::AttributeIndexNotFoundException() : LibViewException()
        {
            generateDescription();
        }
        AttributeIndexNotFoundException::AttributeIndexNotFoundException(const char* location) : LibViewException(location)
        {
            generateDescription();
        }
        const char* AttributeIndexNotFoundException::getName()  const noexcept
        {
            return "AttributeIndexNotFoundException";
        }
        
    }
}